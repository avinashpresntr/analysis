<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Transaction
 * &@property Datatables $datatable
 */
class Entry extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("Appmodel", "app_model");
		$this->load->model("Entrymodel", "entry_model");
		$this->load->library('form_validation');
	}

	function get_allaccount() {
		/* $result = $this->app_model->get_allMasterRecored('scripts','script_id','script_name');
        $recored_arr = array();
        foreach($result as $recored_row){
            $recored_arr[] = $recored_row;
        }
        echo json_encode($recored_arr);
        return $recored_arr;*/
		//$script_id = $this->input->post('script_id');
		$expires=$this->entry_model->getScripts();
		echo json_encode($expires);
		return $expires;
	}

	function save()
	{
		$entry_data = $this->input->post();
		//$mysqldate = date( 'Y-m-d', '02/02/2016');
		//$mysqldate = date('Y-m-d', strtotime(str_replace('-', '/', '02/02/2016')));

		$day = substr($entry_data['datepicker'],0,2);
		$month = substr($entry_data['datepicker'],3,2);
		$year = substr($entry_data['datepicker'],6,4);

		//$t2 = substr($date11,0,2);
		//$b2 = substr($date1,3,2);
		//$y2 = substr($date1,6,4);
		//print_r($entry_data);exit;
		$mysqldate ="$year-$month-$day";		

		$date = date('Y-m-d', strtotime($mysqldate));

		$account = $entry_data['account_name'];
		$account_id = 0;
		/*if(!empty($account)){
            if(is_numeric($account)){
                $account_id = $account;
            } else {
                $account = trim($account);
                $res = $this->app_model->get_result_where('scripts','account_name',$account);
                if(empty($res)){
                    $account_id = $this->app_model->insert('scripts',array("account_name" => $account, "created_at" => date('Y-m-d H:i:s')));
                } else {
                    $account_id = $res[0]->account_id;
                }
            }
        }*/
		$expiry_ids='';
		if(!empty($entry_data['expiry_id'])){$expiry_ids = implode(',', $entry_data['expiry_id']);}
		if(isset($entry_data['entry_id']) && $entry_data['entry_id']!='')
		{
			$this->db->where('id',$entry_data['entry_id']);
			$this->db->update('entry', array('pick_date'=>$date,'script_id'=>$entry_data['account_name'],'expiry_id'=>$expiry_ids,'price'=>$entry_data['price'],'qty'=>$entry_data['qty'],'qty_pending'=>$entry_data['qty'],'buy_sell'=>$entry_data['buy_sale'],'client_id'=>1,'brokPercentage' => $entry_data['brokerage_percentage'],'brokFix' => $entry_data['brokerage_fix']));
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Transaction successfully updated.');
			$buysell = $this->entry_model->getBuySell();
			redirect(base_url() . 'average/manualEntry?id='.$entry_data['entry_id']);  
		}
		else{
			$script_data['script_id'] = $entry_data['account_name'];
			$script_data['expiry_id'] = $expiry_ids;
			//$entry_data['date'] = date('Y-m-d',strtotime($entry_data['date']));
			$script_data['pick_date'] = $date;
			$script_data['price'] = $entry_data['price'];
			$script_data['qty'] = $entry_data['qty'];
			$script_data['qty_pending'] = $entry_data['qty'];
			//buy_or_sell will be difined by - and +.
			//print_r($entry_data);exit;
			$script_data['buy_sell'] = $entry_data['buy_sale'];
			$script_data['client_id'] = 1;
			$script_data['brokPercentage'] = $entry_data['brokerage_percentage'];
			$script_data['brokFix'] = $entry_data['brokerage_fix'];
			$this->db->insert('entry', $script_data);
			if ($this->db->insert_id() > 0) {
				$last_insert_id=$this->db->insert_id();
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Transaction successfully added.');
				$buysell = $this->entry_model->getBuySell();
				redirect(base_url() . 'average/manualEntry?id='.$last_insert_id);           
			}
		}

	}

	/* function getBuySell()
    {
		print_r($this->entry_model->getBuySell());
	}*/


	function getBuySell2()
	{
		$grand_qty_sell=0;
		$grand_total_sell = 0;

		foreach($buysell['sell'] as $key => $value)
		{									
			$average_sell[$value['script_id']][$value['id']]['price'] = $value['price'];
			$average_sell[$value['script_id']][$value['id']]['qty'] = $value['qty'];
			$average_sell[$value['script_id']][$value['id']]['total'] = $value['qty']*$value['price'];

			foreach($average_sell[$value['script_id']] as $key1 => $value1)
			{
				$grand_qty_sell = $grand_qty_sell + $value1['qty'];
				$grand_total_sell = $grand_total_sell + $value1['total'];
				$grand_total_sell_avg=round($grand_total_buy/$grand_qty_buy,2);

				$average_sell[$value['script_id']][$value['id']]['grand_qty_sell']		=$grand_qty_sell;
				$average_sell[$value['script_id']][$value['id']]['grand_total_sell']	=$grand_total_sell;
				$average_sell[$value['script_id']][$value['id']]['grand_total_sell_avg']=$grand_total_sell_avg;
			}
			$grand_qty_sell=0;
			$grand_total_sell = 0;
			$grand_total_avg = 0;
		}

		$grand_qty_buy = 0;
		$grand_total_buy = 0;

		foreach($buysell['buy'] as $key => $value)
		{									
			$average_buy[$value['script_id']][$value['id']]['price'] = $value['price'];
			$average_buy[$value['script_id']][$value['id']]['qty'] = $value['qty'];
			$average_buy[$value['script_id']][$value['id']]['total'] = $value['qty']*$value['price'];

			foreach($average_buy[$value['script_id']] as $key1 => $value1)
			{
				$grand_qty_buy = $grand_qty_buy + $value1['qty'];
				$grand_total_buy = $grand_total_buy + $value1['total'];
				$grand_total_buy_avg=round($grand_total_buy/$grand_qty_buy,2);

				$average_sell[$value['script_id']][$value['id']]['grand_qty_buy']		=$grand_qty_sell;
				$average_sell[$value['script_id']][$value['id']]['grand_total_buy']	=$grand_total_sell;
				$average_sell[$value['script_id']][$value['id']]['grand_total_buy_avg']=$grand_total_sell_avg;
			}
			$grand_qty_buy=0;
			$grand_total_buy = 0;
			$grand_total_buy_avg = 0;
		}

		$data['buy']=$average_buy;
		$data['sell']=$average_sell;
	}

	function getAllScriptsForDropdown()
	{
		//$entry_data = $this->input->post('');
		$scripts=$this->entry_model->getAllScripts();
		$data="";
		foreach($scripts as $key => $value)
		{
			$data.="<option value='".$value->script_id."'>".$value->script_name."</option>";
		}
	}

	function getExpiryByScriptId()
	{
		$script_id = $this->input->post('script_id');
		$expires=$this->entry_model->getExpiryByScriptId($script_id);
		echo json_encode($expires);
		return $expires;
	}
	function getBrokByScriptId()
	{
		$script_id = $this->input->post('script_id');
		$brok=$this->entry_model->getBrokByScriptId($script_id);
		echo json_encode($brok);
		return $brok;
	}

	function saveScript()
	{
		if(!empty($this->input->post('script_id'))){
			$script_id = $this->input->post('script_id');
			//echo $script_id;exit;
			$post_data = $this->input->post();
			$script_data['script_name'] = $post_data['script_name'];
			$script_data['brokPercentage'] = $post_data['brokerage_percentage'];
			$script_data['brokFix'] = $post_data['brokerage_fix'];
			//print_r($script_data);exit
			$result = $this->app_model->update('scripts', $script_data, array('script_id' => $script_id));
			//echo $this->db->last_query();exit
			if ($result) {
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Script successfully Updated.');
				redirect(base_url() . 'average/scripts');           
			}
		}else{
			$entry_data = $this->input->post();
			$date = date('Y-m-d H:i:s');

			$script_data['script_name'] = $entry_data['script_name'];
			$script_data['created_at'] = $date;
			$script_data['brokPercentage'] = $entry_data['brokerage_percentage'];
			$script_data['brokFix'] = $entry_data['brokerage_fix'];

			$this->db->insert('scripts', $script_data);
			if ($this->db->insert_id() > 0) {
				$this->session->set_flashdata('success', true);
				$this->session->set_flashdata('message', 'Script successfully added.');
				redirect(base_url() . 'average/scripts');           
			}	
		}

	}

	function deleteScript()
	{
		$script_id = $this->input->get('id');

		$this->db->where('script_id', $script_id);
		$this->db->delete('scripts');
		$this->session->set_flashdata('success', true);
		$this->session->set_flashdata('message', 'Script successfully Deleted.');
		redirect(base_url() . 'average/scripts');
	}

	function saveExpiry()
	{
		$entry_data = $this->input->post();
		$expiry_data['script_id'] = $entry_data['script_id'];
		$expiry_data['expiry_name'] = $entry_data['expiry_name'];

		$this->db->insert('expiry', $expiry_data);
		if ($this->db->insert_id() > 0) {
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Transaction successfully added.');
			redirect(base_url() . 'average/expiry');           
		}
	}

	function deleteExpiry()
	{
		$expiry_id = $this->input->get('id');

		$this->db->where('expiry_id', $expiry_id);
		$this->db->delete('expiry');
		$this->session->set_flashdata('success', true);
		$this->session->set_flashdata('message', 'Expiry successfully Deleted.');
		redirect(base_url() . 'average/expiry');
	}
}
