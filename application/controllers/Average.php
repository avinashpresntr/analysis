<?php
class Average extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(false);
		$this->load->model('appmodel');
		$this->load->model('filemodel');
		$this->load->model("Entrymodel", "entry_model");
		$this->load->model("Sattlement", "sattlement_model");
	}

	public function index()
	{
		if ($this->session->userdata('is_logged_in')) {
			//$data['records'] = $this->appmodel->highlowlist();
			set_page('average');
		} else {
			redirect('/auth/login/');
		}            
	}

	public function getAverage($id)
	{            
		$data['buy'] = $this->appmodel->getFileByShorOrBuy($id,'Buy');
		$data['short'] = $this->appmodel->getFileByShorOrBuy($id,'Short');

		$data['highlow'] = $this->appmodel->getHighLowSheetData($id);
		$data['filename'] = $this->appmodel->getFilenamebyId($id);
		if($data['highlow'] == 0){
			$this->session->set_flashdata('upload',true);
			$this->session->set_flashdata('message','Please Upload highlow sheet for get average.');
			redirect('auth/');
		}else{
			set_page('average',$data);
		}
	}

	public function getAverageMinus($id)
	{
		//$id = $this->uri->segment(4);
		//$id = $this->input->post('id');
		$data['buy'] = $this->appmodel->getFileByShortOrBuyMinus($id,'Buy');
		$data['short'] = $this->appmodel->getFileByShortOrBuyMinus($id,'Short');
		$data['filename'] = $this->appmodel->getFilenamebyId($id);
		$data['highlow'] = $this->appmodel->getHighLowSheetData($id);

		if($data['highlow'] == 0){
			$this->session->set_flashdata('upload',true);
			$this->session->set_flashdata('message','Please Upload highlow sheet for get average.');
			redirect('auth/');
		}else{
			set_page('averageminus',$data);
		}            
	}        

	public function getAverageBuy($id,$datetype)
	{

		$data['buy'] = $this->appmodel->getFileByShorOrBuy($id,'Buy');
		$data['short'] = $this->appmodel->getFileByShorOrBuy($id,'Short');
		$data['datefilter'] =  $datetype;
		$data['highlow'] = $this->appmodel->getHighLowSheetData($id);
		$data['filename'] = $this->appmodel->getFilenamebyId($id);
		if($data['highlow'] == 0){
			$this->session->set_flashdata('upload',true);
			$this->session->set_flashdata('message','Please Upload highlow sheet for get average.');
			redirect('auth/');
		}else{
			set_page('averagebuy',$data);
		}
	}

	public function getAverageShort($id,$datetype)
	{
		$data['statistics']=$this->appmodel->getFileStatistics($id);
		if(isset($_GET['pl_lot_price']) && !empty($_GET['pl_lot_price']))
			$data['pl_lot_price']=$_GET['pl_lot_price'];
		else
			$data['pl_lot_price']=1;
		$data['buy'] = $this->appmodel->getFileByShorOrBuy($id,'Buy');
		$data['short'] = $this->appmodel->getFileByShorOrBuy($id,'Short');
		$data['datefilter'] =  $datetype;
		$data['hl_file_id'] = $id;
		$data['highlow'] = $this->appmodel->getHighLowSheetData($id);
		$data['filename'] = $this->appmodel->getFilenamebyId($id);
		if($data['highlow'] == 0){
			$this->session->set_flashdata('upload',true);
			$this->session->set_flashdata('message','Please Upload highlow sheet for get average.');
			redirect('auth/');
		}else{
			set_page('averageshort',$data);
		}			
	}

	public function manualEntry()
	{
		$data['buysell']=$this->entry_model->getBuySell();
		$data['scripts']=$this->entry_model->getAllScripts();
		set_page('entry',$data);
	}

	public function ajaxDeleteOnlyEntry()
	{
		$entry_id = $this->input->post('entry_id');
		/*first delete from entry*/
		$this->db->where('id', $entry_id);
		$this->db->delete('entry');

		/*then delete from sattlement*/
		$settlement_master_id_data=$this->entry_model->deleteSettlement($entry_id);

	}
	public function ajaxDeleteOnlyJoomkha()
	{
		$sattlement_master_id = $this->input->post('sattlement_master_id');
		//first get sattlement from sattlement_master_id
		$joomkha = $this->sattlement_model->getSettlementById($sattlement_master_id);
		if(isset($joomkha) && !empty($joomkha))
		{
			foreach($joomkha as $k => $v)
			{
				$entry_id = $v['entry_id'];
				$qty = $v['qty'];
				$this->entry_model->updateEntryByEntryIdForJoomkhaDelete($entry_id,$qty);
			}
			$this->db->where('id', $this->input->post('sattlement_master_id'));
			$this->db->delete('sattlement_master');
		}

	}

	public function ajaxUpdateQtyEntry()
	{
		//echo '<pre>';print_r($_POST);exit;
		$data['buy'] = $this->input->post('buy_array');
		$data['sell'] = $this->input->post('sell_array');
		$script_id = $this->input->post('script_id');
		$expiry_id = $this->input->post('expiry_id');
		$expiry_ids='';
		//if($expiry_id!=''){$expiry_ids = implode(', ', $expiry_id);}
		$fromDate = $this->input->post('dateFrom');
		$toDate = $this->input->post('dateTo');
		$average_sell = array();
		$average_buy = array();

		foreach($data['buy'] as $buydata)
		{
			$pick_date[]=date('Y-m-d',strtotime(str_replace('/', '-', $buydata['pick_date'])));
		}
		foreach($data['sell'] as $selldata)
		{
			$pick_date[]=date('Y-m-d',strtotime(str_replace('/', '-', $selldata['pick_date'])));
		}
		$mostRecent= 0;
		foreach($pick_date as $date){
			$curDate = strtotime($date);
			if ($curDate > $mostRecent) {
				$mostRecent = $curDate;
			}
		}

		$settlement_master_data = array(
			'script_id' => $script_id,
			'expiry_id' => $expiry_id,
			'max_date' => date('Y-m-d',$mostRecent),
			'date_time' => date('Y-m-d H:i:s')
		);

		$this->db->insert('sattlement_master', $settlement_master_data);
		$master_sattlement_id = $this->db->insert_id();

		if($master_sattlement_id>0)
		{
			foreach($data['buy'] as $key => $value){
				$this->db->query('UPDATE `entry` SET `qty_pending` = qty_pending - '.$value['qty'].' WHERE `entry`.`id` =  '.$value['entry_id']);
				$settlement_data_buy = array(
					'settlement_master_id' => $master_sattlement_id,
					'entry_id' => $value['entry_id'],
					'script_id' => $value['script_id'],
					'expiry_id' => $value['expiry_id'],
					'price' => $value['price'],
					'qty' => $value['qty'],
					'buy_sell' => 0,
					'brokPercentage' => $value['brok_per'],
					'brokFix' => $value['brok_fix']
				);
				$this->db->insert('sattlement', $settlement_data_buy);
			}

			foreach($data['sell'] as $key1 => $value1){
				$this->db->query('UPDATE `entry` SET `qty_pending` = qty_pending - '.$value1['qty'].' WHERE `entry`.`id` =  '.$value1['entry_id']);
				//if success then add this entry to new table for zoomkha
				$settlement_data_sell = array(
					'settlement_master_id' => $master_sattlement_id,
					'entry_id' => $value1['entry_id'],
					'script_id' => $value1['script_id'],
					'expiry_id' => $value1['expiry_id'],
					'price' => $value1['price'],
					'qty' => $value1['qty'],
					'buy_sell' => 1,
					'brokPercentage' => $value1['brok_per'],
					'brokFix' => $value1['brok_fix']
				);
				$this->db->insert('sattlement', $settlement_data_sell);
			}
		}
		//fetch dta once updtaed and replace with new data into entryn table

		$dateFrom = $this->input->post('dateFrom');
		$dateTo = $this->input->post('dateTo');

		if(isset($dateFrom) && !empty($dateFrom))
			$dateFromSql = date('Y-m-d', strtotime(str_replace('-', '/', $dateFrom)));
		if(isset($dateTo) && !empty($dateTo))
			$dateToSql = date('Y-m-d', strtotime(str_replace('-', '/', $dateTo)));
		//echo $dateFromSql."<<--:-->></--:-->".$dateToSql;exit;


		if(isset($dateFrom) && !empty($dateFrom) && isset($dateTo) && !empty($dateTo))
		{
			$buysell = $this->entry_model->getBuySellByScriptIdAndDates($script_id,$dateFromSql,$dateToSql);
		}
		else
		{
			$buysell = $this->entry_model->getBuySellByScriptId($script_id);
		}
		//$buysell = $this->entry_model->getBuySellByScriptId($script_id);
		//print_r($buysell);
		$grand_qty_sell=0;
		$grand_total_sell = 0;
		$average_sell = array();
		$average_buy = array();

		$sell_cnt =0;
		if(isset($buysell['sell']) && !empty($buysell['sell']))
		{
			foreach($buysell['sell'] as $key => $value)
			{
				if($value['qty_pending']<=0)
				{
					continue;
				}
				$average_sell[$sell_cnt]['id'] = $value['id'];
				$average_sell[$sell_cnt]['expiry_id'] = $value['expiry_id'];
				$average_sell[$sell_cnt]['script_id'] = $value['script_id'];
				$average_sell[$sell_cnt]['price'] = $value['price'];
				$average_sell[$sell_cnt]['qty'] = $value['qty'];
				$average_sell[$sell_cnt]['qty_pending'] = $value['qty_pending'];
				//$average_sell[$value['id']]['total'] = $value['qty']*$value['price'];
				$average_sell[$sell_cnt]['total'] = $value['qty_pending']*$value['price'];
				$average_sell[$sell_cnt]['pick_date'] = date("d/m/Y", strtotime($value['pick_date']));
				$entryid_exist = $this->sattlement_model->getSettlementByEntryId($value['id']);
				if(isset($entryid_exist) && !empty($entryid_exist))
				{
					$average_sell[$sell_cnt]['is_edit'] = '0';
				}else{
					$average_sell[$sell_cnt]['is_edit']='1';
				}

				foreach($average_sell as $key1 => $value1)
				{
					//$grand_qty_sell = $grand_qty_sell + $value1['qty'];				
					$grand_qty_sell = $grand_qty_sell + $value1['qty_pending'];
					$grand_total_sell = $grand_total_sell + $value1['total'];
					//$grand_total_sell_avg=round($grand_total_sell/$grand_qty_sell,2);
					if($grand_qty_sell<=0)
						$grand_total_sell_avg=round($grand_total_sell/1,2);
					else
						$grand_total_sell_avg=round($grand_total_sell/$grand_qty_sell,2);

					$average_sell[$sell_cnt]['grand_qty_sell']		=$grand_qty_sell;
					$average_sell[$sell_cnt]['grand_total_sell']	=$grand_total_sell;
					$average_sell[$sell_cnt]['grand_total_sell_avg']=$grand_total_sell_avg;
				}
				$sell_cnt++;

				$grand_qty_sell=0;
				$grand_total_sell = 0;
				$grand_total_avg = 0;
			}
		}

		$grand_qty_buy = 0;
		$grand_total_buy = 0;

		$buy_cnt =0;
		if(isset($buysell['buy']) && !empty($buysell['buy']))
		{
			foreach($buysell['buy'] as $key => $value)
			{
				if($value['qty_pending']<=0)
				{
					continue;
				}
				$average_buy[$buy_cnt]['id'] = $value['id'];
				$average_buy[$buy_cnt]['expiry_id'] = $value['expiry_id'];
				$average_buy[$buy_cnt]['script_id'] = $value['script_id'];
				$average_buy[$buy_cnt]['price'] = $value['price'];
				$average_buy[$buy_cnt]['qty'] = $value['qty'];
				$average_buy[$buy_cnt]['qty_pending'] = $value['qty_pending'];
				//$average_buy[$value['id']]['total'] = $value['qty']*$value['price'];
				$average_buy[$buy_cnt]['total'] = $value['qty_pending']*$value['price'];
				$average_buy[$buy_cnt]['pick_date'] = date("d/m/Y", strtotime($value['pick_date']));
				$entryid_exist = $this->sattlement_model->getSettlementByEntryId($value['id']);
				if(isset($entryid_exist) && !empty($entryid_exist))
				{
					$average_buy[$buy_cnt]['is_edit'] = '0';
				}else{
					$average_buy[$buy_cnt]['is_edit']='1';
				}

				foreach($average_buy as $key1 => $value1)
				{
					//$grand_qty_buy = $grand_qty_buy + $value1['qty'];
					$grand_qty_buy = $grand_qty_buy + $value1['qty_pending'];
					$grand_total_buy = $grand_total_buy + $value1['total'];
					//$grand_total_buy_avg=round($grand_total_buy/$grand_qty_buy,2);
					if($grand_qty_buy<=0)
						$grand_total_buy_avg=round($grand_total_buy/1,2);
					else
						$grand_total_buy_avg=round($grand_total_buy/$grand_qty_buy,2);

					$average_buy[$buy_cnt]['grand_qty_buy']		=$grand_qty_buy;
					$average_buy[$buy_cnt]['grand_total_buy']	=$grand_total_buy;
					$average_buy[$buy_cnt]['grand_total_buy_avg']=$grand_total_buy_avg;
				}
				$buy_cnt++;

				$grand_qty_buy=0;
				$grand_total_buy = 0;
				$grand_total_buy_avg = 0;
			}
		}

		$data2['buy']=$average_buy;
		$data2['sell']=$average_sell;
		//$data['pick_date'] = $buysell['pick_date'];


		$joomkha = $this->sattlement_model->getSettlementByScriptId($script_id);

		//$joomkha = $this->sattlement_model->getSettlementByScriptAndExpiryId($script_id);

		$data2['joomkha'] = $joomkha;

		echo json_encode($data2);
		return $data2;

	}
	public function ajaxManualCombineEntry()
	{
		$id = $this->input->post('script_id');
		$expiry_id = $this->input->post('expiry_id');
		$expiry_ids='';
		if($expiry_id!=''){$expiry_ids = implode(', ', $expiry_id);}
		$fromDate = $this->input->post('dateFrom');
		$toDate = $this->input->post('dateTo');

		$buysell = $this->entry_model->getBuySellByScriptAndExpiryId($id,$expiry_ids,$fromDate,$toDate);
		//echo "<pre>";print_r($buysell);exit;
		$grand_qty_sell=0;
		$grand_total_sell = 0;
		$average_sell = array();
		$average_buy = array();

		$sell_cnt =0;
		if(isset($buysell['sell']) && !empty($buysell['sell']))
		{
			foreach($buysell['sell'] as $key => $value)
			{
				if($value['qty_pending']<=0)
				{
					continue;
				}
				$average_sell[$sell_cnt]['id'] = $value['id'];
				$average_sell[$sell_cnt]['expiry_id'] = $value['expiry_id'];
				$average_sell[$sell_cnt]['script_id'] = $value['script_id'];
				$average_sell[$sell_cnt]['price'] = $value['price'];
				$average_sell[$sell_cnt]['qty'] = $value['qty'];
				$average_sell[$sell_cnt]['qty_pending'] = $value['qty_pending'];
				//$average_sell[$value['id']]['total'] = $value['qty']*$value['price'];
				$average_sell[$sell_cnt]['total'] = $value['qty_pending']*$value['price'];
				$average_sell[$sell_cnt]['pick_date'] = date("d/m/Y", strtotime($value['pick_date']));
				$average_sell[$sell_cnt]['brokPercentage'] = $value['brokPercentage'];
				$average_sell[$sell_cnt]['brokFix'] = $value['brokFix'];
				$entryid_exist = $this->sattlement_model->getSettlementByEntryId($value['id']);
				if(isset($entryid_exist) && !empty($entryid_exist))
				{
					$average_sell[$sell_cnt]['is_edit'] = '0';
				}else{
					$average_sell[$sell_cnt]['is_edit']='1';
				}

				foreach($average_sell as $key1 => $value1)
				{
					//$grand_qty_sell = $grand_qty_sell + $value1['qty'];
					$grand_qty_sell = $grand_qty_sell + $value1['qty_pending'];
					$grand_total_sell = $grand_total_sell + $value1['total'];
					if($grand_qty_sell<=0)
						$grand_total_sell_avg=round($grand_total_sell/1,2);
					else
						$grand_total_sell_avg=round($grand_total_sell/$grand_qty_sell,2);

					$average_sell[$sell_cnt]['grand_qty_sell']		=$grand_qty_sell;
					$average_sell[$sell_cnt]['grand_total_sell']		=$grand_total_sell;
					$average_sell[$sell_cnt]['grand_total_sell_avg']	=$grand_total_sell_avg;
				}
				$sell_cnt++;
				$grand_qty_sell=0;
				$grand_total_sell = 0;
				$grand_total_avg = 0;
			}
			$data['sell']=$average_sell;
		}

		$grand_qty_buy = 0;
		$grand_total_buy = 0;
		//print_r($buysell['buy']);

		$buy_cnt =0;
		if(isset($buysell['buy']) && !empty($buysell['buy']))
		{
			foreach($buysell['buy'] as $key => $value)
			{
				if($value['qty_pending']<=0)
				{
					continue;
				}
				$average_buy[$buy_cnt]['id'] = $value['id'];
				$average_buy[$buy_cnt]['expiry_id'] = $value['expiry_id'];
				$average_buy[$buy_cnt]['script_id'] = $value['script_id'];
				$average_buy[$buy_cnt]['price'] = $value['price'];
				$average_buy[$buy_cnt]['qty'] = $value['qty'];
				$average_buy[$buy_cnt]['qty_pending'] = $value['qty_pending'];
				//$average_buy[$value['id']]['total'] = $value['qty']*$value['price'];
				$average_buy[$buy_cnt]['total'] = $value['qty_pending']*$value['price'];
				$average_buy[$buy_cnt]['pick_date'] = date("d/m/Y", strtotime($value['pick_date']));
				$average_buy[$buy_cnt]['brokPercentage'] = $value['brokPercentage'];
				$average_buy[$buy_cnt]['brokFix'] = $value['brokFix'];
				$entryid_exist = $this->sattlement_model->getSettlementByEntryId($value['id']);
				if(isset($entryid_exist) && !empty($entryid_exist))
				{
					$average_buy[$buy_cnt]['is_edit'] = '0';
				}else{
					$average_buy[$buy_cnt]['is_edit']='1';
				}

				foreach($average_buy as $key1 => $value1)
				{
					//$grand_qty_buy = $grand_qty_buy + $value1['qty'];
					$grand_qty_buy = $grand_qty_buy + $value1['qty_pending'];
					$grand_total_buy = $grand_total_buy + $value1['total'];
					if($grand_qty_buy<=0)
						$grand_total_buy_avg=round($grand_total_buy/1,2);
					else
						$grand_total_buy_avg=round($grand_total_buy/$grand_qty_buy,2);


					$average_buy[$buy_cnt]['grand_qty_buy']		=$grand_qty_buy;
					$average_buy[$buy_cnt]['grand_total_buy']	=$grand_total_buy;
					$average_buy[$buy_cnt]['grand_total_buy_avg']=$grand_total_buy_avg;
				}

				$buy_cnt++;

				$grand_qty_buy=0;
				$grand_total_buy = 0;
				$grand_total_buy_avg = 0;
			}
			$data['buy']=$average_buy;				
		}
		/* start update buy average and sell average on select script into scripts table */
		if(count($average_buy) > 0)
		{
			$keys_buy = array_keys($average_buy);
			$lastkey_buy = $keys_buy[count($keys_buy)-1];
			$this->db->where('script_id',$id);
			$this->db->update('scripts', array('buy_average'=>$average_buy[$lastkey_buy]['grand_total_buy_avg']));
		}
		if(count($average_sell) > 0)
		{
			$keys_sell = array_keys($average_sell);
			$lastkey_sell = $keys_sell[count($keys_sell)-1];
			$this->db->where('script_id',$id);
			$this->db->update('scripts', array('short_average'=>$average_sell[$lastkey_sell]['grand_total_sell_avg']));
		}
		/* end update buy average and sell average on select script into scripts table */

		/*start of get expiry by script id code for display expiry*/
		$expires=$this->entry_model->getExpiryByScriptId($id);
		$data['expiry']=$expires;
		/*end of get expiry by script id code for display expiry*/



		$joomkha = $this->sattlement_model->getSettlementByScriptAndExpiryId($id,$expiry_ids,$fromDate,$toDate);

		$data['joomkha'] = $joomkha;

		echo json_encode($data);
		return $data;
		//$data['buysell']=$this->entry_model->getBuySell();
		//set_page('entry',$data);
	}

	public function ajaxManualEntry()
	{
		$id = $this->input->post('script_id');
		$dateFrom = $this->input->post('dateFrom');
		$dateTo = $this->input->post('dateTo');

		if(isset($dateFrom) && !empty($dateFrom))
			$dateFromSql = date('Y-m-d', strtotime(str_replace('-', '/', $dateFrom)));
		if(isset($dateTo) && !empty($dateTo))
			$dateToSql = date('Y-m-d', strtotime(str_replace('-', '/', $dateTo)));
		//echo $dateFromSql."<<--:-->></--:-->".$dateToSql;exit;


		if(isset($dateFrom) && !empty($dateFrom) && isset($dateTo) && !empty($dateTo))
		{
			$buysell = $this->entry_model->getBuySellByScriptIdAndDates($id,$dateFromSql,$dateToSql);
		}
		else
		{
			$buysell = $this->entry_model->getBuySellByScriptId($id);
			//print_r($buysell);
		}

		//print_r($buysell);
		$grand_qty_sell=0;
		$grand_total_sell = 0;
		$average_sell = array();
		$average_buy = array();

		$sell_cnt =0;
		if(isset($buysell['sell']) && !empty($buysell['sell']))
		{
			foreach($buysell['sell'] as $key => $value)
			{
				if($value['qty_pending']<=0)
				{
					continue;
				}
				$average_sell[$sell_cnt]['id'] = $value['id'];
				$average_sell[$sell_cnt]['expiry_id'] = $value['expiry_id'];
				$average_sell[$sell_cnt]['script_id'] = $value['script_id'];
				$average_sell[$sell_cnt]['price'] = $value['price'];
				$average_sell[$sell_cnt]['qty'] = $value['qty'];
				$average_sell[$sell_cnt]['qty_pending'] = $value['qty_pending'];
				//$average_sell[$value['id']]['total'] = $value['qty']*$value['price'];
				$average_sell[$sell_cnt]['total'] = $value['qty_pending']*$value['price'];
				$average_sell[$sell_cnt]['pick_date'] = date("d/m/Y", strtotime($value['pick_date']));
				$average_sell[$sell_cnt]['brokPercentage'] = $value['brokPercentage'];
				$average_sell[$sell_cnt]['brokFix'] = $value['brokFix'];

				foreach($average_sell as $key1 => $value1)
				{
					//$grand_qty_sell = $grand_qty_sell + $value1['qty'];
					$grand_qty_sell = $grand_qty_sell + $value1['qty_pending'];
					$grand_total_sell = $grand_total_sell + $value1['total'];
					if($grand_qty_sell<=0)
						$grand_total_sell_avg=round($grand_total_sell/1,2);
					else
						$grand_total_sell_avg=round($grand_total_sell/$grand_qty_sell,2);

					$average_sell[$sell_cnt]['grand_qty_sell']		=$grand_qty_sell;
					$average_sell[$sell_cnt]['grand_total_sell']		=$grand_total_sell;
					$average_sell[$sell_cnt]['grand_total_sell_avg']	=$grand_total_sell_avg;
				}
				$sell_cnt++;
				$grand_qty_sell=0;
				$grand_total_sell = 0;
				$grand_total_avg = 0;
			}
		}

		$grand_qty_buy = 0;
		$grand_total_buy = 0;
		//print_r($buysell['buy']);

		$buy_cnt =0;
		if(isset($buysell['buy']) && !empty($buysell['buy']))
		{
			foreach($buysell['buy'] as $key => $value)
			{
				if($value['qty_pending']<=0)
				{
					continue;
				}
				$average_buy[$buy_cnt]['id'] = $value['id'];
				$average_buy[$buy_cnt]['expiry_id'] = $value['expiry_id'];
				$average_buy[$buy_cnt]['script_id'] = $value['script_id'];
				$average_buy[$buy_cnt]['price'] = $value['price'];
				$average_buy[$buy_cnt]['qty'] = $value['qty'];
				$average_buy[$buy_cnt]['qty_pending'] = $value['qty_pending'];
				//$average_buy[$value['id']]['total'] = $value['qty']*$value['price'];
				$average_buy[$buy_cnt]['total'] = $value['qty_pending']*$value['price'];
				$average_buy[$buy_cnt]['pick_date'] = date("d/m/Y", strtotime($value['pick_date']));
				$average_buy[$buy_cnt]['brokPercentage'] = $value['brokPercentage'];
				$average_buy[$buy_cnt]['brokFix'] = $value['brokFix'];

				foreach($average_buy as $key1 => $value1)
				{
					//$grand_qty_buy = $grand_qty_buy + $value1['qty'];
					$grand_qty_buy = $grand_qty_buy + $value1['qty_pending'];
					$grand_total_buy = $grand_total_buy + $value1['total'];
					if($grand_qty_buy<=0)
						$grand_total_buy_avg=round($grand_total_buy/1,2);
					else
						$grand_total_buy_avg=round($grand_total_buy/$grand_qty_buy,2);


					$average_buy[$buy_cnt]['grand_qty_buy']		=$grand_qty_buy;
					$average_buy[$buy_cnt]['grand_total_buy']	=$grand_total_buy;
					$average_buy[$buy_cnt]['grand_total_buy_avg']=$grand_total_buy_avg;
				}

				$buy_cnt++;

				$grand_qty_buy=0;
				$grand_total_buy = 0;
				$grand_total_buy_avg = 0;
			}
		}
		//print_r($average_buy);
		/* start update buy average and sell average on select script into scripts table */
		if((isset($average_buy) && isset($average_sell)) && (!empty($average_buy) && !empty($average_sell)))
		{
			$keys_buy = array_keys($average_buy);
			$lastkey_buy = $keys_buy[count($keys_buy)-1];

			$keys_sell = array_keys($average_sell);
			$lastkey_sell = $keys_sell[count($keys_sell)-1];
			$this->db->where('script_id',$id);
			$this->db->update('scripts', array('buy_average'=>$average_buy[$lastkey_buy]['grand_total_buy_avg'],'short_average'=>$average_sell[$lastkey_sell]['grand_total_sell_avg']));
		}
		/* end update buy average and sell average on select script into scripts table */

		/*start of get expiry by script id code for display expiry*/
		$expires=$this->entry_model->getExpiryByScriptId($id);
		$data['expiry']=$expires;
		/*end of get expiry by script id code for display expiry*/

		$data['buy']=$average_buy;
		$data['sell']=$average_sell;			

		$joomkha = $this->sattlement_model->getSettlementByScriptId($id);

		$data['joomkha'] = $joomkha;

		echo json_encode($data);
		return $data;
		//$data['buysell']=$this->entry_model->getBuySell();
		//set_page('entry',$data);
	}

	public function scripts($script_id = null)
	{
		if(isset($script_id) && !empty($script_id)){
			$where_array['script_id'] = $script_id;
			$data['script'] = $this->entry_model->get_all_with_where('scripts','','',$where_array);
			//print_r($data);exit;
		}
		$data['scripts']=$this->entry_model->getAllScripts();
		set_page('script',$data);
	}

	public function expiry()
	{
		$data['scripts']=$this->entry_model->getAllScripts();
		$data['expiry']=$this->entry_model->getAllExpiry();
		set_page('expiry',$data);
	}

	public function ajaxEditEntry()
	{
		$entry_id = $this->input->post('entry_id');
		$entryData = $this->entry_model->getEntryByEntryId($entry_id);
		$entryData['entry'][0]['pick_date']=date('d/m/Y',strtotime($entryData['entry'][0]['pick_date']));

		/*start of get expiry by script id code for display expiry*/
		$expires=$this->entry_model->getExpiryByScriptId($entryData['entry'][0]['script_id']);
		$entryData['expiry']=$expires;
		/*end of get expiry by script id code for display expiry*/

		echo json_encode($entryData);
		return $entryData;
	}
}
?>
