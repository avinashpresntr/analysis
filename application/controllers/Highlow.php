<?php
	class Highlow extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->output->enable_profiler(false);
			$this->load->model('appmodel');
		}

		public function index()
		{
            if ($this->session->userdata('is_logged_in')) {
                $namelike = 'high';
                $data['filelist'] = $this->appmodel->filenamelist($namelike);
                $filenameLike = "high";
                $data['records'] = $this->appmodel->fileList($filenameLike);
                
                set_page('high_low',$data);
            } else {
                redirect('/auth/login/');
            }
            
		}

        public function import()
        {
            
            
            if(!empty($_FILES['file']['name'])){
                $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
                if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
                    if(is_uploaded_file($_FILES['file']['tmp_name'])){
                        $Filename = basename( $_FILES['file']['name']);
                        if (strpos($Filename, 'highlow') == false) {
                            $return['error'] = "Only";
                            print json_encode($return);
                            exit;
                        }
                        $filename_exist = $this->appmodel->checkFilename($Filename);
                        if($filename_exist == 0){
                            $config['upload_path'] = './upload';
                            $config['allowed_types'] = 'text/plain|text/csv|csv';
                            $config['overwrite'] = TRUE;
                            $config['file_name'] = $_FILES['file']['name'];
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);    

                            if($this->upload->do_upload('file')){
                                $file_info = $this->upload->data();
                                $csvfilepath = "upload/" . $file_info['file_name'];
                                $Filename = $file_info['file_name'];
                                
                                
                                $filename = explode('-',$Filename);
                                $filename = $filename[0]."-".$filename[1]."-".$filename[2];

                                $data_file_id =  $this->appmodel->searchDatafileId($filename);

                                    if($data_file_id != 0){

                                        $lastID = $this->appmodel->insertFilename($Filename,$data_file_id);

                                        $this->appmodel->updateDatasheetHlId($data_file_id,$lastID);

                                        $csvFile = fopen(base_url() . $csvfilepath, 'r');
//                                        fgetcsv($csvFile);
                                        $flag = true;
                                            while(($importdata = fgetcsv($csvFile)) !== FALSE){
                                                $hl_date = substr($importdata[0], 0, 4) .'-'. substr($importdata[0], 5, 2) .'-'. substr($importdata[0], 8, 2);
                                                $data = array(
                                                    'file_id' => $lastID,
                                                    'hl_date' => $hl_date,
                                                    'hl_time' => date('H:i:s', strtotime($importdata[1])),
                                                    'hl_open' =>$importdata[2],
                                                    'hl_high' =>$importdata[3],
                                                    'hl_low' =>$importdata[4],
                                                    'hl_close' =>$importdata[5],
                                                    'hl_back_test_signal' =>$importdata[6],
                                                    'data_file_id' =>$data_file_id,
                                                    );
                                                $this->appmodel->insertHighlow($data);
                                            } 
                                        fclose($csvFile);
                                        $delpath = FCPATH . $csvfilepath;
                                        //$delpath = base_url() . $csvfilepath;
                                        unlink($delpath);
                                        $return['success'] = "Success";
                                        print json_encode($return);
                                        exit;
                                    }else{
                                        $delpath = FCPATH . $csvfilepath;
                                        //$delpath = base_url() . $csvfilepath;
                                        unlink($delpath);
                                        $return['error'] = "Data";
                                        print json_encode($return);
                                        exit;
                                    }
                            }else{
                                $delpath = FCPATH . $csvfilepath;
                                //$delpath = base_url() . $csvfilepath;
                                unlink($delpath);
                                $return['error'] = "Invalid_err";
                                $return['msg'] = $this->upload->display_errors();  
                                print json_encode($return);
                                exit;
                            }  
                        }else{
                            $return['error'] = "Exist";
                            print json_encode($return);
                            exit;
                        }
                    }else{
                        $return['error'] = "Notupload";
                        print json_encode($return);
                        exit;
                    }
                }else{
                    $return['error'] = "Invalid";
                    print json_encode($return);
                    exit;
                }   
            }else{
                $return['error'] = "Select";
                print json_encode($return);
                exit;
                redirect('data');
            }
            
        }
        
        
        function tabledata($file_id)
        {   
            /*$tablename = "data_sheet";
            $tablefield = 'do_date, do_buy_sell, do_price, dt_absolute_performance';
            $list = $this->appmodel->tabledata($file_id, $tablename, $tablefield);*/
            $config['select'] = '*';
            $config['table'] = 'highlow_sheet';
            $config['column_search'] = array('hl_date', 'hl_time');
            $config['order'] = array('hl_id' => 'asc');
            $config['where'] = array('file_id' => $file_id );
            $this->load->library('datatables', $config, 'datatable');
            $list = $this->datatable->get_datatables();
            $data = array();
            foreach ($list as $datarow) {
                $row = array();
                $row[] = $datarow->hl_date;
                $row[] = $datarow->hl_time;
                $row[] = $datarow->hl_open;
                $row[] = $datarow->hl_high;
                $row[] = $datarow->hl_low;
                $row[] = $datarow->hl_close;
                $row[] = $datarow->hl_back_test_signal;
                $data[] = $row;
            }

            $output = array(
                "recordsTotal" => $this->datatable->count_all(),
                "recordsFiltered" => $this->datatable->count_filtered(),
                "data" => $data,
            );
            echo json_encode($output);
        }
        function deleteHighlowsheet()
        {
            $tbl = "highlow_sheet";
            $dataSheetId = $_POST['id'];
            
            if(!empty($dataSheetId)){
                $this->appmodel->delete_multiple($dataSheetId, $tbl);
                $return['success'] = "Ok";
                print json_encode($return);
                exit;
                redirect('auth');
            }else{
                 $return['error'] = "Not";
                print json_encode($return);
                exit;
            }
        }
	}
?>
