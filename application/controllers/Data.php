<?php
	class Data extends CI_Controller
	{
		public function __construct()
		{
            parent::__construct();
			$this->output->enable_profiler(false);
			$this->load->model('appmodel');
		}

		public function index()
		{
            if ($this->session->userdata('is_logged_in')) {
                $namelike = 'data';
                $data['filelist'] = $this->appmodel->filenamelist($namelike);
                //$data['records'] = $this->appmodel->datasheetlist();
                set_page('data',$data);
            } else {
                redirect('/auth/login/');
            }
            
		}

		public function import()
        {
			$year = date("Y",strtotime("-1 year"));
            if(!empty($_FILES['file']['name'])){
                $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
                if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
                    if(is_uploaded_file($_FILES['file']['tmp_name'])){
                        $Filename = basename( $_FILES['file']['name']);
                        if (strpos($Filename, 'data') == false) {
                            $return['error'] = "Only";
                            print json_encode($return);
                            exit;
                        }
                        $filename_exist = $this->appmodel->checkFilename($Filename);
                        if($filename_exist == 0){
                            $config['upload_path'] = './upload';
                            $config['allowed_types'] = 'text/plain|text/csv|csv';
                            $config['overwrite'] = TRUE;
                            $config['file_name'] = $_FILES['file']['name'];
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);    

                            if($this->upload->do_upload('file')){
                                $file_info = $this->upload->data();
                                $csvfilepath = "upload/" . $file_info['file_name'];
                                $Filename = $file_info['file_name'];
                                $data_file_id = NULL;
                                $lastID = $this->appmodel->insertFilename($Filename,$data_file_id);

                                $csvFile = fopen(base_url() . $csvfilepath, 'r');
                                fgetcsv($csvFile);
                                $flag = true;
                                $prev_month = null;
                                    while(($importdata = fgetcsv($csvFile)) !== FALSE){
                                        if($flag) { $flag = false; continue; }
                                        $modified_do_date = $importdata[5];
                                        if(is_numeric(substr($modified_do_date, 1, 1))){
										} else {
											$modified_do_date = '0'.$modified_do_date;
										}
										$cur_month = substr($modified_do_date, 3, 3);
										if($cur_month=="Jan" && $prev_month=="Dec"){
											$year++;
										}
										
										//echo strlen($importdata[5]); // 6
                                        $do_date = $this->getDateFormat2($modified_do_date,$year);
                                        //$return['year'][$do_date] = date('Y-m-d H:i:s', strtotime($do_date)). ' - ' .$importdata[5] . ' - '.$year;
                                            $data = array(
                                                'file_id' => $lastID,
                                                'ds_name' => "",
                                                'ds_all_trades' => 0,
                                                'ds_long_trades' => 0,
                                                'ds_short_trades' => 0,
                                                'do_date' => date('Y-m-d H:i:s', strtotime($do_date)),
                                                'do_buy_sell' => $importdata[6],
                                                'do_price' => $importdata[7],
                                                'do_qty' => $importdata[8],
                                                'do_current_value' => $importdata[9],
                                                'dt_brokerage_fees' => $importdata[10],
                                                'dt_entry_date' => $importdata[12],
                                                'dt_exit_date' => $importdata[13],
                                                'dt_type' => $importdata[14],
                                                'dt_no_bars' => $importdata[15],
                                                'dt_absolute_performance' => $importdata[16],
                                                'dt_brokerage' => $importdata[17],
                                            );
                                            $this->appmodel->insertDatasheet($data);                                            
                                            if($importdata[1] == "N/A" || $importdata[2] == "N/A" || $importdata[3] == "N/A"){
                                                $na = "1";
                                            }
                                            else{
                                                $na = "0";
                                            }
                                            if(!empty($importdata[0])) {
                                                    $data_statistics = array(
                                                    'file_id' => $lastID,
                                                    'statistics_text' => $importdata[0],
                                                    'all_trades' => $importdata[1],
                                                    'long_trades' => $importdata[2],
                                                    'short_trades' => $importdata[3],
                                                    'isNA' => $na,
                                                );
                                                $this->appmodel->insertStatistics($data_statistics);    
                                            }
                                            $prev_month = $cur_month;
                                    }
                                fclose($csvFile);
                                $this->load->helper("file");
                                $delpath = FCPATH . $csvfilepath;
                                //$delpath = base_url() . $csvfilepath;
                                unlink($delpath);
                                $return['success'] = "Success";
                                print json_encode($return);
                                exit;
                            }else{
                                $delpath = FCPATH . $csvfilepath;
                                //$delpath = base_url() . $csvfilepath;
                                unlink($delpath);
                                $return['error'] = "Invalid_err";
                                $return['msg'] = $this->upload->display_errors();  
                                print json_encode($return);
                                exit;
                            }  
                        }else{
                            $return['error'] = "Exist";
                            print json_encode($return);
                            exit;
                        }
                    }else{
                        $return['error'] = "Notupload";
                        print json_encode($return);
                        exit;
                    }
                }else{
                    $return['error'] = "Invalid";
                    print json_encode($return);
                    exit;
                }   
            }else{
                $return['error'] = "Select";
                print json_encode($return);
                exit;
                redirect('data');
            }
                
          
        }
        
        function getDateFormat2($date, $year){
			$return = '';
			$return .= substr($date, 0, 2);//date
			$return .= ' '.substr($date, 3, 3);//Month
			if(strlen($date)<=11){
				$return .= ' '.substr($date, 6, 4);//Year
			} else {
				$return .= ' '.$year;//Year
				$return .= ' '.substr($date, 6);//Time
			}
            return $return;
        }
        
        function tabledata($file_id)
        {   
            /*$tablename = "data_sheet";
            $tablefield = 'do_date, do_buy_sell, do_price, dt_absolute_performance';
            $list = $this->appmodel->tabledata($file_id, $tablename, $tablefield);*/
            $config['select'] = 'do_date, do_buy_sell, do_price, dt_absolute_performance';
            $config['table'] = 'data_sheet';
            $config['column_search'] = array('do_date', 'do_buy_sell', 'do_price', 'dt_absolute_performance');
            $config['order'] = array('data_id' => 'asc');
            $config['where'] = array('file_id' => $file_id );
            
            $this->load->library('datatables', $config, 'datatable');
            $list = $this->datatable->get_datatables();
            $data = array();
            foreach ($list as $datarow) {
                $row = array();
                $row[] = $datarow->do_date;
                $row[] = $datarow->do_buy_sell;
                $row[] = $datarow->do_price;
                $row[] = $datarow->dt_absolute_performance;
                $data[] = $row;
            }

            $output = array(
                "recordsTotal" => $this->datatable->count_all(),
                "recordsFiltered" => $this->datatable->count_filtered(),
                "data" => $data,
            );
            echo json_encode($output);
        }
        function deleteDatasheet()
        {
            $tbl = "data_sheet";
            $dataSheetId = $_POST['id'];
            
            if(!empty($dataSheetId)){
                $this->appmodel->delete_multiple_2($dataSheetId, $tbl);    
                    $return['success'] = "Ok";
                    print json_encode($return);
                    exit;
                redirect('auth');
            }else{
                $return['error'] = "Not";
                print json_encode($return);
                exit;
                //redirect('auth');
            }
        }
	}
?>
