<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="<?=base_url();?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?php echo base_url();?>data">
            <i class="fa fa-user"></i> <span> Data</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?php echo base_url();?>highlow">
            <i class="fa fa-user"></i> <span> High Low</span>
          </a>
        </li>       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
