<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url('assets/plugins/datepicker/datepicker3.css');?>">
<style>
	//@import url('http://getbootstrap.com/dist/css/bootstrap.css');

	#myPopoverContent {display: block; float: left;}
	.popover{
		width:800px;   
	}

	#test {margin-left: 250px;}
	td {text-align: right; }
	
	.blue-madison{
		color:blue;
		font-weight: bolder;
	}
</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div style="width: 45%; float: left;">
			<div style="width: 25%; float: left;">
				<h1 style="font-size:20px; margin-top:0px;">
					<?php 
						$max = $highlow[0]->hl_high;
						$min = $highlow[0]->hl_low;						
							
						foreach($highlow as $key => $val){
							if($max < $val->hl_high){
								$max = $val->hl_high;
							}
							if($min > $val->hl_low){
								$min = $val->hl_low;
							}
						}
						$data_file['highest_high'] = $max;
						$data_file['lowest_low'] = $min;
					?>
					All Average : </h1>
			</div>
			<div style="width: 75%; float: right;">
				<b style="color: green;">Highest: >> <?=$max;?></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<b style="color: red;">Lowest: >> <?=$min;?></b>
			</div>
		</div>
		
		
		
		
		<form style="width: 55%;float: right;" action = "<?php echo site_url('average/getAverageShort/'.$hl_file_id.'/'.$datefilter);?>" method = "get" width="80%">
			
			<!--<a href="#custom-statistics" class="btn btn-primary">Custom Statistics</a>-->
			<?php echo validation_errors(); ?>
			<?php echo form_open('form'); ?>
			<input type = "submit" value = "Submit" class="btn btn-primary" style="float: right;"/>			
			<input type = "text" name = "pl_lot_price" value = "<?=$pl_lot_price;?>" size = "10"  style="float: right;margin: 5px 12px 12px 12px;"/>
			
			<!--<div>
				<input type = "submit" value = "Submit" class="btn btn-primary"/>
			</div>-->
		</form>
		
		
    </section>	
    <hr />
    <!-- Main content -->
    <?php
		/*$first23 = reset($buy);
		$last23 = end($buy);
		echo "<pre>";
		var_dump($first23);
		var_dump($last23);
		echo "</pre>";*/
    ?>
    <section class="content">
		<a href="#statistics" class="btn btn-primary">
			<i class="fa fa-table" aria-hidden="true"></i>
			Statistics</a>
		<a href="<?=base_url();?>" class="btn btn-primary waves-effect waves-light pull-right" style="margin-bottom: 20px;"><i class="fa fa-dashboard"></i>&nbsp; Go To Dashboard</a>
		<div style="overflow: scroll;clear: both;">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top">
						 <div class="box box-info box-solid">
							<div class="box-header with-border">
							  <h3 class="box-title"> Buy (<?php echo $filename->file_name; ?>)</h3>
							  <div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
								</button>
							  </div>
							  <!-- /.box-tools -->
							</div>
							<!-- /.box-header -->
											   
							<div class="box-body table-responsive" style="display: block;">
								<table id="example" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th style="display:none;">Index</th>
											<th>Date</th>
											<th>Price</th>
											<th>Entry</th>
											<th>Avg</th>
											<th>Count</th>
											<th>Info</th>
											<!--<th>Average</th>-->
										</tr>
									</thead>
									<tbody>
										<?php 
										$abs_neg_counts=0;
										$do_pice_array = array();
										$cnt = 0;
										
										$buy_minus_arr = array();
										$buy_minus_cnt = 0;
										$buy_plus_arr = array();
										$buy_plus_cnt = 0;
										
										$buy_minus_total_price = 0;
										
										$positive_count_buy = 0;
										$negative_count_buy = 0;
										$plus_minus_count_buy = 0;
										
										$avg_count_buy[1] = 0;
										$avg_count_buy[2] = 0;
										$avg_count_buy[3] = 0;
										$avg_count_buy[4] = 0;
										$avg_count_buy[5] = 0;
										$avg_count_buy[6] = 0;
										$avg_count_buy[7] = 0;
										$avg_count_buy[8] = 0;
										$avg_count_buy[9] = 0;
										$avg_count_buy[10] = 0;
										$avg_count_buy[11] = 0;
										$avg_count_buy[12] = 0;
										$avg_count_buy[13] = 0;
										$avg_count_buy[14] = 0;
										$avg_count_buy[15] = 0;
										$avg_count_buy[16] = 0;
										$avg_count_buy[17] = 0;
										$avg_count_buy[18] = 0;
										$avg_count_buy[19] = 0;
										$avg_count_buy[20] = 0;
										$avg_count_buy[21] = 0;
										
										
										$first_buy =false;
										//$last_buy =false;
										foreach($buy as $key => $single_file) { 									
											$exit_or =0;
											$datecount=0;
											
											$plus_minus_count_buy = $plus_minus_count_buy+$single_file->dt_absolute_performance;
											$curdate = date("Y-m-d", strtotime($buy[$key]->do_date));
											//$keyNextbykey=$key;
											$pluskey=1;
											$nextdatewithminus=$key+1;
											while(isset($buy[$nextdatewithminus]->dt_absolute_performance) && $buy[$nextdatewithminus]->dt_absolute_performance>-0.00000001)
											{
												//$nextminusdate = $buy[$nextdatewithminus]->do_date;
												//exit;
												$pluskey++;
												$nextdatewithminus++;
											}
											
											$nextdate= '';
											if(isset($buy[$nextdatewithminus]->do_date) && !empty($buy[$nextdatewithminus]->do_date)){
												$nextdate= date("Y-m-d", strtotime($buy[$nextdatewithminus]->do_date));
											}
											if($single_file->dt_absolute_performance<0)
											{
												$do_pice_array[$abs_neg_counts]=$single_file->do_price;
												$abs_neg_counts++;
												
											}											
											$average_check = 0;
											if(!empty($do_pice_array)){
												$average_check = array_sum($do_pice_array) / count($do_pice_array);
											}
											while($curdate==$nextdate) {
												//if($buy[$key+$datecount]->dt_absolute_performance>0)
													//continue;
												$nextdate=date("Y-m-d", strtotime($buy[$key+$datecount]->do_date));
												$datecount++;
											}
											$exit_date = "Not exited";
											if($nextdate=="1970-01-01")
											{
												//$date = DateTime::createFromFormat("Y-m-d", $currentdate);
												$pieces = explode("-", $curdate);
												//$year = $date->format("Y");
												$nextdate = $pieces[0] . '-12-31';
												//print_r($pieces);
												//$nextdate = $pieces[0];
											}
											$query=$this->appmodel->getHighLowDataByDate($curdate,$nextdate,$single_file->file_id);
											$query2=$this->appmodel->getHighLowDataByDate2($curdate,$nextdate,$single_file->file_id);
											foreach($query as $key1 => $data) {									
												if($data->hl_high>$average_check)
												{
													$exit_date = $data->hl_date;
													$exit_or =true;
													$abs_neg_counts=0;
													$count_perfact = 0;
													if(!empty($do_pice_array)){
														$count_perfact = count($do_pice_array);
													}
													unset($do_pice_array);
													break;
												}
											}
											
											if($single_file->dt_absolute_performance>=0)
											{
												$buy_plus_arr[$buy_plus_cnt]['dt_absolute_performance'] = $single_file->dt_absolute_performance;
												$positive_count_buy = $positive_count_buy+$single_file->dt_absolute_performance;
												$buy_plus_cnt++;
												continue;
											}
											else
											{
												$buy_minus_arr[$buy_minus_cnt]['dt_absolute_performance'] = $single_file->dt_absolute_performance;
												$buy_minus_arr[$buy_minus_cnt]['do_price'] = $single_file->do_price;
												$buy_minus_total_price = $buy_minus_total_price+$single_file->do_price;
												
												$negative_count_buy = $negative_count_buy+$single_file->dt_absolute_performance;
												if(!$first_buy)
												{
													$first_abs_performance = $single_file->dt_absolute_performance;
													$first_buy = true;
												}												
											}
											$positive_count_buy_lot = $positive_count_buy * $pl_lot_price;
											?>
											<tr>
												<td style="display:none;"><?=$cnt;?></td>
												<td nowrap><?=date("Y-m-d", strtotime($buy[$key]->do_date));?></td>
												<!--<td><?=$single_file->do_price."::::".$key."+".$pluskey."::".$nextdatewithminus;?></td>-->
												<td><?=$single_file->do_price;?></td>
												<td><?php
													echo $single_file->dt_absolute_performance."<br>";
													?></td>
												<td><?=round($average_check,2);?></td>
												<!--<td><?=count($do_pice_array);?>-->
												<td><?php
													if($exit_or)
													{
														$buy_minus_arr[$buy_minus_cnt]['exit_count'] = $count_perfact;
														//$buy_minus_cnt++;
														echo $count_perfact;
														$count_avg = $count_perfact;
														if($count_perfact>20)
															$avg_count_buy[21] = $avg_count_buy[$count_perfact]+1;
														else
															$avg_count_buy[$count_perfact] = $avg_count_buy[$count_perfact]+1;
													}
													else
													{
														$buy_minus_arr[$buy_minus_cnt]['not_exit_count'] = 1;
														//$buy_minus_cnt++;
														echo count($do_pice_array);
														$count_avg = count($do_pice_array);
														$buy_minus_arr[$buy_minus_cnt]['exit'] = $count_avg;
													}
													$buy_minus_cnt++;
												?>
												</td>
												<!--<td><?=$query[0]->hl_high;?></td>										-->
												<td rowspan="2">
													Exit : <?=$exit_date;?>
													<?php
													if($datefilter==1){ ?>
													<table border=1 style="width:100%;">
														<tr>
															<td>Date</td>
															<td>High</td>
															<td>Low</td>
															<td>Close</td>
															<td>PL</td>
															<td>+ & PL</td>
														</tr>
														<?php
														foreach($query2 as $key1 => $data) {
															if($data->hl_date >= $exit_date)
															{
																continue;
															}
															$count_lot = ($data->hl_close-$average_check)*$count_avg*$pl_lot_price;
															echo "<tr>";
															echo "<td nowrap hl-id='".$key1."' class='hl_date'>".$data->hl_date."</td>";
															echo "<td hl-id='".$key1."' class='hl_high'>".$data->hl_high."</td>";
															echo "<td hl-id='".$key1."' class='hl_low'>".$data->hl_low."</td>";
															echo "<td hl-id='".$key1."' class='hl_close'>".$data->hl_close."</td>";
															if($data->hl_date >= $exit_date)
															{
																echo "<td hl-id='".$key1."' class='hl_count'></td>";
																echo "<td hl-id='".$key1."' class='hl_count'></td>";
															}
															else
															{
																if(round($count_lot,2)<=0)
																{
																	$color_all1 = "red";
																}
																else
																{
																	$color_all1 = "blue";
																}

																if(($positive_count_buy_lot)+round($count_lot,2)<=0)
																{
																	$color_all2 = "red";
																}
																else
																{
																	$color_all2 = "blue";
																}
																echo "<td hl-id='".$key1."' class='hl_count' style='color:".$color_all1."'><b>".round($count_lot,2)."</b></td>";
																echo "<td hl-id='".$key1."' class='hl_count_lot' style='color:".$color_all2."'><b>".(($positive_count_buy_lot)+round($count_lot,2))."</b></td>";
															}
															echo "</tr>";
														}
														?>
													</table>
													<?php } ?>
												</td>
											</tr>
											<tr>
												<td colspan="5"><?php
													echo "<span style='color:blue; font-weight: bold'> (+) : ".$positive_count_buy_lot."</span>";
													//echo " >>>> <span style='color:red; font-weight: bold'>  (-) : ".$negative_count_buy."</span>";
													//echo " >>>> <span style='font-weight: bold'>(-/+) : ".$plus_minus_count_buy."</span>";
													?>
												</td>
											</tr>
										<?php
										$cnt++;
									}
									//print_r($avg_count_buy);
									?>
									</tbody>
								</table>
								<?php
									//if(buy_minus_arr)
									$buy_last_elem = end($buy_minus_arr);
									if(isset($buy_last_elem['not_exit_count']) && $buy_last_elem['not_exit_count']==1)
									{
										if(isset($buy_last_elem['count'])){
											if(isset($buy_last_elem['count']) && $buy_last_elem['count']>20)
												$avg_count_buy[21] = $avg_count_buy[21]+1;
											else
												$avg_count_buy[$buy_last_elem['count']] = $avg_count_buy[$buy_last_elem['count']]+1;
										}
									}
								?>
								<table id="example33" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th style="display:none;">Index</th>
											<th>1</th>
											<th>2</th>
											<th>3</th>
											<th>4</th>
											<th>5</th>
											<th>6</th>
											<th>7</th>
											<th>8</th>
											<th>9</th>
											<th>10</th>
											<th>11</th>
											<th>12</th>
											<th>13</th>
											<th>14</th>
											<th>15</th>
											<th>16</th>
											<th>17</th>
											<th>18</th>
											<th>19</th>
											<th>20</th>
											<th>20+</th>
										</tr>
									</thead>
									<tbody>
										<tr>											
											<td class="blue-madison"><?=$avg_count_buy[1];?></td>
											<td class="blue-madison"><?=$avg_count_buy[2];?></td>
											<td class="blue-madison"><?=$avg_count_buy[3];?></td>
											<td class="blue-madison"><?=$avg_count_buy[4];?></td>
											<td class="blue-madison"><?=$avg_count_buy[5];?></td>
											<td class="blue-madison"><?=$avg_count_buy[6];?></td>
											<td class="blue-madison"><?=$avg_count_buy[7];?></td>
											<td class="blue-madison"><?=$avg_count_buy[8];?></td>
											<td class="blue-madison"><?=$avg_count_buy[9];?></td>
											<td class="blue-madison"><?=$avg_count_buy[10];?></td>
											<td class="blue-madison"><?=$avg_count_buy[11];?></td>
											<td class="blue-madison"><?=$avg_count_buy[12];?></td>
											<td class="blue-madison"><?=$avg_count_buy[13];?></td>
											<td class="blue-madison"><?=$avg_count_buy[14];?></td>
											<td class="blue-madison"><?=$avg_count_buy[15];?></td>
											<td class="blue-madison"><?=$avg_count_buy[16];?></td>
											<td class="blue-madison"><?=$avg_count_buy[17];?></td>
											<td class="blue-madison"><?=$avg_count_buy[18];?></td>
											<td class="blue-madison"><?=$avg_count_buy[19];?></td>
											<td class="blue-madison"><?=$avg_count_buy[20];?></td>
											<td class="blue-madison"><?=$avg_count_buy[21];?></td>
										</tr>
									</tbody>
								</table>
								<?php 
									$data_file['count_buy']=serialize($avg_count_buy); 
								?>
								
								<table id="example3" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th style="display:none;">Index</th>
											<th>1-5</th>
											<th>6-7</th>
											<th>8-10</th>
											<th>11-15</th>
											<th>16-20</th>
											<th>20+</th>
											<!--<th>Average</th>-->
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><?=$avg_count_buy[1]+$avg_count_buy[2]+$avg_count_buy[3]+$avg_count_buy[4]+$avg_count_buy[5];?></td>
											<td><?=$avg_count_buy[6]+$avg_count_buy[7];?></td>
											<td><?=$avg_count_buy[8]+$avg_count_buy[9]+$avg_count_buy[10];?></td>
											<td><?=$avg_count_buy[11]+$avg_count_buy[12]+$avg_count_buy[13]+$avg_count_buy[14]+$avg_count_buy[15];?></td>
											<td><?=$avg_count_buy[16]+$avg_count_buy[17]+$avg_count_buy[18]+$avg_count_buy[19]+$avg_count_buy[20];?></td>
											<td><?=$avg_count_buy[21];?></td>											
										</tr>
									</tbody>
								</table>
								<?php
									//print_r(reset($buy_minus_arr));
								?>
							</div>
							<!-- /.box-body -->
						</div>
						
					</td>
					<td valign="top">
						<div class="box box-info box-solid">
							<div class="box-header with-border">
							  <h3 class="box-title"> Short (<?php echo $filename->file_name; ?>)</h3>
							  <div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
								</button>
							  </div>
							  <!-- /.box-tools -->
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive" style="display: block;">
								<table id="example1" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th style="display:none;">Index</th>
											<th>Date</th>
											<th>Price</th>
											<th>Entry</th>
											<th>Avg</th>
											<th>Count</th>
											<th>Info</th>
											<!--<th>Average</th>-->
										</tr>
									</thead>
									<tbody>
										<?php 
										$abs_neg_counts=0;
										$do_pice_array = array();
										
										$positive_count_short = 0;
										$plus_minus_count_short = 0;
										$negative_count_short = 0;
										
										$short_minus_arr = array();
										$short_minus_cnt = 0;
										$short_plus_arr = array();
										$short_plus_cnt = 0;
										
										$short_minus_total_price = 0;
										
										$cnt = 0;
										$avg_count_short[1] = 0;
										$avg_count_short[2] = 0;
										$avg_count_short[3] = 0;
										$avg_count_short[4] = 0;
										$avg_count_short[5] = 0;
										$avg_count_short[6] = 0;
										$avg_count_short[7] = 0;
										$avg_count_short[8] = 0;
										$avg_count_short[9] = 0;
										$avg_count_short[10] = 0;
										$avg_count_short[11] = 0;
										$avg_count_short[12] = 0;
										$avg_count_short[13] = 0;
										$avg_count_short[14] = 0;
										$avg_count_short[15] = 0;
										$avg_count_short[16] = 0;
										$avg_count_short[17] = 0;
										$avg_count_short[18] = 0;
										$avg_count_short[19] = 0;
										$avg_count_short[20] = 0;
										$avg_count_short[21] = 0;
										
										/*echo "<pre>";
										print_r($short);
										echo "</pre>";*/
									
										foreach($short as $key => $single_file) { 									
											$exit_or =0;
											$datecount=0;
											
											$plus_minus_count_short = $plus_minus_count_short+$single_file->dt_absolute_performance;
											
											$curdate = date("Y-m-d", strtotime($short[$key]->do_date));
											//$keyNextbykey=$key;
											$pluskey=1;
											$nextdatewithminus=$key+1;
											while(isset($short[$nextdatewithminus]->dt_absolute_performance) && $short[$nextdatewithminus]->dt_absolute_performance>-0.00000001)
											{
												//$nextminusdate = $short[$nextdatewithminus]->do_date;
												//exit;
												$pluskey++;
												$nextdatewithminus++;
											}
											
											$nextdate= '';
											if(isset($short[$nextdatewithminus]->do_date) && !empty($short[$nextdatewithminus]->do_date)){
												$nextdate= date("Y-m-d", strtotime($short[$nextdatewithminus]->do_date));
											}
											if($single_file->dt_absolute_performance<0)
											{
												$do_pice_array[$abs_neg_counts]=$single_file->do_price;
												$abs_neg_counts++;
											}
											$average_check = 0;
											if(!empty($do_pice_array)){
												$average_check = array_sum($do_pice_array) / count($do_pice_array);
											}
																		
											while($curdate==$nextdate) {
												//if($short[$key+$datecount]->dt_absolute_performance>0)
													//continue;
												$nextdate=date("Y-m-d", strtotime($short[$key+$datecount]->do_date));
												$datecount++;
											} 
											$exit_date = "Not exited";
											if($nextdate=="1970-01-01")
											{
												//$date = DateTime::createFromFormat("Y-m-d", $currentdate);
												$pieces = explode("-", $curdate);
												//$year = $date->format("Y");
												$nextdate = $pieces[0] . '-12-31';
												//print_r($pieces);
												//$nextdate = $pieces[0];
											}
											$query=$this->appmodel->getHighLowDataByDate($curdate,$nextdate,$single_file->file_id);
											$query2=$this->appmodel->getHighLowDataByDate2($curdate,$nextdate,$single_file->file_id);
											foreach($query as $key1 => $data) {
												if($data->hl_low<$average_check)
												{
													$exit_date = $data->hl_date;
													$exit_or =true;
													$abs_neg_counts=0;
													$count_perfact2 = count($do_pice_array);
													unset($do_pice_array);
													break;
												}
											}
											
											if($single_file->dt_absolute_performance>=0)
											{
												$short_plus_arr[$short_plus_cnt]['dt_absolute_performance'] = $single_file->dt_absolute_performance;
												$short_plus_cnt++;
												$positive_count_short = $positive_count_short+$single_file->dt_absolute_performance;
												continue;
											}
											else
											{
												$short_minus_arr[$short_minus_cnt]['dt_absolute_performance']=  $single_file->dt_absolute_performance;
												$short_minus_arr[$short_minus_cnt]['do_price']=  $single_file->do_price;
												$short_minus_total_price = $short_minus_total_price+$single_file->do_price;
																								
												$negative_count_short = $negative_count_short+$single_file->dt_absolute_performance;
											}
											$positive_count_short_lot = $positive_count_short * $pl_lot_price;
											//echo $nextdate;
											?>
											<tr>
												<td style="display:none;"><?=$cnt;?></td>
												<td nowrap><?=date("Y-m-d", strtotime($short[$key]->do_date));?></td>
												<!--<td><?=$single_file->do_price."::::".$key."+".$pluskey."::".$nextdatewithminus;?></td>-->
												<td><?=$single_file->do_price;?></td>
												<td><?php
													echo $single_file->dt_absolute_performance."<br>";													
													?></td>
												<td><?=round($average_check,2);?></td>
												<!--<td><?=count($do_pice_array);?>-->
												<td><?php
												
													if($exit_or)
													{
														$short_minus_arr[$short_minus_cnt]['exit_count'] = $count_perfact2;
														$short_minus_cnt++;
														if($count_perfact2>20)
															$avg_count_short[21] = $avg_count_short[$count_perfact2]+1;
														else			
															$avg_count_short[$count_perfact2] = $avg_count_short[$count_perfact2]+1;
															
														$count_avg = $count_perfact2;
														echo $count_perfact2;
													}
													else
													{	
														$short_minus_arr[$short_minus_cnt]['not_exit_count'] = 1;
														$short_minus_arr[$short_minus_cnt]['count'] = $count_avg;
														
														$short_minus_cnt++;														
														$count_avg = count($do_pice_array);
														echo count($do_pice_array);
													}
												?>
												<?php //print_r($do_pice_array);?>
												</td>
												<!--<td><?=$query[0]->hl_high;?></td>										-->
												<td rowspan="2">
													Exit : <?=$exit_date;?>
													<?php 
													if($datefilter==1){ ?>
													<table border=1 style="width:100%;">
														<tr>
															<td>Date</td>
															<td>High</td>
															<td>Low</td>
															<td>Close</td>
															<td>PL</td>
															<td>+ & PL</td>
														</tr>
														<?php
														foreach($query2 as $key1 => $data) {
															if($data->hl_date >= $exit_date)
															{
																continue;
															}
															$count_lot = ($average_check-$data->hl_close)*$count_avg*$pl_lot_price;
															echo "<tr>";
															echo "<td nowrap>".$data->hl_date."</td>";
															echo "<td>".$data->hl_high."</td>";
															echo "<td>".$data->hl_low."</td>";
															echo "<td hl-id='".$key1."' class='hl_close'>".$data->hl_close."</td>";
															/*if($exit_or)
																echo "<td hl-id='".$key1."' class='hl_count'></td>";
															else
																echo "<td hl-id='".$key1."' class='hl_count'>".round($count_lot,2)."</td>";*/
															if($data->hl_date >= $exit_date)
															{
																echo "<td hl-id='".$key1."' class='hl_count'></td>";
																echo "<td hl-id='".$key1."' class='hl_count'></td>";
															}
															else
															{
																if(round($count_lot,2)<=0)
																{
																	$color_all1 = "red";
																}
																else
																{
																	$color_all1 = "blue";
																}

																if(($positive_count_short_lot)+round($count_lot,2)<=0)
																{
																	$color_all2 = "red";
																}
																else
																{
																	$color_all2 = "blue";
																}
																echo "<td hl-id='".$key1."' class='hl_count' style='color:".$color_all1."'><b>".round($count_lot,2)."</b></td>";
																echo "<td hl-id='".$key1."' class='hl_count_lot' style='color:".$color_all2."'><b>".(($positive_count_short_lot)+round($count_lot,2))."</b></td>";
															}
															echo "</tr>";
														}
														?>
													</table>
													<?php } ?>
												</td>
											</tr>
											<tr>
												<td colspan="5"><?php											
													echo "<span style='color:blue; font-weight: bold'> (+) : ".$positive_count_short_lot."</span>";
													//echo " >>>> <span style='color:red; font-weight: bold'>  (-) : ".$negative_count_short."</span>";
													//echo " >>>> <span style='font-weight: bold'>(-/+) : ".$plus_minus_count_short."</span>";
													?>
												</td>
											</tr>
										<?php
										$cnt++;
									}
									//print_r($avg_count_short);
									?>
									</tbody>
								</table>
								<?php
									$short_last_elem = end($short_minus_arr);
									if(isset($short_last_elem['not_exit_count']) && $short_last_elem['not_exit_count']==1)
									{
										if($short_last_elem['count']>20)
											$avg_count_short[21] = $avg_count_short[21]+1;
										else
											$avg_count_short[$short_last_elem['count']] = $avg_count_short[$short_last_elem['count']]+1;
									}
								?>
								<table id="example33" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th style="display:none;">Index</th>
											<th>1</th>
											<th>2</th>
											<th>3</th>
											<th>4</th>
											<th>5</th>
											<th>6</th>
											<th>7</th>
											<th>8</th>
											<th>9</th>
											<th>10</th>
											<th>11</th>
											<th>12</th>
											<th>13</th>
											<th>14</th>
											<th>15</th>
											<th>16</th>
											<th>17</th>
											<th>18</th>
											<th>19</th>
											<th>20</th>
											<th>20+</th>
										</tr>
									</thead>
									<tbody>
										<tr>											
											<td class="blue-madison"><?=$avg_count_short[1];?></td>
											<td class="blue-madison"><?=$avg_count_short[2];?></td>
											<td class="blue-madison"><?=$avg_count_short[3];?></td>
											<td class="blue-madison"><?=$avg_count_short[4];?></td>
											<td class="blue-madison"><?=$avg_count_short[5];?></td>
											<td class="blue-madison"><?=$avg_count_short[6];?></td>
											<td class="blue-madison"><?=$avg_count_short[7];?></td>
											<td class="blue-madison"><?=$avg_count_short[8];?></td>
											<td class="blue-madison"><?=$avg_count_short[9];?></td>
											<td class="blue-madison"><?=$avg_count_short[10];?></td>
											<td class="blue-madison"><?=$avg_count_short[11];?></td>
											<td class="blue-madison"><?=$avg_count_short[12];?></td>
											<td class="blue-madison"><?=$avg_count_short[13];?></td>
											<td class="blue-madison"><?=$avg_count_short[14];?></td>
											<td class="blue-madison"><?=$avg_count_short[15];?></td>
											<td class="blue-madison"><?=$avg_count_short[16];?></td>
											<td class="blue-madison"><?=$avg_count_short[17];?></td>
											<td class="blue-madison"><?=$avg_count_short[18];?></td>
											<td class="blue-madison"><?=$avg_count_short[19];?></td>
											<td class="blue-madison"><?=$avg_count_short[20];?></td>
											<td class="blue-madison"><?=$avg_count_short[21];?></td>
										</tr>
									</tbody>
								</table>
								<table id="example4" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th style="display:none;">Index</th>
											<th>1-5</th>
											<th>6-7</th>
											<th>8-10</th>
											<th>11-15</th>
											<th>16-20</th>
											<th>20+</th>
											<!--<th>Average</th>-->
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><?=$avg_count_short[1]+$avg_count_short[2]+$avg_count_short[3]+$avg_count_short[4]+$avg_count_short[5];?></td>
											<td><?=$avg_count_short[6]+$avg_count_short[7];?></td>
											<td><?=$avg_count_short[8]+$avg_count_short[9]+$avg_count_short[10];?></td>
											<td><?=$avg_count_short[11]+$avg_count_short[12]+$avg_count_short[13]+$avg_count_short[14]+$avg_count_short[15];?></td>
											<td><?=$avg_count_short[16]+$avg_count_short[17]+$avg_count_short[18]+$avg_count_short[19]+$avg_count_short[20];?></td>
											<td><?=$avg_count_short[21];?></td>
										</tr>
									</tbody>
								</table>
							</div>
							<?php
								$data_file['count_short']=serialize($avg_count_short);
							?>
							<!-- /.box-body -->
						</div>
					</td>
				</tr>
			</table> 
			<?php
				//print_r(end($buy_minus_arr));
				//print_r(end($short_minus_arr));
			?>
			<div id="myPopoverContent">
				<table border="1" style="width: 50%;float: left;" id="statistics" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Statistics Text</th>
							<th>All Trades</th>
							<th>Long Trades</th>
							<th>Short Trades</th>							
						</tr>
					</thead>
					<tbody>
					<?php
					foreach($statistics as $key => $single_statistics) {
						if($single_statistics->statistics_text=="Total Profit")
						{
							$total_profit = $single_statistics->all_trades;
							$long_profit = $single_statistics->long_trades;
							$short_profit = $single_statistics->short_trades;
						}
						echo "<tr>";
						echo "<td>".$single_statistics->statistics_text."</td>";
						if(!$single_statistics->isNA){
							echo "<td>".$single_statistics->all_trades."</td>";
							echo "<td>".$single_statistics->long_trades."</td>";
							echo "<td>".$single_statistics->short_trades."</td>";
						}
						else
						{
							echo "<td>N/A</td>";
							echo "<td>N/A</td>";
							echo "<td>N/A</td>";
						}
						echo "</tr>";
					}
					?>
						<tr>
							<td colspan=4><a href="#" class="btn btn-primary goto-top">
								<i class="fa fa-table" aria-hidden="true"></i>
									Go To Top
								</a>
							</td>
						</tr>
					</tbody>
				</table>
				<table border="1" style="width: 45%;float: right;" id="custom-statistics" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>
								<a href="#" class="btn btn-primary goto-top">
									<i class="fa fa-table" aria-hidden="true"></i>
									Go To Top
								</a>
							</th>
							<th>All</th>
							<th>Long</th>
							<th>Short</th>							
						</tr>
					</thead>
					<tbody>
					<?php
						//Buy
						
						$first_buy_minus = reset($buy_minus_arr);
						$last_buy_minus = end($buy_minus_arr);
						//print_r($first_minus);
						//print_r($last_buy_minus);
						$buy_first_percentage=$total_profit/$first_buy_minus['do_price'];
						$data_file['buy_first_percentage'] = $buy_first_percentage;
						$buy_first_percentage_long=$long_profit/$first_buy_minus['do_price'];
						$data_file['buy_first_percentage_long'] = $buy_first_percentage_long;
						$buy_first_percentage_short=$short_profit/$first_buy_minus['do_price'];
						$data_file['buy_first_percentage_short'] = $buy_first_percentage_short;
						
						echo "<tr>";
						//echo "<td>1st % =(Total Profit->All Trades / Buy First price)</td>";
						echo "<td>Buy 1st</td>";
						echo "<td>".round($buy_first_percentage,2)."</td>";
						echo "<td>".round($buy_first_percentage_long,2)."</td>";
						echo "<td>".round($buy_first_percentage_short,2)."</td>";
						echo "</tr>";
						
						$buy_average_percentage=$total_profit/($buy_minus_total_price/$buy_minus_cnt);
						$data_file['buy_average_percentage'] = $buy_average_percentage;
						$buy_average_percentage_long=$long_profit/($buy_minus_total_price/$buy_minus_cnt);
						$data_file['buy_average_percentage_long'] = $buy_average_percentage_long;
						$buy_average_percentage_short=$short_profit/($buy_minus_total_price/$buy_minus_cnt);
						$data_file['buy_average_percentage_short'] = $buy_average_percentage_short;
						
						echo "<tr>";
						//echo "<td>Avg % = (Total Profit->All Trades / [Buy total price/Count])</td>";
						echo "<td>Buy Avg</td>";
						echo "<td>".round($buy_average_percentage,2)."</td>";
						echo "<td>".round($buy_average_percentage_long,2)."</td>";
						echo "<td>".round($buy_average_percentage_short,2)."</td>";
						//echo "<td>".$buy_average_percentage."</td>";
						echo "</tr>";
						
						$buy_last_percentage=$total_profit/$last_buy_minus['do_price'];
						$data_file['buy_last_percentage'] = $buy_last_percentage;
						$buy_last_percentage_long=$long_profit/$last_buy_minus['do_price'];
						$data_file['buy_last_percentage_long'] = $buy_last_percentage_long;
						$buy_last_percentage_short=$short_profit/$last_buy_minus['do_price'];
						$data_file['buy_last_percentage_short'] = $buy_last_percentage_short;
						//$buy_last_percentage=($buy_minus_total_price/$short_minus_cnt)/$total_profit;
						echo "<tr>";
						//echo "<td>Last % = (Total Profit->All Trades / Buy last price)</td>";
						echo "<td>Buy Last</td>";
						echo "<td>".round($buy_last_percentage,2)."</td>";
						echo "<td>".round($buy_last_percentage_long,2)."</td>";
						echo "<td>".round($buy_last_percentage_short,2)."</td>";
						echo "</tr>";
						//Short
						$first_short_minus = reset($short_minus_arr);
						$last_short_minus = end($short_minus_arr);
						//print_r($first_short_minus);
						$short_first_percentage=$total_profit/$first_short_minus['do_price'];
						$data_file['short_first_percentage'] = $short_first_percentage;
						$short_first_percentage_long=$long_profit/$first_short_minus['do_price'];
						$data_file['short_first_percentage_long'] = $short_first_percentage_long;
						$short_first_percentage_short=$short_profit/$first_short_minus['do_price'];
						$data_file['short_first_percentage_short'] = $short_first_percentage_short;
						echo "<tr>";
						//echo "<td>1st % =(Total Profit->All Trades / Short First price)</td>";
						echo "<td>Short 1st</td>";
						echo "<td>".round($short_first_percentage,2)."</td>";
						echo "<td>".round($short_first_percentage_long,2)."</td>";
						echo "<td>".round($short_first_percentage_short,2)."</td>";
						echo "</tr>";
						
						$short_average_percentage=$total_profit/($short_minus_total_price/$short_minus_cnt);
						$data_file['short_average_percentage'] = $short_average_percentage;
						$short_average_percentage_long=$long_profit/($short_minus_total_price/$short_minus_cnt);
						$data_file['short_average_percentage_long'] = $short_average_percentage_long;
						$short_average_percentage_short=$short_profit/($short_minus_total_price/$short_minus_cnt);
						$data_file['short_average_percentage_short'] = $short_average_percentage_short;
						echo "<tr>";
						//echo "<td>Avg % = (Total Profit->All Trades / [Short total price/Count])</td>";
						echo "<td>Short Avg</td>";
						echo "<td>".round($short_average_percentage,2)."</td>";
						echo "<td>".round($short_average_percentage_long,2)."</td>";
						echo "<td>".round($short_average_percentage_short,2)."</td>";						
						//echo "<td>".$short_average_percentage."</td>";
						echo "</tr>";
						
						$short_last_percentage=$total_profit/$last_short_minus['do_price'];
						$data_file['short_last_percentage'] = $short_last_percentage;
						$short_last_percentage_long=$long_profit/$last_short_minus['do_price'];
						$data_file['short_last_percentage_long'] = $short_last_percentage_long;
						$short_last_percentage_short=$short_profit/$last_short_minus['do_price'];
						$data_file['short_last_percentage_short'] = $short_last_percentage_short;
						
						echo "<tr>";
						//echo "<td>Last % = (Total Profit->All Trades / Short last price)</td>";
						echo "<td>Short Last</td>";
						echo "<td>".round($short_last_percentage,2)."</td>";
						echo "<td>".round($short_last_percentage_long,2)."</td>";
						echo "<td>".round($short_last_percentage_short,2)."</td>";
						echo "</tr>";
					?>
						
					</tbody>
				</table>
				
				<!--<button class="btn btn-box-tool goto-top">Top</button>-->
			</div>
		</div>		
    </section>
   
    <!-- /.content -->
  </div>
  <?php
	$data_file['file_id']=$hl_file_id;
	$query=$this->filemodel->updateFile($data_file);
  ?>
  <!-- /.content-wrapper -->

<!-- bootstrap datepicker -->
<script src="<?=base_url('assets/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
<script>
    $(document).ready(function(){
		/*$("#example").dataTable({"paging": false, "order": [[ 0, "asc" ]],"aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [ 1,2,3,4], }
       ]});
	   
        var datatable = $("#example").dataTable();
        
        
        $('#example tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                datatable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        
        $("#example1").dataTable({"paging": false, "order": [[ 0, "asc" ]],"aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [ 1,2,3,4], }
       ]});
	   
        var datatable = $("#example1").dataTable();
        
        
        $('#example1 tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                datatable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        
		$("#statistics").dataTable({"paging": false,"aoColumnDefs": [
			{ 'bSortable': false, 'aTargets': [ 1,2,3], }
		]});
	   
		var datatable = $("#statistics").dataTable();
        
        
		$('#statistics tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
			}
			else {
				datatable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
			}
        });*/
    });
    
</script>
<script type="text/javascript">
$(document).ready(function() {
	
    $("#average").click(function(event) {
        event.preventDefault();
        var fromDate = $("input#fromDate").val();
        var toDate = $("input#toDate").val();
        
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "average/getAverage",
           
            data: {fromDate: fromDate, toDate: toDate},
            success: function() {
            
            }
        });
    });
    
    $(".goto-top").click(function(event){
		$('html, body').animate({ scrollTop: 0 }, 'slow');
	});
	/*$('[data-toggle=popover]').popover({
		content: $('#myPopoverContent').html(),
		html: true
	}).mouseover(function() {
		$(this).popover('show');
	}).mouseleave(function() {
		$(this).popover('hide');
	});*/
});
</script>
