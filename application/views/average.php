<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url('assets/plugins/datepicker/datepicker3.css');?>">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Average
        <a href="<?=base_url();?>" class="btn btn-primary waves-effect waves-light pull-right"><i class="fa fa-dashboard"></i>&nbsp; Go To Dashboard</a>
      </h1>
    </section>	
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
				 <div class="box box-info box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title"> Buy (<?php echo $filename->file_name; ?>)</h3>
                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                      <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive" style="display: block;">
                        <table id="example" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="display:none;">Index</th>
                                    <th>Date</th>
                                    <th>Price</th>
                                    <th>Entry</th>
									<th>Low</th>
                                    <th>price or AVG</th>
                                </tr>
                            </thead>
                            <tbody>
								<?php 
								$abs_neg_counts=0;
								$do_pice_array = array();
								$cnt = 0;
								$watch_price = 0;
								$watch_avg = 0;
								$avg =0;
								//print_r($buy);
								foreach($buy as $key => $single_file) { 
									$mysql_date = date("Y-m-d", strtotime($buy[$key+1]->do_date));
									$query=$this->appmodel->getHighLowDataByDate($mysql_date,$single_file->file_id);
									?>
									<tr>
										
									<?php
									if($single_file->dt_absolute_performance<0)
									{
										/*if price is greater than next date low then do
										 * $abs_neg_counts=1;
										 * $ifAbsIs_and_above_low = 1;
										 */
										if($single_file->do_price>=$query[0]->hl_low)
										{
											//continue;
											$watch_price=0;
											$watch_avg = 1;
											$avg = $single_file->do_price;
										}
										else
										{
											$abs_neg_counts++;
											$watch_price=0;
											$watch_avg = 0;
										}
										?>
										
										
										<td><?=$single_file->do_date;?></td>
										<td><?=$single_file->do_price;?></td>
										<td><?=$abs_neg_counts;?></td>
										<td><?=$query[0]->hl_low;?></td>
										<td><?=$watch_price.":---".$watch_avg;?></td>
										<?php
									}
									
									
									
									//print_r($buy[$key+1]);
									//$next_val = $buy[$key+1];
									/*if(isset($buy[$key+1]->do_date) && $buy[$key+1]->do_date != '')
									{	
										$mysql_date = date("Y-m-d", strtotime($buy[$key+1]->do_date));
									}
									$query=$this->appmodel->getHighLowDataByDate($mysql_date,$single_file->file_id);
									if($query[0]->hl_high>=$single_file->do_price && $query[0]->hl_low<=$single_file->do_price){
										$abs_neg_counts=0;
										unset($do_pice_array);
										//continue;
									}
									else
									{
									}
									if($single_file->dt_absolute_performance<0)
									{										
										$do_pice_array[$abs_neg_counts]=$single_file->do_price;
										//$do_pice_array[$abs_neg_counts+1]=$buy[$key+1]->do_price;
										$abs_neg_counts++;
										$average_of_foo = array_sum($do_pice_array) / count($do_pice_array);
									}									
									else
									{										
										if(isset($average_of_foo))										
										{
											$do_pice_array[$abs_neg_counts]=$single_file->do_price;
											$abs_neg_counts++;
											$average_of_foo = array_sum($do_pice_array) / count($do_pice_array);
										}
										else
										{
											$abs_neg_counts=0;
											unset($do_pice_array);
											continue;
										}
									}
									?>
									<tr>
										<td style="display:none;"><?=$cnt;?></td>
										<td><?php echo $key.":".$single_file->do_date;?></td>
										<td><?=$single_file->do_price;?></td>
										<td><?=$abs_neg_counts;?></td>
										<td><?php
											$diffs = $query[0]->hl_low - $average_of_foo;
											$diffs = $diffs * $abs_neg_counts;
											echo number_format((float)$diffs, 2, '.', ''); ?>
										</td>
										<?php
										if($single_file->do_price==$average_of_foo)
										{
											?>
												<td></td>
											<?php											
										}
										else
										{											
											//$mysql_date = date("Y-m-d", strtotime($single_file->do_date));
											
											if($query)
											{
												if($query[0]->hl_high>=$average_of_foo && $query[0]->hl_low<=$average_of_foo)
												{
												?>											
													<td><?=number_format((float)$average_of_foo, 2, '.', '');?></td>
													</tr>
													<tr>
														<td style="display:none;"><?=$cnt;?></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td>0</td>
												<?php
													$abs_neg_counts=0;
													unset($do_pice_array);
													unset($average_of_foo);	
												}
												else
												{
												?>
													<td><?=number_format((float)$average_of_foo, 2, '.', '');?></td>
												<?php
												}
											}
											else
											{
												echo "<td></td>";
											}
										}
										?>
									</tr>                               
                                <?php
								$cnt++;*/
							}?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- general form elements -->
            </div>
            <!-- /.buy start -->
            <div class="col-md-6">
                <div class="box box-info box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title"> Buy (<?php echo $filename->file_name; ?>)</h3>
                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                      <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive" style="display: block;">
                        <table id="example" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="display:none;">Index</th>
                                    <th>Date</th>
                                    <th>Price</th>
                                    <th>Entry</th>
									<th>Different</th>
                                    <th>Average</th>
                                </tr>
                            </thead>
                            <tbody>
								<?php 
								$abs_neg_counts=0;
								$do_pice_array = array();
								$cnt = 0;
								foreach($buy as $key => $single_file) { 
									if(isset($buy[$key+1]->do_date) && $buy[$key+1]->do_date != '')
									{	
										$mysql_date = date("Y-m-d", strtotime($buy[$key+1]->do_date));
									}
									$query=$this->appmodel->getHighLowDataByDate($mysql_date,$single_file->file_id);
									if($query[0]->hl_high>=$single_file->do_price && $query[0]->hl_low<=$single_file->do_price){
										$abs_neg_counts=0;
										unset($do_pice_array);
										continue;
									}
									if($single_file->dt_absolute_performance<0)
									{										
										$do_pice_array[$abs_neg_counts]=$single_file->do_price;
										//$do_pice_array[$abs_neg_counts+1]=$buy[$key+1]->do_price;
										$abs_neg_counts++;
										$average_of_foo = array_sum($do_pice_array) / count($do_pice_array);
									}									
									else
									{										
										if(isset($average_of_foo))										
										{
											$do_pice_array[$abs_neg_counts]=$single_file->do_price;
											$abs_neg_counts++;
											$average_of_foo = array_sum($do_pice_array) / count($do_pice_array);
										}
										else
										{
											$abs_neg_counts=0;
											unset($do_pice_array);
											continue;
										}
									}
									?>
									<tr>
										<td style="display:none;"><?=$cnt;?></td>
										<td><?=$single_file->do_date;?></td>
										<td><?=$single_file->do_price;?></td>
										<td><?=$abs_neg_counts;?></td>
										<td><?php
											$diffs = $query[0]->hl_low - $average_of_foo;
											$diffs = $diffs * $abs_neg_counts;
											echo number_format((float)$diffs, 2, '.', ''); ?>
										</td>
										<?php
										if($single_file->do_price==$average_of_foo)
										{
											?>
												<td></td>
											<?php											
										}
										else
										{											
											//$mysql_date = date("Y-m-d", strtotime($single_file->do_date));
											
											if($query)
											{
												if($query[0]->hl_high>=$average_of_foo && $query[0]->hl_low<=$average_of_foo)
												{
												?>											
													<td><?=number_format((float)$average_of_foo, 2, '.', '');?></td>
													</tr>
													<tr>
														<td style="display:none;"><?=$cnt;?></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td>0</td>
												<?php
													$abs_neg_counts=0;
													unset($do_pice_array);
													unset($average_of_foo);	
												}
												else
												{
												?>
													<td><?=number_format((float)$average_of_foo, 2, '.', '');?></td>
												<?php
												}
											}
											else
											{
												echo "<td></td>";
											}
										}
										?>
									</tr>                               
                                <?php
								$cnt++;
							}?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.buy End -->
            <!-- /.col -->
            <!-- /.buy start -->
            <div class="col-md-6">
                <div class="box box-info box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title"> Sell (<?php echo $filename->file_name; ?>)</h3>
                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                      <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive" style="display: block;">
                        <table id="example1" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                   <th style="display:none;">Index</th>
                                    <th>Date</th>
                                    <th>Price</th>
                                    <th>Entry</th>
									<th>Different</th>
                                    <th>Average</th>
                                </tr>
                            </thead>
                            <tbody>
							<?php 
								$abs_neg_counts=0;
								$do_pice_array = array();
								$cnt = 0;
								foreach($short as $key => $single_file) { 
																		
									if($single_file->dt_absolute_performance<0)
									{										
										$do_pice_array[$abs_neg_counts]=$single_file->do_price;
										//$do_pice_array[$abs_neg_counts+1]=$buy[$key+1]->do_price;
										$abs_neg_counts++;
										$average_of_foo = array_sum($do_pice_array) / count($do_pice_array);
									}									
									else
									{										
										if(isset($average_of_foo))										
										{
											$do_pice_array[$abs_neg_counts]=$single_file->do_price;
											$abs_neg_counts++;
											$average_of_foo = array_sum($do_pice_array) / count($do_pice_array);
										}
										else
										{
											$abs_neg_counts=0;
											unset($do_pice_array);
											continue;
										}
									}	
									if(isset($short[$key+1]->do_date) && $short[$key+1]->do_date != '')
									{	
										$mysql_date = date("Y-m-d", strtotime($short[$key+1]->do_date));
									}
									$query=$this->appmodel->getHighLowDataByDate($mysql_date,$single_file->file_id);
									?>
									<tr>
										<td style="display:none;"><?=$cnt;?></td>
										<td><?=$single_file->do_date;?></td>
										<td><?=$single_file->do_price;?></td>
										<td><?=$abs_neg_counts;?></td>
										<td><?php
											$diffs = $average_of_foo - $query[0]->hl_high;
											$diffs = $diffs * $abs_neg_counts;
											echo number_format((float)$diffs, 2, '.', ''); ?>
										</td>
										<?php
										if($single_file->do_price==$average_of_foo)
										{
											?>
												<td></td>
											<?php											
										}
										else
										{	
											if($query)
											{
												if($query[0]->hl_high>=$average_of_foo && $query[0]->hl_low<=$average_of_foo)
												{
												?>											
													<td><?=number_format((float)$average_of_foo, 2, '.', '');?></td>
													</tr>
													<tr>
														<td style="display:none;"><?=$cnt;?></td>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td>0</td>
												<?php
													$abs_neg_counts=0;
													unset($do_pice_array);
													unset($average_of_foo);	
												}
												else
												{
												?>
													<td><?=number_format((float)$average_of_foo, 2, '.', '');?></td>
												<?php
												}
											}
											else
											{
												echo "<td></td>";
											}
										}
										?>
									</tr>                               
                                <?php
								$cnt++;
							}?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.buy End -->
            <!-- /.col -->
            
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- bootstrap datepicker -->
<script src="<?=base_url('assets/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
<script>
    $(document).ready(function(){
		$("#example").dataTable({"paging": false, "order": [[ 0, "asc" ]],"aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [ 1,2,3,4], }
       ]});
	   $("#example1").dataTable({"paging": false, "order": [[ 0, "asc" ]],"aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [ 1,2,3,4] }
       ]});
        var datatable = $("#example").dataTable();
        var datatable1 = $("#example1").dataTable();
        
        $('#example tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                datatable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        
        $('#example1 tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                datatable1.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        
        
        $('.datepicker').datepicker({
          autoclose: true
        });
       
    });
    
</script>
<script type="text/javascript">
$(document).ready(function() {
	
    $("#average").click(function(event) {
        event.preventDefault();
        var fromDate = $("input#fromDate").val();
        var toDate = $("input#toDate").val();
        
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "average/getAverage",
           
            data: {fromDate: fromDate, toDate: toDate},
            success: function() {
            
            }
        });
    });
});
</script>

      
