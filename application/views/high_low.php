<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        High / Low
        <!--<a href="party/add" class="btn btn-primary waves-effect waves-light pull-right"><i class="fa fa-plus"></i> Add Party</a>-->
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <form role="form" action="" method="post" enctype="multipart/form-data" id="frm_insert">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Select CSV File For Import</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="form-group">
                                <input type="file" id="exampleInputFile" name="file">
                                <p class="help-block"><?php ?></p>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" name="import" id="import">Import</button>
                        </div>
                        <!-- Loading (remove the following to stop the loading)-->
                        <div class="overlay" id="overlay" style="">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <!-- end loading -->
                    </div>
                    <!-- /.box -->
                </form>
            </div>
          
          <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-body">
                    <p>
                        <b>File Name Samples.</b><br />
                        <small>
                            ITEMNAME-ddmmyyyy-hhmm-highlow.csv<br />
                            ITEMNAME-ddmmyyyy-1d-highlow.csv<br />
                            ITEMNAME-ddmmyyyy-1w-highlow.csv<br />
                            ITEMNAME-ddmmyyyy-1m-highlow.csv<br />
                            ITEMNAME-ddmmyyyy-1y-highlow.csv<br />
                        </small>
                    </p>
                </div>
              </div>
              <!-- /.box -->
          </div>
        </div>  
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-body">
                        <label for="form-address">Select File</label>          
                        <div class="input-group input-group-sm">
                            <select name="file_id"  id="file_id" class="select2" style="width: 100%;">
                                <option value="" selected="selected" >- Select file -</option>
                                <?php foreach($filelist as $row): ?>
									<option value="<?php echo $row->file_id; ?>"><?php echo $row->file_name; ?></option>
                                <?php endforeach; ?> 
                            </select>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-info btn-flat" onclick="getdatatable()">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
        </div>      
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Table</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-striped table-bordered example1">
                            <thead>
                                <tr>
                                    <th>High/Low Date</th>
                                    <th>High/Low Time</th>
                                    <th>Open</th>
                                    <th>High</th>
                                    <th>Low</th>
                                    <th>Close</th>
                                    <th>Back Test Signal</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">High-low Files</h3>
                    </div>
                    <form id="frm-example" action="" method="POST">
                        <div style="margin: 10px 15px;">
                            <button class="btn btn-primary" type="submit">Delete Selected </button>
                        </div>
                    <div class="box-body table-responsive">
                        <table id="example" class="table table-striped table-bordered display select" >
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="select_all" value="1"></th>
                                <th>File Name</th>
                                <th>Date</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach($records as $row)
                            {
                            ?>
                            <tr>
                                <td><?php echo $row->file_id;?></td>
                                <td><?=$row->file_name; ?></td>
                                <td><?=$row->currentdate; ?></td>
                                <!--<td><button type="button" onclick="fileId(<?=$row->file_id;?>)" class="btn btn-primary">Average</button></td>-->
                                <td> 
                                    <a href="javascript:void(0)" onclick="deleteFile(<?=$row->file_id;?>)" class="btn btn-primary"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script>
   
    $(document).ready(function(){
		//$(".select2").select2();
        $('#overlay').hide();
        
        $('.example1').DataTable();
        var datatable = $(".example1").dataTable();
        $('.example1 tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                datatable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );
        
        
        $("#frm_insert").submit(function(event){
            event.preventDefault();
            var postData = new FormData(this);
            $.ajax({
                    url:'<?php echo base_url();?>highlow/import',
                    type:'POST',
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: postData,
                    beforeSend: function() { $('#overlay').show(); },
                    complete: function() { $('#overlay').hide(); },
                    success:function(response){
                        var json = $.parseJSON(response);
                        if (json['success'] == 'Success') {
                            show_notify('High Low sheet Imported Sucessfully.',true);
                           setInterval('location.reload()', 1000);
                        }
                        if (json['error'] == 'Exist') {
                            show_notify('Already Exist.',false);
                        }
                        if (json['error'] == 'Data') {
                            show_notify('Please Import Data sheet First.',false);
                        }
                        if (json['error'] == 'Notupload') {
                            show_notify('File does not upload.',false);
                        }
                        if (json['error'] == 'Invalid_err') {
                            var msg = json['msg'];
                            show_notify(msg,false);
                        }
                        if (json['error'] == 'Invalid') {
                            show_notify("Invalid file",false);
                        }
                        if (json['error'] == 'Select') {
                            show_notify("Select file",false);
                        }
                        if (json['error'] == 'Only') {
                            show_notify('Please upload only high low sheet.',false);
                        }
                        
                        return false;
                    }
            });
        });
        
    });
    function getdatatable()
    {
        var file_id = $('#file_id').val();
        //alert(file_id);
        if(file_id != ''){
            var table = $('.example1').DataTable({ 
                "bDestroy": true, 
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "lengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
                "order": [], //Initial no order.

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url('highlow/tabledata/')?>"+ file_id,
                    "type": "POST",
                },

                //Set column definition initialisation properties.
                "columnDefs": [
                { 
                    "targets": [0,1,2,3,4,5,6],
                    "orderable": false, //set not orderable
                },
                ],

            });
        }
    }
   
</script>
<script type="text/javascript">
    function updateDataTableSelectAllCtrl(table){
       var $table             = table.table().node();
       var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
       var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
       var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

       if($chkbox_checked.length === 0){
          chkbox_select_all.checked = false;
          if('indeterminate' in chkbox_select_all){
             chkbox_select_all.indeterminate = false;
          }

       } else if ($chkbox_checked.length === $chkbox_all.length){
          chkbox_select_all.checked = true;
          if('indeterminate' in chkbox_select_all){
             chkbox_select_all.indeterminate = false;
          }

       } else {
          chkbox_select_all.checked = true;
          if('indeterminate' in chkbox_select_all){
             chkbox_select_all.indeterminate = true;
          }
       }
    }

    $(document).ready(function (){
        
       var rows_selected = [];
       var table = $('#example').DataTable({
          'columnDefs': [{
             'targets': 0,
             'searchable':false,
             'orderable':false,
             'className': 'dt-body-center',
             'render': function (data, type, full, meta){
                 return '<input type="checkbox">';
             }
          }],
          'order': [1, 'asc'],
          'rowCallback': function(row, data, dataIndex){
             var rowId = data[0];

             if($.inArray(rowId, rows_selected) !== -1){
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).addClass('selected');
             }
          }
       });

       $('#example tbody').on('click', 'input[type="checkbox"]', function(e){
          var $row = $(this).closest('tr');

          var data = table.row($row).data();

          var rowId = data[0];

          var index = $.inArray(rowId, rows_selected);

          if(this.checked && index === -1){
             rows_selected.push(rowId);

          } else if (!this.checked && index !== -1){
             rows_selected.splice(index, 1);
          }

          if(this.checked){
             $row.addClass('selected');
          } else {
             $row.removeClass('selected');
          }

          updateDataTableSelectAllCtrl(table);

          e.stopPropagation();
       });

       $('#example').on('click', 'tbody td, thead th:first-child', function(e){
          $(this).parent().find('input[type="checkbox"]').trigger('click');
       });

       $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
          if(this.checked){
             $('tbody input[type="checkbox"]:not(:checked)', table.table().container()).trigger('click');
          } else {
             $('tbody input[type="checkbox"]:checked', table.table().container()).trigger('click');
          }

          e.stopPropagation();
       });

       table.on('draw', function(){
          updateDataTableSelectAllCtrl(table);
       });

       $('#frm-example').on('submit', function(e){
          var form = this;
        
          $.each(rows_selected, function(index, rowId){
             $(form).append(
                 $('<input>')
                    .attr('type', 'hidden')
                    .attr('name', 'id[]')
                    .val(rowId)
             );
          });
           //$('#example-console-rows').text(rows_selected.join(","));
           
          //$('#example-console').text($(form).serialize());
          //console.log("Form submission", rows_selected);
           var highlowSheetIds =  $(form).serialize().replace(/%5B/g, '[').replace(/%5D/g, ']');
           //alert(dataSheetIds);
           if(rows_selected) {
                   if (confirm("Are you sure!") == true) {
                        $.ajax({
                          url     : "<?php echo base_url(); ?>" + "highlow/deleteHighlowsheet",
                          type    : 'POST',
                          data : highlowSheetIds,
                          success:function(response){
                                var json = $.parseJSON(response);
                                if (json['success'] == 'Ok') {
                                    show_notify('File Deleted Sucessfully.',true);
                                   setInterval('location.reload()', 2000);
                                }
                                if (json['error'] == 'Not') {
                                    show_notify('Error.',false);
                                }
                                return false;
                            }
                        });
                   }
              }
              else {
                  show_notify('Please Select File For Delete.',false);
            }
           
          $('input[name="id\[\]"]', form).remove();
          e.preventDefault();
       });
        
        
    });
    
    function deleteFile(id)
    {
        var id = id;
        //alert(id);
        if (confirm("Are you sure!") == true) {
        $this  = $(this);
        $.ajax({
            url:"<?php echo base_url(); ?>" + "highlow/deleteHighlowsheet",
            type : "POST",
            data : {id:id},
            success:function(response){
                        var json = $.parseJSON(response);
                        if (json['success'] == 'Ok') {
                            show_notify('File Deleted Sucessfully.',true);
                           setInterval('location.reload()', 2000);
                        }
                        if (json['error'] == 'Not') {
                            show_notify('Error.',false);
                        }
                        return false;
                }

            });
        }
    }
</script>
      
