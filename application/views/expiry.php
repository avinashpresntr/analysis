<div class="box box-info">
	<div class="box-header with-border">
	  <h3 class="box-title">Expiry</h3>
	</div>
	<div class="box-body">
    	<form action="<?=base_url()?>entry/saveExpiry" method="post"  >
			<div class="row">
			  <div class="col-sm-4">
				<label for="">Script Name:</label>
                <select name="script_id" id="script_id" class="select2" style="width:100%;">
                    <option value=0>--Select Script--</option>
                    <?php
                    foreach($scripts as $key => $value)
                    {?>
                        <option value="<?=$value['script_id'];?>"><?=$value['script_name'];?></option>
                    <?php
                    }
                    ?>
                </select>
			  </div>
              <div class="col-sm-4">
				<label for="">Expiry Name:</label>
				<input type="text" class="form-control" id="expiry_name" name="expiry_name">
			  </div>
			</div>
            
            <div class="row">
                <div class="col-sm-4">
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
            </div>
            </form>
            <div style="border-bottom: 1px solid #3C8DBC;padding-bottom: 9px;margin: 10px 0 20px 0;"></div>
			 <div class="row">
				<div class="col-md-6">
					<!-- general form elements -->
					 <div class="box box-info box-solid">
						<div class="box-header with-border">
						  <h3 class="box-title"> Expiry </h3>
						  <div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						  </div>
						  <!-- /.box-tools -->
						</div>
						<!-- /.box-header -->
						
						<div class="box-body table-responsive" style="display: block;">
							<table id="example1" class="table table-striped table-bordered" style="text-align: right;">
								<thead>
									<tr>
										<th style="display:none;">Index</th>
										<th align="right" style="text-align: right;">Script Name</th>
                                        <th align="right" style="text-align: right;">Expiry Name</th>
                                        <th align="right" style="text-align: right;">Action</th>
									</tr>
								</thead>
								<tbody id="buy_part">
                                	<?php
									$total_expiry = count($expiry);
									if($total_expiry > 1)
									{
										foreach($expiry as $row)
										{
										?>
										<tr>
											<td nowrap><?php echo $row['script_name'];?></td>
											<td nowrap><?php echo $row['expiry_name'];?></td>
											<td nowrap> <a href="<?=base_url()?>entry/deleteExpiry?id=<?php echo $row['expiry_id'];?>" >Delete</a> </td>
										</tr>  
									<?php }
									}
									?>
                                   
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>	
	
</div>

