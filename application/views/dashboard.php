<!-- iCheck -->
<style>
	//@import url('http://getbootstrap.com/dist/css/bootstrap.css');
	
	.blue-madison{
		color:blue;
		//font-weight: bolder;
	}
	
	.red-rock{
		color:red;
		//font-weight: bolder;
	}
</style>
  <link rel="stylesheet" href="<?= base_url('assets/plugins/iCheck/square/blue.css');?>">
<?php if($this->session->flashdata('success') == true){?>
    <script>
        $(document).ready(function(){
            show_notify("<?=$this->session->flashdata('message')?>",true);
        });
    </script>
<?php }?>
<?php if($this->session->flashdata('upload') == true){?>
    <script>
        $(document).ready(function(){
            show_notify("<?=$this->session->flashdata('message')?>",false);
        });
    </script>
<?php }?>

<?php
    /*$party_details = $this->applib->counts_rows('party_details');
    $model = $this->applib->counts_rows('model');
    $finish = $this->applib->counts_rows('finish');
    $size = $this->applib->counts_rows('size');*/
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashbord
        <!--<a href="party/add" class="btn btn-primary waves-effect waves-light pull-right"><i class="fa fa-plus"></i> Add Party</a>-->
      </h1>
    </section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">Uploded Data File List</h3>
                </div>
                <form action="#" method="get">
					<div class="row">
						<div class="col-sm-1">
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label>Select Fields</label>
								<select name="field" class="form-control">
									<option value='currentdate' <?=($field == "currentdate" ? 'selected' : '');?>>Date</option>
									<option value='buy_first_percentage' <?=($field == "buy_first_percentage" ? 'selected' : '');?>>Buy 1st</option>
									<option value='buy_first_percentage_long' <?=($field == "buy_first_percentage_long" ? 'selected' : '');?>>Buy 1st (Long)</option>
									<option value='buy_first_percentage_short' <?=($field == "buy_first_percentage_short" ? 'selected' : '');?>>Buy 1st (Short)</option>
									<option value='buy_average_percentage' <?=($field == "buy_average_percentage" ? 'selected' : '');?>>Buy Avg</option>
									<option value='buy_average_percentage_long' <?=($field == "buy_average_percentage_long" ? 'selected' : '');?>>Buy Avg (Long)</option>
									<option value='buy_average_percentage_short' <?=($field == "buy_average_percentage_short" ? 'selected' : '');?>>Buy Avg (Short)</option>
									<option value='buy_last_percentage' <?=($field == "buy_last_percentage" ? 'selected' : '');?>>Buy Last</option>
									<option value='buy_last_percentage_long' <?=($field == "buy_last_percentage_long" ? 'selected' : '');?>>Buy Last (Long)</option>
									<option value='buy_last_percentage_short' <?=($field == "buy_last_percentage_short" ? 'selected' : '');?>>Buy Last (Short)</option>
									<option value='short_first_percentage' <?=($field == "short_first_percentage" ? 'selected' : '');?>>Short 1st</option>
									<option value='short_first_percentage_long' <?=($field == "short_first_percentage_long" ? 'selected' : '');?>>Short 1st (Long)</option>
									<option value='short_first_percentage_short' <?=($field == "short_first_percentage_short" ? 'selected' : '');?>>Short 1st (Short)</option>
									<option value='short_average_percentage' <?=($field == "short_average_percentage" ? 'selected' : '');?>>Short Avg</option>
									<option value='short_average_percentage_long' <?=($field == "short_average_percentage_long" ? 'selected' : '');?>>Short Avg (Long)</option>
									<option value='short_average_percentage_short' <?=($field == "short_average_percentage_short" ? 'selected' : '');?>>Short Avg (Short)</option>
									<option value='short_last_percentage' <?=($field == "short_last_percentage" ? 'selected' : '');?>>Short Last</option>
									<option value='short_last_percentage_long' <?=($field == "short_last_percentage_long" ? 'selected' : '');?>>Short Last (Long)</option>
									<option value='short_last_percentage_short' <?=($field == "short_last_percentage_short" ? 'selected' : '');?>>Short Last (Short)</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label>Select Order</label>
								<select name="order" class="form-control">
									<option value='Asc' <?=($order == "Asc" ? 'selected' : '');?>>Ascending</option>
									<option value='Desc' <?=($order == "Desc" ? 'selected' : '');?>>Descending</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label>&nbsp;</label>
								<span class="input-group-btn">
									<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
									</button>
								</span>
							</div>
						</div>
					</div>				
                </form>
                <form id="frm-example" action="" method="POST">
                    <div style="margin: 10px 15px;">
                        <button class="btn btn-primary" type="submit">Delete Selected </button>
                    </div>                                        
                <div class="box-body table-responsive">
                    
                    <table id="example" class="table table-striped table-bordered display select" style="text-align: right;">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="select_all" value="1"></th>
                                <th>#</th>
                                <th>Action</th>                                
                                <th>File Name</th>
                                <th>Date</th>
                                <th class="blue-madison">Buy 1st</th>
                                <th class="blue-madison">Buy Avg</th>
                                <th class="blue-madison">Buy Last</th>
                                <th class="red-rock">Short 1st</th>
                                <th class="red-rock">Short Avg</th>
                                <th class="red-rock">Short Last</th>
                                <th>Highest High</th>
                                <th>Lowest Low</th>
                                <th>Buy Count</th>
                                <th>Short Count</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_script = count($records);
                            foreach($records as $row)
                            {
								//print_r($row);
                            ?>
                            <tr>
                                <td nowrap><?php echo $row->file_id;?></td>
                                <td nowrap><?php echo $total_script;?></td>
                                <td nowrap>
									<!--<a href='<?=base_url();?>average/getAverageMinus/<?=$row->file_id;?>' class="btn btn-primary">Average A.P. Minus</a>
									<a href='<?=base_url();?>average/getAverage/<?=$row->file_id;?>' class="btn btn-primary">All Average</a>-->
									<!-- Get Param 1 = File_id, Param 2 = Exit date filter-->
									<a href='<?=base_url();?>average/getAverageBuy/<?=$row->file_id."/1";?>' class="btn btn-primary">1</a>
									<a href='<?=base_url();?>average/getAverageBuy/<?=$row->file_id."/2";?>' class="btn btn-primary">2</a>
									<a href='<?=base_url();?>average/getAverageShort/<?=$row->file_id."/1";?>' class="btn btn-primary">3</a>
                                </td>
                               
                                <td nowrap><?=$row->file_name; ?>
									<?php
									echo "<br><table style='text-align:right;' border=1>";
									?>
									<thead>
										<tr>
											<th>Statistics Text</th>
											<th>All</th>
											<th>Long</th>
											<th>Short</th>							
										</tr>
									</thead>
									<tbody>
									<?php
									$file_stats=$this->app_model->getFileStatistics($row->file_id);
									$cnt=0;
									foreach($file_stats as $key => $single_statistics) {
										if($cnt <3)
										{
											echo "<tr>";											
											echo "<td>".$single_statistics->statistics_text."</td>";
											if($single_statistics->all_trades<0)
											{
												$color_all = "red";
											}
											else
											{
												$color_all = "blue";
											}
											echo "<td><span style='color:".$color_all."'>".$single_statistics->all_trades."</span></td>";
											if($single_statistics->long_trades<0)
											{
												$color_long = "red";
											}
											else
											{
												$color_long = "blue";
											}
											echo "<td><span style='color:".$color_long."'>".$single_statistics->long_trades."</span></td>";
											if($single_statistics->short_trades<0)
											{
												$color_short = "red";
											}
											else
											{
												$color_short = "blue";
											}
											echo "<td><span style='color:".$color_short."'>".$single_statistics->short_trades."</span></td>";
											echo "</tr>";
										}
										else
										{
											continue;
										}
										/*if($single_statistics->statistics_text=="Total Profit")
										{
											$total_profit = $single_statistics->all_trades;
											$long_profit = $single_statistics->long_trades;
											$short_profit = $single_statistics->short_trades;
											
										}*/
										$cnt++;
									}
									$total_script--;
									echo "<tbody></table>";
									?>
                                </td>
                                <td nowrap><?=$row->currentdate; ?></td>
                                <td nowrap><?php echo "All: >> ".round($row->buy_first_percentage,2)."<br><span class='blue-madison'>Long: >> ".round($row->buy_first_percentage_long,2)."</span><br><span class='red-rock'>Short: >> ".round($row->buy_first_percentage_short,2)."</span>";?></td>
                                <td nowrap><?php echo "All: >> ".round($row->buy_average_percentage,2)."<br><span class='blue-madison'>Long: >> ".round($row->buy_average_percentage_long,2)."</span><br><span class='red-rock'> Short: >> ".round($row->buy_average_percentage_short,2)."</span>";?></td>
                                <td nowrap><?php echo "All: >> ".round($row->buy_last_percentage,2)."<br><span class='blue-madison'>Long: >> ".round($row->buy_last_percentage_long,2)."</span><br><span class='red-rock'> Short: >> ".round($row->buy_last_percentage_short,2)."</span>";?></td>
                                <td nowrap><?php echo "All: >> ".round($row->short_first_percentage,2)."<br><span class='blue-madison'>Long: >> ".round($row->short_first_percentage_long,2)."</span><br><span class='red-rock'> Short: >> ".round($row->short_first_percentage_short,2)."</span>";?></td>
                                <td nowrap><?php echo "All: >> ".round($row->short_average_percentage,2)."<br><span class='blue-madison'>Long: >> ".round($row->short_average_percentage_long,2)."</span><br><span class='red-rock'>Short: >> ".round($row->short_average_percentage_short,2)."</span>";?></td>
                                <td nowrap><?php echo "All: >> ".round($row->short_last_percentage,2)."<br><span class='blue-madison'>Long: >> ".round($row->short_last_percentage_long,2)."</span><br><span class='red-rock'> Short: >> ".round($row->short_last_percentage_short,2)."</span>";?></td>
                                <td nowrap><?=$row->highest_high; ?></td>
                                <td nowrap><?=$row->lowest_low; ?></td>
                                <td nowrap><?php
									$count_buy_array=unserialize($row->count_buy);
									foreach($count_buy_array as $key => $value)
									{
										if($value!=0 && isset($key) && !empty($key))
											echo $key.":>>".$value."<br>";
									}
                                ?></td>
                                <td nowrap><?php
									$count_short_array=unserialize($row->count_short);
									foreach($count_short_array as $key => $value)
									{
										if($value!=0)
											echo $key.":>>".$value."<br>";
									}?></td>
                                <!--<td><button type="button" onclick="fileId(<?=$row->file_id;?>)" class="btn btn-primary">Average</button></td>-->
                                <td> 
                                    <a href="javascript:void(0)" onclick="deleteFile(<?=$row->file_id;?>)" class="btn btn-primary"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                    
                </form>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- iCheck -->
<script src="<?=base_url('assets/plugins/iCheck/icheck.min.js');?>"></script>

<script>
    function fileId(id){
        var id = id
        //alert(id);
        $.ajax({
            type: "post",
            url: "<?php echo base_url('average/getAverage');?>",
            data: {id:id},
            cache: false,
            async: false,
            success: function(result){
                console.log(result); 
            }
        });

    }
</script>
<script type="text/javascript">
    function updateDataTableSelectAllCtrl(table){
        var $table             = table.table().node();
        var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
        var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
        var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

        if($chkbox_checked.length === 0){
            chkbox_select_all.checked = false;
            if('indeterminate' in chkbox_select_all){
                chkbox_select_all.indeterminate = false;
            }
        } else if ($chkbox_checked.length === $chkbox_all.length){
            chkbox_select_all.checked = true;
            if('indeterminate' in chkbox_select_all){
                chkbox_select_all.indeterminate = false;
            }
        } else {
            chkbox_select_all.checked = true;
            if('indeterminate' in chkbox_select_all){
                chkbox_select_all.indeterminate = true;
            }
        }
    }

    $(document).ready(function (){
            var rows_selected = [];
            var table = $('#example').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'searchable':false,
                    'orderable':false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox">';
                    }
                },                
                {
                    targets: [0,1,2,4,5,6,7,8,9,12,13,14],
                    'orderable':false
                }],
               // 'order': [3, 'desc'],
                'pageLength': 500,
                'lengthMenu': [ 500, 1000, 2500, 5000, 10000 ],
                'rowCallback': function(row, data, dataIndex){
                var rowId = data[0];

                    if($.inArray(rowId, rows_selected) !== -1){
                        $(row).find('input[type="checkbox"]').prop('checked', true);
                        $(row).addClass('selected');
                    }
                }
            });

            $('#example tbody').on('click', 'input[type="checkbox"]', function(e){
                var $row = $(this).closest('tr');
                var data = table.row($row).data();
                var rowId = data[0];
                var index = $.inArray(rowId, rows_selected);
                
                if(this.checked && index === -1){
                    rows_selected.push(rowId);
                } else if (!this.checked && index !== -1){
                    rows_selected.splice(index, 1);
                }

                if(this.checked){
                    $row.addClass('selected');
                } else {
                    $row.removeClass('selected');
                }

                updateDataTableSelectAllCtrl(table);
                e.stopPropagation();
            });

            $('#example').on('click', 'tbody td, thead th:first-child', function(e){
                $(this).parent().find('input[type="checkbox"]').trigger('click');
            });

            $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
                if(this.checked){
                    $('tbody input[type="checkbox"]:not(:checked)', table.table().container()).trigger('click');
                } else {
                    $('tbody input[type="checkbox"]:checked',   table.table().container()).trigger('click');
                }
                e.stopPropagation();
            });

            table.on('draw', function(){
                updateDataTableSelectAllCtrl(table);
            });

            $('#frm-example').on('submit', function(e){
                var form = this;

                $.each(rows_selected, function(index, rowId){
                    $(form).append(
                    $('<input>')
                    .attr('type', 'hidden')
                    .attr('name', 'id[]')
                    .val(rowId)
                    );
                });
                //$('#example-console-rows').text(rows_selected.join(","));

                //$('#example-console').text($(form).serialize());
                //console.log("Form submission", rows_selected);
                var dataSheetIds =  $(form).serialize().replace(/%5B/g, '[').replace(/%5D/g, ']');
                //alert(dataSheetIds);
                if(rows_selected) {
                    if (confirm("Are you sure!") == true) {
                        $.ajax({
                            url : "<?php echo base_url(); ?>" + "data/deleteDatasheet",
                            type : 'POST',
                            data : dataSheetIds,
                            success:function(response){
                                var json = $.parseJSON(response);
                                if (json['success'] == 'Ok') {
                                    show_notify('File Deleted Sucessfully.',true);
                                    setInterval('location.reload()', 2000);
                                }
                                if (json['error'] == 'Not') {
                                    show_notify('Error.',false);
                                }
                                return false;
                            }

                        });
                    }
                }
                else {
                    show_notify('Please ---------Select File For Delete.',false);
                }

                $('input[name="id\[\]"]', form).remove();
                e.preventDefault();
            });


    });

    function deleteFile(id){
        var id = id;
        //alert(id);
        if (confirm("Are you sure!") == true) {
            $this  = $(this);
            $.ajax({
                url:"<?php echo base_url(); ?>" + "data/deleteDatasheet",
                type : "POST",
                data : {id:id},
                success:function(response){
                    var json = $.parseJSON(response);
                    if (json['success'] == 'Ok') {
                        show_notify('File Deleted Sucessfully.',true);
                        setInterval('location.reload()', 2000);
                    }
                    if (json['error'] == 'Not') {
                        show_notify('Error.',false);
                    }
                    return false;
                }
            });
        }
    }
</script>
