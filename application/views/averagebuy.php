<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url('assets/plugins/datepicker/datepicker3.css');?>">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Average
        <a href="<?=base_url();?>" class="btn btn-primary waves-effect waves-light pull-right"><i class="fa fa-dashboard"></i>&nbsp; Go To Dashboard</a>
      </h1>
    </section>	
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                 <div class="box box-info box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title"> Buy (<?php echo $filename->file_name; ?>)</h3>
                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                      <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive" style="display: block;">
                        <table id="example" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="display:none;">Index</th>
                                    <th>Date</th>
                                    <th>Price</th>
                                    <th>Entry</th>
									<th>Avg</th>
                                    <th>Count</th>
                                    <th>Info</th>
                                    <!--<th>Average</th>-->
                                </tr>
                            </thead>
                            <tbody>
								<?php 
								$abs_neg_counts=0;
								$do_pice_array = array();
								$cnt = 0;
								
								/*echo "<pre>";
								print_r($buy);
								echo "</pre>";*/
							
								foreach($buy as $key => $single_file) { 									
									$exit_or =0;
									$datecount=0;
									
									$curdate = date("Y-m-d", strtotime($buy[$key]->do_date));
									//$keyNextbykey=$key;
									$pluskey=1;
									$nextdatewithminus=$key+1;
									while(isset($buy[$nextdatewithminus]->dt_absolute_performance) && $buy[$nextdatewithminus]->dt_absolute_performance>0)
									{
										//$nextminusdate = $buy[$nextdatewithminus]->do_date;
										//exit;
										$pluskey++;
										$nextdatewithminus++;
									}
									
									$nextdate= '';
									if(isset($buy[$nextdatewithminus]->do_date) && !empty($buy[$nextdatewithminus]->do_date)){
										$nextdate= date("Y-m-d", strtotime($buy[$nextdatewithminus]->do_date));
									}
									if($single_file->dt_absolute_performance<0)
									{
										$do_pice_array[$abs_neg_counts]=$single_file->do_price;
										$abs_neg_counts++;
									}
									$average_check = 0;
									if(!empty($do_pice_array)){
										$average_check = array_sum($do_pice_array) / count($do_pice_array);
									}
									while($curdate==$nextdate) {
										//if($buy[$key+$datecount]->dt_absolute_performance>0)
											//continue;
										$nextdate=date("Y-m-d", strtotime($buy[$key+$datecount]->do_date));
										$datecount++;
									} 
									$exit_date = "Not exited";
									if($nextdate=="1970-01-01")
									{
										//$date = DateTime::createFromFormat("Y-m-d", $currentdate);
										$pieces = explode("-", $curdate);
										//$year = $date->format("Y");
										$nextdate = $pieces[0] . '-12-31';
										//print_r($pieces);
										//$nextdate = $pieces[0];
									}
									$query=$this->appmodel->getHighLowDataByDate($curdate,$nextdate,$single_file->file_id);
									foreach($query as $key1 => $data) {									
										if($data->hl_high>$average_check)
										{
											$exit_date = $data->hl_date;
											$exit_or =true;
											$abs_neg_counts=0;
											$count_perfact = 0;
											if(!empty($do_pice_array)){
												$count_perfact = count($do_pice_array);
											}
											unset($do_pice_array);
											break;
										}
									}
									
									if($single_file->dt_absolute_performance>0)
										continue;
									
									?>
									<tr>
										<td style="display:none;"><?=$cnt;?></td>
										<td nowrap><?=date("Y-m-d", strtotime($buy[$key]->do_date));?></td>
										<!--<td><?=$single_file->do_price."::::".$key."+".$pluskey."::".$nextdatewithminus;?></td>-->
										<td><?=$single_file->do_price;?></td>
										<td><?=$single_file->dt_absolute_performance;?></td>
										<td><?=round($average_check,2);?></td>
										<!--<td><?=count($do_pice_array);?>-->										
										<td><?php
											if($exit_or)
												echo $count_perfact;
											else
												echo count($do_pice_array);
										?>									
										</td>
										<!--<td><?=$query[0]->hl_high;?></td>										-->
										<td>
											Exit : <?=$exit_date;?>
											<?php 
											if($datefilter==1){ ?>
											<table border=1>
												<tr>
													<td>Date</td>
													<td>High</td>
													<td>Low</td>
												</tr>
												<?php
												foreach($query as $key1 => $data) {									
													echo "<tr>";
													echo "<td nowrap>".$data->hl_date."</td>";
													echo "<td>".$data->hl_high."</td>";
													echo "<td>".$data->hl_low."</td>";
													echo "</tr>";	
												}
												?>
											</table>
											<?php } ?>
										</td>
									</tr>                               
                                <?php
								$cnt++;
							}?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.Short start -->
            <div class="col-md-6">
                <div class="box box-info box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title"> Short (<?php echo $filename->file_name; ?>)</h3>
                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                      <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive" style="display: block;">
                        <table id="example1" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="display:none;">Index</th>
                                    <th>Date</th>
                                    <th>Price</th>
                                    <th>Entry</th>
									<th>Avg</th>
                                    <th>Count</th>
                                    <th>Info</th>
                                    <!--<th>Average</th>-->
                                </tr>
                            </thead>
                            <tbody>
								<?php 
								$abs_neg_counts=0;
								$do_pice_array = array();
								$cnt = 0;
								
								/*echo "<pre>";
								print_r($short);
								echo "</pre>";*/
							
								foreach($short as $key => $single_file) { 									
									$exit_or =0;
									$datecount=0;
									
									$curdate = date("Y-m-d", strtotime($short[$key]->do_date));
									//$keyNextbykey=$key;
									$pluskey=1;
									$nextdatewithminus=$key+1;
									while(isset($short[$nextdatewithminus]->dt_absolute_performance) && $short[$nextdatewithminus]->dt_absolute_performance>-0.00000001)
									{
										//$nextminusdate = $short[$nextdatewithminus]->do_date;
										//exit;
										$pluskey++;
										$nextdatewithminus++;
									}
									
									$nextdate= '';
									if(isset($short[$nextdatewithminus]->do_date) && !empty($short[$nextdatewithminus]->do_date)){
										$nextdate= date("Y-m-d", strtotime($short[$nextdatewithminus]->do_date));
									}
									if($single_file->dt_absolute_performance<0)
									{
										$do_pice_array[$abs_neg_counts]=$single_file->do_price;
										$abs_neg_counts++;
									}
									$average_check = 0;
									if(!empty($do_pice_array)){
										$average_check = array_sum($do_pice_array) / count($do_pice_array);
									}
									while($curdate==$nextdate) {
										//if($short[$key+$datecount]->dt_absolute_performance>0)
											//continue;
										$nextdate=date("Y-m-d", strtotime($short[$key+$datecount]->do_date));
										$datecount++;
									} 
									$exit_date = "Not exited";
									if($nextdate=="1970-01-01")
									{
										//$date = DateTime::createFromFormat("Y-m-d", $currentdate);
										$pieces = explode("-", $curdate);
										//$year = $date->format("Y");
										$nextdate = $pieces[0] . '-12-31';
										//print_r($pieces);
										//$nextdate = $pieces[0];
									}
									$query=$this->appmodel->getHighLowDataByDate($curdate,$nextdate,$single_file->file_id);
									foreach($query as $key1 => $data) {									
										if($data->hl_low<$average_check)
										{
											$exit_date = $data->hl_date;
											$exit_or =true;
											$abs_neg_counts=0;
											$count_perfact2 = count($do_pice_array);
											unset($do_pice_array);
											break;
										}
									}
									
									if($single_file->dt_absolute_performance>=0)
										continue;
									
									
									
									//echo $nextdate;
									?>
									<tr>
										<td style="display:none;"><?=$cnt;?></td>
										<td nowrap><?=date("Y-m-d", strtotime($short[$key]->do_date));?></td>
										<!--<td><?=$single_file->do_price."::::".$key."+".$pluskey."::".$nextdatewithminus;?></td>-->
										<td><?=$single_file->do_price;?></td>
										<td><?=$single_file->dt_absolute_performance;?></td>
										<td><?=round($average_check,2);?></td>
										<!--<td><?=count($do_pice_array);?>-->
										<td><?php
											if($exit_or)
												echo $count_perfact2;
											else
												echo count($do_pice_array);
										?>
										<?php //print_r($do_pice_array);?>
										</td>
										<!--<td><?=$query[0]->hl_high;?></td>										-->
										<td>
											Exit : <?=$exit_date;?>
											<?php 
											if($datefilter==1){ ?>
											<table border=1>
												<tr>
													<td>Date</td>
													<td>High</td>
													<td>Low</td>
												</tr>
												<?php
												foreach($query as $key1 => $data) {									
													echo "<tr>";
													echo "<td nowrap>".$data->hl_date."</td>";
													echo "<td>".$data->hl_high."</td>";
													echo "<td>".$data->hl_low."</td>";
													echo "</tr>";	
												}
												?>
											</table>
											<?php } ?>
										</td>
									</tr>                               
                                <?php
								$cnt++;
							}?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.short End -->
            <!-- /.col -->
            <!-- /.short start -->
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- bootstrap datepicker -->
<script src="<?=base_url('assets/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
<script>
    $(document).ready(function(){
		$("#example").dataTable({"paging": false, "order": [[ 0, "asc" ]],"aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [ 1,2,3,4], }
       ]});
	   
        var datatable = $("#example").dataTable();
        
        
        $('#example tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                datatable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        
        $("#example1").dataTable({"paging": false, "order": [[ 0, "asc" ]],"aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [ 1,2,3,4], }
       ]});
	   
        var datatable = $("#example1").dataTable();
        
        
        $('#example1 tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                datatable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
    });
    
</script>
<script type="text/javascript">
$(document).ready(function() {
	
    $("#average").click(function(event) {
        event.preventDefault();
        var fromDate = $("input#fromDate").val();
        var toDate = $("input#toDate").val();
        
        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "average/getAverage",
           
            data: {fromDate: fromDate, toDate: toDate},
            success: function() {
            
            }
        });
    });
});
</script>
