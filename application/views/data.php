<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data
        <!--<a href="party/add" class="btn btn-primary waves-effect waves-light pull-right"><i class="fa fa-plus"></i> Add Party</a>-->
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
        <!-- form start -->
            <form role="form" action="" method="post" enctype="multipart/form-data" id="frm_insert">
            <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Select CSV File For Import</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <input type="file" id="exampleInputFile" name="file">
                            <p class="help-block"><?php ?></p>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <!-- Loading (remove the following to stop the loading)-->
                    <div class="overlay" id="overlay" style="">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>
                    <!-- end loading -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" name="import">Import</button>
                    </div>
                </div>
                <!-- /.box -->
            </form>
        </div>
        
        <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-body">
                    <p>
                        <b>File Name Samples.</b><br />
                        <small>
                            ITEMNAME-ddmmyyyy-hhmm-data.csv<br />
                            ITEMNAME-ddmmyyyy-1d-data.csv<br />
                            ITEMNAME-ddmmyyyy-1w-data.csv<br />
                            ITEMNAME-ddmmyyyy-1m-data.csv<br />
                            ITEMNAME-ddmmyyyy-1y-data.csv<br />
                        </small>
                    </p>
                </div>
              </div>
              <!-- /.box -->
          </div>
      </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-body">
                        <label for="form-address">Select File</label>          
                        <div class="input-group input-group-sm">
                            <select name="file_id"  id="file_id" class="select2" style="width: 100%;">
                                <option value="" selected="selected" >- Select file -</option>
                                <?php foreach($filelist as $row): ?>
                                <option value="<?php echo $row->file_id; ?>"><?php echo $row->file_name; ?></option>
                                <?php endforeach; ?> 
                            </select>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-info btn-flat" onclick="getdatatable()">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
        </div>      
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    <h3 class="box-title">Table</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table id="example" class="table table-striped table-bordered example">
                            <thead>
                                <tr>
                                    <th>Oarder Date</th>
                                    <th>Buy/Sell</th>
                                    <th>Price</th>
                                    <th>Absolute Performance</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Select2 -->
<script>
    $('#overlay').hide();
    //$(".select2").select2();
    var datatable = $("#example").dataTable();
    var datatable1 = $("#example1").dataTable();
    $(document).ready(function(){
        $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            datatable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });


        $("#frm_insert").submit(function(event){
        event.preventDefault();
        var postData = new FormData(this);
        $.ajax({
        url:'<?php echo base_url();?>data/import',
        type:'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: postData,
        beforeSend: function() { $('#overlay').show(); },
        complete: function() { $('#overlay').hide(); },
        success:function(response){
                var json = $.parseJSON(response);
                if (json['success'] == 'Success') {
                    show_notify('File Imported Sucessfully.',true);
                    setInterval('location.reload()', 1500);
                }
                if (json['error'] == 'Exist') {
                    show_notify('File Already Exist.',false);
                }
                if (json['error'] == 'Notupload') {
                    show_notify('File does not upload.',false);
                }
                if (json['error'] == 'Invalid_err') {
                    var msg = json['msg'];
                    show_notify(msg,false);
                }
                if (json['error'] == 'Invalid') {
                    show_notify("Invalid file",false);
                }
                if (json['error'] == 'Select') {
                    show_notify("Select file",false);
                }
                if (json['error'] == 'Only') {
                    show_notify('Please upload only data sheet.',false);
                }
                return false;
        }
    });
    });

    });
    function getdatatable()
    {
    var file_id = $('#file_id').val();
    //alert(file_id);
    if(file_id != ''){
    var table = $('.example').DataTable({ 
        "bDestroy": true, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "lengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('data/tabledata/')?>"+ file_id,
            "type": "POST",
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [0,1,2,3],
            "orderable": false, //set not orderable
        },
        ],

    });
    }
    }
</script>
