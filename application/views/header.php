<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Analysis | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/dist/css/font-awesome.min.css');?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url('assets/dist/css/ionicons.min.css');?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css');?>">
  
  <!----------------Notify---------------->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/notify/jquery.growl.css');?>">
  <!----------------Notify---------------->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url();?>assets/plugins/select2/select2.css">
    <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?= base_url();?>assets/plugins/datepicker/datepicker3.css">
  <!-- jQuery 2.2.3 -->
  <script src="<?= base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap.css'); ?>"  type="text/css" />
    
  <script src="<?=base_url('assets/plugins/datatables/jquery.dataTables.min.js');?>"></script>
  <script src="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>

  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/dist/css/AdminLTE.min.css');?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url('assets/dist/css/skins/_all-skins.min.css');?>">
  <link href="<?= base_url('assets/dist/css/custom.css');?>" rel="stylesheet" type="text/css" />  
  
    
</head>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
  <header class="main-header">
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
		<!-- Logo -->
    <a href="<?= base_url()?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Analysis</b></span>
    </a>
      <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
		  <ul class="nav navbar-nav">
			<li class=""></li>
			<li>
			  <a href="<?=base_url();?>">
				<i class="fa fa-dashboard"></i> <span> &nbsp; Dashboard</span>
			  </a>
			</li>
			<li>
			  <a href="<?php echo base_url();?>data">
				<i class="fa fa-list"></i> <span> &nbsp; Data</span>
			  </a>
			</li>
			<li>
			  <a href="<?php echo base_url();?>highlow">
				<i class="fa fa-file"></i> <span> &nbsp; High Low</span>
			  </a>
			</li>
			<li class="treeview">
			  <a href="<?php echo base_url();?>average/manualEntry">
				<i class="fa fa-user"></i> <span> Manual Entry</span>
			  </a>
			</li>
            <li class="treeview">
			  <a href="<?php echo base_url();?>average/scripts">
				<i class="fa fa-user"></i> <span> Scripts</span>
			  </a>
			</li>
            <li class="treeview">
			  <a href="<?php echo base_url();?>average/expiry">
				<i class="fa fa-user"></i> <span> Expiry</span>
			  </a>
			</li>
		  </ul>
		</div>
		<!-- /.navbar-collapse -->
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/dist/img/avatar5.png');?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?=isset($name)?ucwords($name):'Admin';?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url('assets/dist/img/avatar5.png');?>" class="img-circle" alt="User Image">
                <p>
                    <?=isset($name)?ucwords($name):'Admin';?>
                    <br/>
                    <?=isset($email)?$email:'';?>
                </p>
                
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?= base_url()?>auth/profile" class="btn btn-default btn-flat">Change Password</a>
                </div>
                <div class="pull-right">
                  <a href="<?= base_url()?>auth/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
