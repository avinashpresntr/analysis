<?php
if(isset($_GET['id']) && $_GET['id']!='') {
	if($this->session->flashdata('success') == true){?>
<script>
	$(document).ready(function(){
		show_notify("<?=$this->session->flashdata('message')?>",true);
	});
</script>
<?php }
}
?>
<style>
	@import url('http://getbootstrap.com/dist/css/bootstrap.css');

	.blue-madison{
		color:blue;
		//font-weight: bolder;
	}

	.red-rock{
		color:red;
		//font-weight: bolder;
	}
</style>
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Entry</h3>
	</div>
	<div class="box-body">
		<!-- <div class="row"> -->
		<form action="<?=base_url()?>entry/save" id="entry-form" method="post" data-parsley-validate novalidate class="form-horizontal">
			<table class="table table-borderless bg-info" id="tbl_form">
				<tr class="">
					<td class="col-md-2"><label for="">Date:</label></td>
					<td class="col-md-2"><label for="">Script Name:</label></td>
					<td class="col-md-2"><label for="">Expiry</label></td>
					<td class="col-md-2"><label for="">% Brok</label></td>
					<td class="col-md-2"><label class="" for="">Per Unit Brok</label>	</td>
				</tr>
				<tr>
					<td class="col-md-2">
						<input type="text" class="" id="datepicker" name="datepicker" value="<?php echo date('d/m/Y'); ?>">
						<input type="hidden"  id="entry_id" name="entry_id" value="<?php if(isset($_GET['id']) && $_GET['id']!='') {echo $_GET['id'];} ?>">
						<input type="hidden"  id="selected_script_id" name="selected_script_id" value="<?php if(isset($_GET['script_id']) && $_GET['script_id']!='') {echo $_GET['script_id'];} ?>">
					</td>
					<td class="col-md-2">
						<select class="" style="width:100%;" name="account_name" id="account_name" required>
						</select>
					</td>
					<td class="col-md-2">
						<select class="" style="width:100%;" name="expiry_id[]" id="expiry_id">
							<option value=0>--Select Expiry--</option>
						</select>
					</td>
					<td class="col-md-2">
						<input type="text" class="" id="brokerage_percentage" name="brokerage_percentage" value="0">
					</td>
					<td class="col-md-2">
						<input type="text" class="" id="brokerage_fix" name="brokerage_fix" value="0">	
					</td>
				</tr>
				<tr>
					<td class="col-md-2"><label for="">Buy OR Short</label></td>
					<td class="col-md-2"><label for="">Price</label></td>
					<td class="col-md-2"><label for="">Quantity</label></td>
					<td class="col-md-2"><label for="">&nbsp;</label></td>
					<td class="col-md-2"><label for="">&nbsp;</label></td>
				</tr>
				<tr>
					<td class="col-md-2">
						<select class="" style="width:200px;" name="buy_sale" id="buy_sale">
							<option value=0>Buy</option>
							<option value=1>Short</option>
						</select>
					</td>
					<td class="col-md-2">
						<input type="number" class="" name="price" >
					</td>
					<td class="col-md-2">
						<input type="number" class="" name="qty">
					</td>
					<td class="col-md-2"></td>
					<td class="col-md-2"><button type="submit" id="btn_submit" class="btn btn-primary btn-xs form-control">Submit</button></td>
				</tr>

			</table>
			<!--<div class="row">
<div class="col-sm-3">
<div class="input-group">
<label for="">Date:</label>
<input type="text" class="form-control" id="datepicker" name="datepicker" value="<?php echo date('d/m/Y'); ?>">
<input type="hidden" class="form-control" id="entry_id" name="entry_id" value="<?php if(isset($_GET['id']) && $_GET['id']!='') {echo $_GET['id'];} ?>">
<input type="hidden" class="form-control" id="selected_script_id" name="selected_script_id" value="<?php if(isset($_GET['script_id']) && $_GET['script_id']!='') {echo $_GET['script_id'];} ?>">
</div>
</div>
<div class="col-sm-3">
<label for="">Script Name:</label>
<select class="select2" style="width:100%;" name="account_name" id="account_name">
</select>
</div>
<div class="col-sm-3">
<label for="">Expiry</label>
<select class="" style="width:100%;" name="expiry_id[]" id="expiry_id">
<option value=0>--Select Expiry--</option>
</select>
</div>
<div class="col-sm-1">
<label for="">% Brok</label>
<input type="text" class="form-control" id="brokerage_percentage" name="brokerage_percentage" value="0">
</div>
<div class="col-sm-2">
<label class="col-sm-12" for="">Per Unit Brok</label>	
<div class="col-sm-6">
<input type="text" class="form-control" id="brokerage_fix" name="brokerage_fix" value="0">	
</div>

</div>
</div>
<div class="row">
<div class="col-sm-3">
<label for="">Buy OR Short</label>
<br>
<select class="select2" style="width:200px;" name="buy_sale" id="buy_sale">
<option value=0>Buy</option>
<option value=1>Short</option>
</select>
</div>
<div class="col-sm-1">
<label for="">Price</label>
<input type="number" class="form-control" name="price" >
</div>
<div class="col-sm-1">
<label for="">Quantity</label>
<input type="number" class="form-control" name="qty">
</div>
<div class="col-sm-4">
<label for="">&nbsp;</label>
</div>
<div class="col-sm-3">
<label for="">&nbsp;</label>
<button type="submit" class="btn btn-primary btn-xs form-control">Submit</button>
</div>
</div> 
</div>-->
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4"></div>
				<div class="col-sm-4"><!-- <button type="submit" class="btn btn-primary">Submit</button> --></div>
				<input type="hidden" name="client_id" value="1">
				<!-- /.row -->
			</div>
		</form>
		<div style="border-bottom: 1px solid #3C8DBC;padding-bottom: 9px;margin: 10px 0 20px 0;"></div>
		<div class="row" >
			<div class="col-md-3">
				<div class="input-group">
					<label for="">Script Name:</label>
					<select name="script_name" id="script_name" class="select2" style="width:100%;">
						<option value=''>--Select Script--</option>
						<?php
						foreach($scripts as $key => $value)
						{?>
						<option value="<?=$value['script_id'];?>"><?=$value['script_name'];?></option>
						<?php
						}
						?>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="input-group">
					<label for="">Expiry:</label>
					<!--<select style="width:100%;" name="expiry2_id[]" id="expiry2_id" onchange="script_details(this.value)" multiple="multiple">-->
					<select class="form-control" style="width:100%;" name="expiry2_id[]" id="expiry2_id" multiple="multiple">
						<option value="">All</option>
					</select>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="input-group">
					<label for="">Date From:</label>
					<input type="text" class="form-control" id="dateFrom" class="datepicker" name="dateFrom" value="">
				</div>
			</div>
			<div class="col-sm-2">
				<div class="input-group">
					<label for="">Date To:</label>
					<input type="text" class="form-control" id="dateTo" class="datepicker" name="dateTo" value="">
				</div>
			</div>
			<div class="col-sm-2">
				<div class="input-group">
					<label for="">&nbsp;</label>
				</div>
				<button class="btn btn-primary btn-xs form-control" id="btn_go">Go</button>	
			</div>
		</div>
		<div style="border-bottom: 1px solid #3C8DBC;padding-bottom: 9px;margin: 10px 0 20px 0;"></div>
		<div class="row">
			<div class="col-md-6">
				<!-- general form elements -->
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<h3 class="box-title"> Buy </h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					<?php
					// print_r($buysell);
					?>
					<div class="box-body table-responsive" style="display: block;">
						<table id="example12" class="table table-striped table-bordered" style="text-align: right;">
							<thead>
								<tr>
									<th style="display:none;">Index</th>
									<th align="right" style="text-align: right;">Action</th>
									<th align="right" style="text-align: right;">Sr. No</th>
									<th align="right" style="text-align: right;">Date</th>
									<th align="right" style="text-align: right;">Price</th>
									<th align="right" style="text-align: right;">Pending Qty</th>
									<th align="right" style="text-align: right;">Qty</th>
									<th align="right" style="text-align: right;">Total</th>
									<th align="right" style="text-align: right;">Average</th>
								</tr>
							</thead>
							<tbody id="buy_part">
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
			<!-- /.Short start -->
			<div class="col-md-6">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<h3 class="box-title"> Short </h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive" style="display: block;">
						<table id="example12" class="table table-striped table-bordered" style="text-align: right;">
							<thead>
								<tr>
									<th style="display:none;">Index</th>
									<th align="right" style="text-align: right;">Action</th>
									<th align="right" style="text-align: right;">Sr. No</th>
									<th align="right" style="text-align: right;">Date</th>
									<th align="right" style="text-align: right;">Price</th>
									<th align="right" style="text-align: right;">Pending Qty</th>
									<th align="right" style="text-align: right;">Qty</th>
									<th align="right" style="text-align: right;">Total</th>
									<th align="right" style="text-align: right;">Average</th>
								</tr>
							</thead>
							<tbody id="sell_part">
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<button type="button" class="btn btn-block btn-primary disabled" id="save_equal_qty">Save</button>
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
		<div style="border-bottom: 1px solid #3C8DBC;padding-bottom: 9px;margin: 10px 0 20px 0;"></div>
		<div class="col-md-6 ">
			<div id="joomkhas"></div>
			<div class="col-md-6" id="buy_joomkhu"></div>
			<div class="col-md-6" id="sell_joomkhu"></div>
		</div>
	</div>
</div>
<script>
	var table;
	$(document).ready(function(){

		account_name_load();
		if($('#entry_id').val()!='')
		{
			setTimeout(function(){
				fillEntryById($('#entry_id').val());
				setTimeout(function(){
					$('#script_name').val($('#account_name').val());
					//$('#script_name').select2().select2('val',$('#account_name').val());
					$( "#script_name" ).trigger( "change" );
					$('#expiry2_id').val($('#expiry_id').val());
					//$('#expiry2_id').select2().select2('val',$('#expiry_id').val());
					$( "#btn_go" ).trigger( "click" );	
				}, 1000);
			}, 1000);

			//fillEntryById(entry_id);
		}
		if($('#selected_script_id').val()!='')
		{
			$('#script_name').val($('#selected_script_id').val());
			//$("#script_name").select2('val',$('#selected_script_id').val()).change();
			$( "#buy_part").html("");
			$( "#sell_part").html("");

			$("#save_equal_qty").addClass('disabled');
			$(".btn_save").addClass('disabled');

			var script_id = $('#script_name').val();
			var dateFrom = $('#dateFrom').val();
			var dateTo = $('#dateTo').val();

			$.ajax({
				url: '<?=base_url('average/ajaxManualEntry');?>',
				data: { script_id: script_id, dateFrom :dateFrom, dateTo :dateTo },
				method: "POST",
				success: function(result){
					//fill_entry_joomkhas(result);
					fillExpiry(result);
				}
			});
			$( "#btn_go" ).trigger( "click" );	
		}
		$(document).on('click', '.btn_save', function(event) {
			if(!$(this).hasClass('disabled'))
				$('#save_equal_qty').click();
		});

		$('#save_equal_qty').click(function() {
			var buy = new Array();
			var sell = new Array();
			var buy_count = 0;
			var sell_count = 0;
			var script_id = 1;
			//var expiry_id = 1;
			var expiry_id = $('select#expiry2_id').val();
			var dateFrom = $('#dateFrom').val();
			var dateTo = $('#dateTo').val();
			$('.sum_buy').each(function(){
				//buy[buy_count] = $(this).attr('entry_id');
				if($(this).val() != "" && $(this).val()!=0)
				{
					//buy[$(this).attr('entry_id')] = $(this).val();
					buy.push({
						entry_id:   $(this).attr('entry_id'),
						qty: $(this).val(),
						script_id: $(this).attr('script_id'),
						expiry_id: $(this).attr('expiry_id'),
						price: $(this).attr('price'),
						pick_date: $(this).attr('pick_date'),
						brok_per: $(this).attr('brok_per'),
						brok_fix: $(this).attr('brok_fix')
					});
				}
				script_id = $(this).attr('script_id');
				expiry_id = $(this).attr('expiry_id');
				//console.log(script_id);				
			});

			$('.sum_sell').each(function(){

				if($(this).val() != "" && $(this).val()!=0)
				{
					sell.push({
						entry_id:   $(this).attr('entry_id'),
						qty: $(this).val(),
						script_id: $(this).attr('script_id'),
						expiry_id: $(this).attr('expiry_id'),
						price: $(this).attr('price'),
						pick_date: $(this).attr('pick_date'),
						brok_per: $(this).attr('brok_per'),
						brok_fix: $(this).attr('brok_fix')
					});
				}				
			});

			$.ajax({
				url: '<?=base_url('average/ajaxUpdateQtyEntry');?>',
				data: { buy_array: buy, sell_array: sell, script_id :  script_id , expiry_id : expiry_id, dateFrom :dateFrom, dateTo :dateTo },
				method: "POST",
				success: function(result){
					fill_entry_joomkhas(result);
					fillExpiry(result);
				}
			});
		});

		$('body').on('change','.sum_buy',function(){
			console.log('sum_buy');
			var pending_qt = parseInt($(this).attr('pending_qty'));
			var box_qty = parseInt($(this).val());
			if(box_qty > pending_qt)
			{
				alert('Please set lower value then pending.');
				return false;
			}
			var totalPointsBuy = 0;
			$('.sum_buy').each(function(){
				totalPointsBuy += parseInt($(this).val()); //<==== a catch  in here !! read below
			});
			//console.log(totalPoints);
			$("#buy_total_qty").html(totalPointsBuy);

			var sum_buy = parseInt($("#buy_total_qty").html());
			var sum_sell = parseInt($("#sell_total_qty").html());
			console.log(sum_buy+"---:---- in buy<<--:-- >>"+sum_sell);
			if(sum_buy == sum_sell && sum_buy!=0 && sum_buy!=0){
				$("#save_equal_qty").removeClass('disabled');
				$(".btn_save").removeClass('disabled');
			}else{
				$("#save_equal_qty").addClass('disabled');
				$(".btn_save").addClass('disabled');
			}

		});

		$('body').on('change','.sum_sell',function(){
			console.log('sum_sell');
			var pending_qt = parseInt($(this).attr('pending_qty'));
			var box_qty = parseInt($(this).val());
			if(box_qty > pending_qt)
			{
				alert('Please set lower value then pending.');
				return false;
			}
			var totalPointsSell = 0;
			$('.sum_sell').each(function(){
				totalPointsSell += parseInt($(this).val()); //<==== a catch  in here !! read below	
			});

			console.log("In sale total point sale"+totalPointsSell);
			$("#sell_total_qty").html(totalPointsSell);

			var sum_buy = parseInt($("#buy_total_qty").html());
			var sum_sell = parseInt($("#sell_total_qty").html());
			console.log(sum_buy+"---:---- in sell<<--:-- >>"+sum_sell);
			if(sum_buy == sum_sell && sum_buy!=0 && sum_buy!=0){
				$("#save_equal_qty").removeClass('disabled');
				$(".btn_save").removeClass('disabled');
			}else{
				$("#save_equal_qty").addClass('disabled');
				$(".btn_save").addClass('disabled');
			}
		});

		$('body').on('click','.del_entry',function(){
			var r = confirm("Are you sure you want to delete this Entry?");
			if (r == true) {
				//txt = "You pressed OK!";
				var entry_id = parseInt($(this).attr('entry_id'));
				var script_id=$('#script_name').val();
				var expiry_id=$('#expiry2_id').val();
				$.ajax({
					url: '<?=base_url('average/ajaxDeleteOnlyEntry');?>',
					data: { entry_id: entry_id },
					method: "POST",
					success: function(result){
						if(expiry_id == '')
						{
							$("#script_name").val(script_id).change();
							$( "#btn_go" ).trigger( "click" );	
							return false;
						}
						else
						{
							$("#expiry2_id").val(expiry_id).change();
							$( "#btn_go" ).trigger( "click" );	
							return false;
						}
					}
				});
			}
		});

		$('body').on('click','.edit_entry',function(){
			var entry_id = parseInt($(this).attr('entry_id'));
			fillEntryByIdForEdit(entry_id);
		});

		$('body').on('click','.del_joomkhas',function(){
			var r = confirm("Are you sure you want to delete this Settlement?");
			if (r == true) {
				var sattlement_master_id = parseInt($(this).attr('sattlement_master_id'));
				var script_id=$('#script_name').val();
				var expiry_id=$('#expiry2_id').val();
				$.ajax({
					url: '<?=base_url('average/ajaxDeleteOnlyJoomkha');?>',
					data: { sattlement_master_id: sattlement_master_id },
					method: "POST",
					success: function(result){
						var script_id = $('#script_name').val();
						var dateFrom = $('#dateFrom').val();
						var dateTo = $('#dateTo').val();

						$.ajax({
							url: '<?=base_url('average/ajaxManualEntry');?>',
							data: { script_id: script_id, dateFrom :dateFrom, dateTo :dateTo },
							method: "POST",
							success: function(result){
								fillExpiry(result);
							}
						});
						$( "#btn_go" ).trigger( "click" );	
					}
				});
			}			
		});

		$(document).on('change', '.enable_pending_qty', function() {
			if(this.checked) {
				// checkbox is checked
				//console.log("checked");
				var next=$(this).next();
				var pending_next=next.attr('pending_qty');
				//sconsole.log(next);
				next.val(pending_next);
				//$('.check'),trigger('change');
			}
			else
			{
				//console.log("Not checked");
				var next = $(this).next();
				var pending_next = next.attr('pending_qty');
				//console.log("Not checked"+next);
				next.val(0);
			}
			if($(this).hasClass('buy_part_chk'))
			{
				$(".sum_buy").change();
			}
			else if($(this).hasClass('sell_part_chk'))
			{
				$(".sum_sell").change();
			}
			//$(".sum_sell").change();
			//$(".sum_sell").change();

		});		

		$('#script_name').change(function(){
			//console.log($(this).val());
			$("#btn_go").trigger('click');
			$( "#buy_part").html("");
			$( "#sell_part").html("");

			$("#save_equal_qty").addClass('disabled');
			$(".btn_save").addClass('disabled');

			var script_id = $(this).val();
			var dateFrom = $('#dateFrom').val();
			var dateTo = $('#dateTo').val();

			$.ajax({
				url: '<?=base_url('average/ajaxManualEntry');?>',
				data: { script_id: script_id, dateFrom :dateFrom, dateTo :dateTo },
				method: "POST",
				success: function(result){
					//fill_entry_joomkhas(result);
					fillExpiry(result);
				}
			});
		});

		$('#account_name').change(function(){
			//fire your ajax call
			var script_id=$('#account_name').val();
			if($.isNumeric($(this).val()))
			{				
				//make ajax call
				$.ajax({
					url: '<?=base_url('entry/getExpiryByScriptId');?>',
					data: { script_id: script_id },
					method: "POST",
					success: function(result){
						//console.log(result);
						$( "#expiry_id").html("");
						var obj = jQuery.parseJSON(result);
						$( "#expiry_id").append( "<option value=0>--Select Expiry--</option>");
						$.each( obj, function( i, val ) {
							//console.log(i+":Price"+val.price)

							$( "#expiry_id").append( "<option value="+val.expiry_id+">"+val.expiry_name+"</option>" );
							//return ( val !== "three" );
						});
						$('[name="expiry_id"]').select2().select2('val','');
						//$('[name="expiry_id"]').select('val','');
					}
				});

				//make ajax call to get brok per and value
				$.ajax({
					url: '<?=base_url('entry/getBrokByScriptId');?>',
					data: { script_id: script_id },
					method: "POST",
					success: function(result){
						//console.log(result);
						var obj = jQuery.parseJSON(result);
						var brokPercentage = obj[0].brokPercentage;
						var brokFix = obj[0].brokFix;
						$( "#brokerage_percentage").val(brokPercentage);
						$( "#brokerage_fix").val(brokFix);
					}
				});
			}
			else
			{
				$( "#expiry_id").html("");
				$( "#expiry_id").attr("disabled","disabled");
			}
			//console.log($(this).val()+"<< Is Numeric >>"+$.isNumeric($(this).val()));
		});

		$( ".target" ).change(function() {
			alert( "Handler for .change() called." );
		});

		$('#buy_sell').keypress(function(event){
			console.log(event.which);
			if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
				event.preventDefault();
			}});

		$('#datepicker').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#dateFrom').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#dateTo').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('.account_required').hide();
		$(document).on('click', '.submit_btn', function () {
			var account_name = $('#account_name').val();
			if(account_name == ''){
				$('.account_required').show();
				return false;
			}
		});
		$("#text_account_name").focus(); 
		$("input,select").bind("keydown",function(event) {
			if (event.which === 13) {
				event.stopPropagation();
				event.preventDefault();
				$(':input:eq(' + ($(':input').index(this) + 1) +')').focus();
			}
		});

		table = $('#table_transaction').DataTable({
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				"url": "<?php echo site_url('entry/transaction-datatable')?>",
				"type": "POST",
				"data": function(d){
					d.as_on_data = $("#as_on_data").val();
				},
			},
			"columnDefs": [
				{
					"targets": [],
					"orderable": false,
				},
			],
		});

		var as_on_data = $('#as_on_data').datepicker({
			autoclose: true,
			format:'dd-mm-yyyy'
		});
		$(document).on('change','#as_on_data',function(){
			table.draw();
		});
	});
	function colSum() {
		var sum=0;
		//iterate through each input and add to sum
		$('.td_sum').each(function() {     
			sum += parseInt($(this).text());                     
		}); 
		//change value of total
		//$('#mySum').html(sum);
	}

	function fill_entry_joomkhas(result)
	{
		$( "#buy_part").html('');
		$( "#sell_part").html('');
		//console.log(result);
		var obj = jQuery.parseJSON(result);
		//console.log(obj.buy[0]['total']);
		if(obj.buy != null)
		{
			var buy_sr_no=1;
			var total_tot = 0;
			var qty_pending_total = 0;
			$.each( obj.buy, function( i, val ) {
				//console.log(i+":id"+val.id);
				var edit_html='';
				if(val.is_edit=='1'){
					edit_html="<a entry_id="+val.id+" class='edit_entry' href='javascript:void(0)'>Edit</a>/";
				}
				var sum=0;
				$("#buy_part").append( "<tr class='blue-madison'><td>"+edit_html+"<a entry_id="+val.id+" class='del_entry' href='javascript:void(0)'>Delete</a></td><td>"+buy_sr_no+"</td><td>"+val.pick_date+"</td><td>"+val.price+"</td><td>"+val.qty_pending+"</td><td>&nbsp;<input type='checkbox' class='enable_pending_qty buy_part_chk'><input type='hidden' script_id="+val.script_id+" entry_id="+val.id+"  price="+val.price+" expiry_id="+val.expiry_id+" pick_date="+val.pick_date+" buy_sell=0 pending_qty="+val.qty_pending+" brok_per="+val.brokPercentage+" brok_fix="+val.brokFix+" value=0 class='sum_buy' id='sum_buy"+i+"' name='sum_buy"+i+"'><button class='btn btn-primary btn-xs disabled btn_save'>Save</button></td><td class='td_sum'>"+val.total+"</td><td>"+val.grand_total_buy_avg+"</td></tr>");
				//sortTablebuy_part();

				//old tr
				//$( "#buy_part").append( "<tr><td>"+edit_html+"<a entry_id="+val.id+" class='del_entry' href='javascript:void(0)'>Delete</a></td><td>"+buy_sr_no+"</td><td>"+val.pick_date+"</td><td>"+val.price+"</td><td>"+val.qty_pending+"</td><td>"+val.qty+"&nbsp;<input type='checkbox' class='enable_pending_qty buy_part_chk'><input script_id="+val.script_id+" entry_id="+val.id+"  price="+val.price+" expiry_id="+val.expiry_id+" pick_date="+val.pick_date+" buy_sell=0 type='text' pending_qty="+val.qty_pending+" brok_per="+val.brokPercentage+" brok_fix="+val.brokFix+" value=0 class='form-control sum_buy' id='sum_buy"+i+"' name='sum_buy"+i+"' style='width: 70px;float: right;  text-align: right;'></td><td>"+val.total+"</td><td>"+val.grand_qty_buy+"</td><td>"+val.grand_total_buy+"</td><td>"+val.grand_total_buy_avg+"</td></tr>");
				buy_sr_no++;
				//return ( val !== "three" );
			});
			for (i = 0; i < obj.buy.length; i++) {  //loop through the array
				total_tot += parseInt(obj.buy[i]['total']);
				qty_pending_total += parseInt(obj.buy[i]['qty_pending']);

			}
			//console.log(total_tot);

			$( "#buy_part").append( "<tr class='blue-madison'><td colspan=4>Grand Total</td><td id='qty_pending_total'></td><td id='buy_total_qty'>0</td><td id='total_tot'></td><td colspan=6></td></tr>" );
			$("#total_tot").html(total_tot);
			$("#qty_pending_total").html(qty_pending_total);
		}
		if(obj.sell != null)
		{
			var sell_sr_no=1;
			var total_totsell = 0;
			var qty_pending_totalsell = 0;
			$.each( obj.sell, function( i, val ) {
				//console.log(":Price"+val.grand_qty_buy)
				var edit_html='';
				if(val.is_edit=='1'){
					edit_html="<a entry_id="+val.id+" class='edit_entry' href='javascript:void(0)'>Edit</a>/";
				}
				$( "#sell_part").append( "<tr class='red-rock'><td>"+edit_html+"<a entry_id="+val.id+" class='del_entry' href='javascript:void(0)'>Delete</a><td>"+sell_sr_no+"</td><td>"+val.pick_date+"</td><td>"+val.price+"</td><td>"+val.qty_pending+"</td><td><input type='checkbox' class='enable_pending_qty sell_part_chk'><input type='hidden' script_id="+val.script_id+" entry_id="+val.id+"  price="+val.price+" expiry_id="+val.expiry_id+" pick_date="+val.pick_date+" buy_sell=1 pending_qty="+val.qty_pending+" brok_per="+val.brokPercentage+" brok_fix="+val.brokFix+" value=0 class='sum_sell' id='sum_sell"+i+"' name='sum_sell"+i+"'><button class='btn btn-primary btn-xs disabled btn_save'>Save</button></td><td>"+val.total+"</td><td>"+val.grand_total_sell_avg+"</td></tr>");
				
				/*Old tr						
				$( "#sell_part").append( "<tr><td>"+edit_html+"<a entry_id="+val.id+" class='del_entry' href='javascript:void(0)'>Delete</a><td>"+sell_sr_no+"</td><td>"+val.pick_date+"</td><td>"+val.price+"</td><td>"+val.qty_pending+"</td><td>"+val.qty+"&nbsp;<input type='checkbox' class='enable_pending_qty sell_part_chk'><input script_id="+val.script_id+" entry_id="+val.id+"  price="+val.price+" expiry_id="+val.expiry_id+" pick_date="+val.pick_date+" buy_sell=1 type='text' pending_qty="+val.qty_pending+" brok_per="+val.brokPercentage+" brok_fix="+val.brokFix+" value=0 class='form-control sum_sell' id='sum_sell"+i+"' name='sum_sell"+i+"' style='width: 70px;float: right; text-align: right;'></td><td>"+val.total+"</td><td>"+val.grand_qty_sell+"</td><td>"+val.grand_total_sell+"</td><td>"+val.grand_total_sell_avg+"</td></tr>");*/
				sell_sr_no++;
			});

			for (i = 0; i < obj.sell.length; i++) {
				total_totsell += parseInt(obj.sell[i]['total']);
				qty_pending_totalsell += parseInt(obj.sell[i]['qty_pending']);
			}

			$( "#sell_part").append( "<tr class='red-rock'><td colspan=4>Grand Total</td><td id='qty_pending_totalsell'></td><td id='sell_total_qty'>0</td><td id='total_totsell'></td><td colspan=6></td></tr>" );
			$("#total_totsell").html(total_totsell);
			$("#qty_pending_totalsell").html(qty_pending_totalsell);
		}

		$('#joomkhas').html('');
		$("#buy_joomkhu").html('');
		$("#sell_joomkhu").html('');

		var sr_no_joomkha=0;

		$.each( obj.joomkha, function( i, val ) {

			//$('#joomkhas').append('<div class="col-md-12"><div class="box box-info box-solid"><div class="box-header with-border"><h3 class="box-title">'+sr_no_joomkha+"#) "+val.date_time+'</h3><a sattlement_master_id='+val.id+' class="btn btn-primary del_joomkhas" href="javascript:void(0)">Delete</a><div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button></div></div>');
			sr_no_joomkha++;
			var buy_data = "";
			var sell_data = "";
			var pl_data = "<div class='col-md-12' style='margin-top: 10px;'><table class='table table-striped table-bordered' style='margin-bottom: 0px;text-align:right;'><tbody>";
			console.log(val.sattlements.buy);
			$.each( val.sattlements.buy, function( i_buy, val_buy ) {
				buy_data+="<tr><td>"+val_buy.qty+"</td><td>"+val_buy.part_qty+"</td><td>"+val_buy.price+"</td><td>"+val_buy.brokPercentage+"</td><td>"+val_buy.brokFix+"</td></tr>";
			});

			var profit_loss = val.total_sell - val.total_buy;
			var net_profit_loss = profit_loss - val.total_brokerage;

			if(profit_loss > 0){
				pl_data+="<tr><td colspan=2><b>Profit-Loss</b></td><td class='total_pl blue-madison'><b>"+profit_loss+"</b></td>";	
			}else{
				pl_data+="<tr><td colspan=2><b>Profit-Loss</b></td><td class='total_pl red-rock'><b>"+profit_loss+"</b></td>";	
			}
			pl_data+="<td colspan=2><b>Total Brokerage</b></td><td class='tot_brok'><b>"+val.total_brokerage+"</b></td>";
			if(net_profit_loss > 0){
				pl_data+="<td colspan=2><b>Net Profit Loss</b></td><td class='net_pl blue-madison'><b>"+net_profit_loss+"</b></td></tr>";	
			}else{
				pl_data+="<td colspan=2><b>Net Profit Loss</b></td><td class='net_pl red-rock'><b>"+net_profit_loss+"</b></td></tr>";	
			}
			pl_data+="</tbody></table></div>";

			//$("#joomkhas").append('<div class="col-md-6"><table class="table table-striped table-bordered"><thead><tr><th>Orig Qty</th><th>Buy Part</th><th>Price</th></tr></thead><tbody>'+buy_data+'</tbody></table></div>');

			$.each( val.sattlements.sell, function( i_sell, val_sell ) {
				sell_data+="<tr><td>"+val_sell.qty+"</td><td>"+val_sell.part_qty+"</td><td>"+val_sell.price+"</td><td>"+val_sell.brokPercentage+"</td><td>"+val_sell.brokFix+"</td></tr>";

			});

			//$("#joomkhas").append('<div class="col-md-6"><table class="table table-striped table-bordered"><thead><tr><th>Orig Qty</th><th>Short Part</th><th>Price</th></tr></thead><tbody>'+sell_data+'</tbody></table></div>');
			//$("#joomkhas").append('<div class="box-body table-responsive" style="display: block;"><div class="col-md-6"><table class="table table-striped table-bordered"><thead><tr><th>Orig Qty</th><th>Buy Part</th><th>Price</th></tr></thead><tbody>'+buy_data+'</tbody></table></div><div class="col-md-6"><table class="table table-striped table-bordered"><thead><tr><th>Orig Qty</th><th>Short Part</th><th>Price</th></tr></thead><tbody>'+sell_data+'</tbody></table></div></div>');
			//console.log(val.sattlements.buy);
			$('#joomkhas').append('<div class="col-md-12"><div class="box box-info box-solid"><div class="box-header with-border"><h4 class="box-title">'+sr_no_joomkha+"#) "+val.date_time+'</h4>&nbsp;&nbsp;<a sattlement_master_id='+val.id+' class="btn btn-primary btn-xs del_joomkhas" href="javascript:void(0)">Delete</a><div class="box-tools pull-right"></div></div><div class="box-body table-responsive" style="display: block;"><div class="col-md-6"><table class="table table-striped table-bordered" style="margin-bottom: 0px;text-align:right;"><thead><tr><th style="text-align:right;">Orig Qty</th><th style="text-align:right;">Buy Part</th><th style="text-align:right;">Price</th><th style="text-align:right;">% Brok</th><th style="text-align:right;">Per Unit Brok</th></tr></thead><tbody class="blue-madison">'+buy_data+'</tbody></table></div><div class="col-md-6"><table class="table table-striped table-bordered" style="margin-bottom: 0px;text-align:right;"><thead><tr><th style="text-align:right;">Orig Qty</th><th style="text-align:right;">Short Part</th><th style="text-align:right;">Price</th><th style="text-align:right;">% Brok</th><th style="text-align:right;">Per Unit Brok</th></th></tr></thead><tbody tbody class="red-rock">'+sell_data+'</tbody></table></div>'+pl_data+'</div></div></div>');
			//$('#joomkhas').append('<div class="col-md-12"><div class="box box-info box-solid"><div class="box-header with-border"><h4 class="box-title">'+sr_no_joomkha+"#) "+val.date_time+'</h4>&nbsp;&nbsp;<a sattlement_master_id='+val.id+' class="btn btn-primary btn-xs del_joomkhas" href="javascript:void(0)">Delete</a><div class="box-tools pull-right"></div></div><div class="box-body table-responsive" style="display: block;"><div class="col-md-6"><table class="table table-striped table-bordered" style="margin-bottom: 0px;text-align:right;"><thead><tr><th style="text-align:right;">Orig Qty</th><th style="text-align:right;">Buy Part</th><th style="text-align:right;">Price</th><th style="text-align:right;">Brok.Per</th><th style="text-align:right;">Brok.Fix</th></tr></thead><tbody>'+buy_data+'</tbody></table></div><div class="col-md-6"><table class="table table-striped table-bordered" style="margin-bottom: 0px;text-align:right;"><thead><tr><th style="text-align:right;">Orig Qty</th><th style="text-align:right;">Short Part</th><th style="text-align:right;">Price<th style="text-align:right;">Brok.Per</th><th style="text-align:right;">Brok.Fix</th></th></tr></thead><tbody>'+sell_data+'</tbody></table></div>'+pl_data+'</div></div></div>');
		});
		var grand_pl = 0;
		var grand_total_brok = 0;
		var grand_net_pl = 0;
		$('.total_pl').each(function(){
			console.log(parseInt($(this).text()));
			grand_pl += parseInt($(this).text()); //<==== a catch  in here !! read below
		});

		$('.tot_brok').each(function(){
			console.log(parseInt($(this).text()));
			grand_total_brok += parseInt($(this).text()); //<==== a catch  in here !! read below
		});

		$('.net_pl').each(function(){
			console.log(parseInt($(this).text()));
			grand_net_pl += parseInt($(this).text()); //<==== a catch  in here !! read below
		});

		$("#joomkhas").append("<div class='col-md-12'><span style='color:Blue;font-weight: bold;font-size: 3em;'>Total Profit/Loss: "+grand_pl+"</span></div>");
		$("#joomkhas").append("<div class='col-md-12'><span style='color:Blue;font-weight: bold;font-size: 3em;'>Grand Total Brokerage: "+grand_total_brok+"</span></div>");
		$("#joomkhas").append("<div class='col-md-12'><span style='color:Blue;font-weight: bold;font-size: 3em;'>Grand Net Profit/Loss: "+grand_net_pl+"</span></div>");


		//$('[name="expiry2_id"]').select2().select2('val','');
	}

	function fillExpiry(result)
	{
		//alert(grand_pl);
		var obj = jQuery.parseJSON(result);
		//New code By sagar for fill expiry2
		$( "#expiry2_id").html("");
		//$( "#expiry2_id").append( "<option value='0'>All</option>");
		$.each( obj.expiry, function( i, val ) {
			//console.log(i+":Price"+val.price)

			$( "#expiry2_id").append( "<option value="+val.expiry_id+">"+val.expiry_name+"</option>" );
			//return ( val !== "three" );
		});
	}

	function account_name_load(){

		$.ajax({
			url: '<?=base_url('entry/get_allaccount');?>',
			dataType: 'json',
			success: function(result){
				//console.log(result);
				//var obj = jQuery.parseJSON(result);
				$( "#account_name").append( "<option value=0>--Select Script--</option>");
				$.each( result, function( i, val ) {
					console.log(result);
					$( "#account_name").append( "<option value="+val.script_id+">"+val.script_name+"</option>" );
				});
			}
		});
	}

	$('#expiry2_id1').blur(function(e) {
		var script_id=$('#script_name').val();
		//console.log($('select#expiry2_id').val());
		var expiry_id = $('select#expiry2_id').val();
		//var expiry_id = id;
		var opts = e.target.options;
		var len = opts.length;
		var selected = [];
		for (var i = 0; i < len; i++) {
			if (opts[i].selected) {
				selected.push(opts[i].value);
			}
		}	

		if(expiry_id == 0)
		{
			//$("#script_name").select2("val", script_id).change();
			//$("#script_name").val(0).change();
			$("#script_name").val(script_id).change();
			return false;
		}
		$.ajax({
			url: '<?=base_url('average/ajaxManualCombineEntry');?>',
			data: { script_id: script_id,expiry_id:expiry_id },
			method: "POST",
			success: function(result){
				//console.log(result);
				fill_entry_joomkhas(result);						
			}
		});
	});


	$('#btn_go').click(function(e) {
		var script_id=$('#script_name').val();
		console.log($('select#expiry2_id').val());
		var expiry_id = $('select#expiry2_id').val();
		var dateFrom = $('#dateFrom').val();
		var dateTo = $('#dateTo').val();

		$.ajax({
			url: '<?=base_url('average/ajaxManualCombineEntry');?>',
			data: { script_id: script_id,expiry_id:expiry_id, dateFrom :dateFrom, dateTo :dateTo },
			method: "POST",
			success: function(result){
				//console.log(result);
				fill_entry_joomkhas(result);						
			}
		});
	});

	function fillEntryById(entry_id){
		$.ajax({
			url: '<?=base_url('average/ajaxEditEntry');?>',
			data: { entry_id: entry_id },
			method: "POST",
			success: function(result){
				var obj = jQuery.parseJSON(result);
				$.each( obj.entry, function( i, val ) {
					//alert($('#datepicker').val()+"--::--"+val.pick_date);
					$('#datepicker').val(val.pick_date);
					$('#account_name').val(val.script_id);
					//$('[name="account_name"]').select2().select2('val',val.script_id);
					$("[name='account_name']").val(val.script_id);
					//$('#buy_sale').select2().select2('val',val.buy_sell);
					$('#buy_sale').val(val.buy_sell);

					$('[name="price"]').val(val.price);
					$('[name="qty"]').val(val.qty);
					//$('#entry_id').val(val.id);
					$('#entry_id').val('');
					$('#brokerage_percentage').val(val.brokPercentage);
					$('#brokerage_fix').val(val.brokFix);
					var selected_expiry_id=val.expiry_id;
					//fire your ajax call
					var script_id=$('#account_name').val();
					if($.isNumeric($('#account_name').val()))
					{				
						//make ajax call
						$.ajax({
							url: '<?=base_url('entry/getExpiryByScriptId');?>',
							data: { script_id: script_id },
							method: "POST",
							success: function(result){
								console.log(result);
								$( "#expiry_id").html("");
								var obj = jQuery.parseJSON(result);
								$( "#expiry_id").append( "<option value=0>--Select Script--</option>");
								$.each( obj, function( i, val ) {
									$( "#expiry_id").append("<option value="+val.expiry_id+">"+val.expiry_name+"</option>" );
								});
								//$('#expiry_id').select2().select2('val',selected_expiry_id);
								$('#expiry_id').val(selected_expiry_id);
							}
						});
					}
					else
					{
						$( "#expiry_id").html("");
						$( "#expiry_id").attr("disabled","disabled");
					}
				});
			}
		});
	}
	function fillEntryByIdForEdit(entry_id){
		$.ajax({
			url: '<?=base_url('average/ajaxEditEntry');?>',
			data: { entry_id: entry_id },
			method: "POST",
			success: function(result){
				var obj = jQuery.parseJSON(result);
				$.each( obj.entry, function( i, val ) {
					//alert(val.pick_date);
					$('#datepicker').val(val.pick_date) ;					
					//$('#datepicker').val(val.pick_date);
					$('#account_name').val(val.script_id);
					$('[name="account_name"]').select2().select2('val',val.script_id);
					$('#buy_sale').val(val.buy_sell);
					$('#buy_sale').select2().select2('val',val.buy_sell);
					$('[name="price"]').val(val.price);
					$('[name="qty"]').val(val.qty);
					$('#entry_id').val(val.id);
					$('#brokerage_percentage').val(val.brokPercentage);
					$('#brokerage_fix').val(val.brokFix);
					var selected_expiry_id=val.expiry_id;
					//fire your ajax call
					var script_id=$('#account_name').val();
					if($.isNumeric($('#account_name').val()))
					{				
						//make ajax call
						$.ajax({
							url: '<?=base_url('entry/getExpiryByScriptId');?>',
							data: { script_id: script_id },
							method: "POST",
							success: function(result){
								console.log(result);
								$( "#expiry_id").html("");
								var obj = jQuery.parseJSON(result);
								$( "#expiry_id").append( "<option value=0>--Select Script--</option>");
								$.each( obj, function( i, val ) {
									$( "#expiry_id").append("<option value="+val.expiry_id+">"+val.expiry_name+"</option>" );
								});
								$('#expiry_id').select2().select2('val',selected_expiry_id);
							}
						});
					}
					else
					{
						$( "#expiry_id").html("");
						$( "#expiry_id").attr("disabled","disabled");
					}
				});
			}
		});
	}

	/* function script_details(id){
        var script_id=$('#script_name').val();
        console.log($('select#expiry2_id').val());
        var expiry_id = $('select#expiry2_id').val();
        //var expiry_id = id;
        var opts = $(this).target.options;
        var len = opts.length;
        var selected = [];
        for (var i = 0; i < len; i++) {
            if (opts[i].selected) {
                selected.push(opts[i].value);
            }
        }
        if(expiry_id == 0)
        {
			//$("#script_name").select2("val", script_id).change();
			//$("#script_name").val(0).change();
			$("#script_name").val(script_id).change();
			return false;
		}
		$.ajax({
			url: '<?=base_url('average/ajaxManualCombineEntry');?>',
			data: { script_id: script_id,expiry_id:expiry_id },
			method: "POST",
			success: function(result){
				//console.log(result);
				fill_entry_joomkhas(result);					
			}
		});
    }*/
	function sortTablebuy_part() {
		//alert('');
		var table, rows, switching, i, x, y, shouldSwitch;
		table = document.getElementById("buy_part");
		switching = true;
		while (switching) {

			switching = false;
			rows = table.getElementsByTagName("TR");
			for (i = 1; i < (rows.length - 1); i++) {

				shouldSwitch = false;

				x = rows[i].getElementsByTagName("TD")[2];
				y = rows[i + 1].getElementsByTagName("TD")[2];

				if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {

					shouldSwitch= true;
					break;
				}
			}
			if (shouldSwitch) {

				rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
				switching = true;
			}
		}
	}

	$(document).keypress(function(e) {
		e.preventdefault();
		if(e.which == 112) {
			$("#buy_sale").val("0");
			$("#tbl_form").addClass("bg-info");
			$("#tbl_form").removeClass("bg-danger");
		}
		if(e.which == 113) {
			$("#buy_sale").val("1");
			$("#tbl_form").addClass("bg-danger");
			$("#tbl_form").removeClass("bg-info");
		}
	});
	$('#buy_sale').on('change', function() {
		if(this.value == 0){
			$("#tbl_form").addClass("bg-info");
			$("#tbl_form").removeClass("bg-danger");
		}else{
			$("#tbl_form").addClass("bg-danger");
			$("#tbl_form").removeClass("bg-info");
		}
	});
	$("#btn_submit").click(function(){
		var account_name = $('#account_name option:selected').val();
		if(account_name == "0"){
			show_notify("Please Select Script name.",false);
			return false;
		}
		var expiry_id = $('#expiry_id option:selected').val();
		if(expiry_id == "0"){
			show_notify("Please Select Expiry.",false);
			return false;
		}
	});
</script>
