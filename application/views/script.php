<?php
if(isset($script)){
	//print_r($script);exit;
}
if($this->session->flashdata('success') == true){?>
<script>
	$(document).ready(function(){
		show_notify("<?=$this->session->flashdata('message')?>",true);
	});
</script>
<?php }?>
<?php if($this->session->flashdata('upload') == true){?>
<script>
	$(document).ready(function(){
		show_notify("<?=$this->session->flashdata('message')?>",false);
	});
</script>
<?php }?>
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">
		<?php if(isset($script) && !empty($script)){ ?>Edit 
		<?php } else { ?>Add 
		<?php } ?>Script
		</h3>
	</div>
	<div class="box-body">
		<form action="<?=base_url()?>entry/saveScript" method="post"  >
			<input type="hidden" name="script_id" value="<?php if(!empty($script)){ echo $script[0]->script_id;} ?>"/>
			<div class="row">
				<div class="col-sm-3">
					<label for="">Script Name:</label>
					<input type="text" class="form-control" id="script_name" name="script_name" required="required" value="<?php if(!empty($script)){ echo $script[0]->script_name;} ?>">
					<ul class="parsley-errors-list filled account_required"  style="display: none;">
						<li class="parsley-required">This value is required.</li>
					</ul>
				</div>
				<div class="col-sm-3">
					<label for="">Brokerage Percentage:</label>
					<input type="text" class="form-control" id="brokerage_percentage" name="brokerage_percentage" value="<?php if(!empty($script)){ echo $script[0]->brokPercentage;}else{echo '0';}?>">
				</div>
				<div class="col-sm-3">
					<label for="">Brokerage Fix:</label>
					<input type="text" class="form-control" id="brokerage_fix" name="brokerage_fix" value="<?php if(!empty($script)){ echo $script[0]->brokFix;}else{echo '0';}?>">
				</div>
				<div class="col-sm-3">
					<label for="">&nbsp;</label>
					<button type="submit" class="btn btn-primary btn-xs form-control">Submit</button>
				</div>
			</div>
		</form>
		<div style="border-bottom: 1px solid #3C8DBC;padding-bottom: 9px;margin: 10px 0 20px 0;"></div>
		<div class="row">
			<div class="col-md-3">
				<label for="">Filter:</label>
				<select class="select2" style="width:100%;" name="filter" id="filter">
					<option value=0>All</option>
					<option value=1 <?php if(isset($_GET['filter']) && $_GET['filter']!='') {echo 'selected';} ?>>Skip 0 pending qty</option>
				</select>
			</div>
		</div>
		<div style="border-bottom: 1px solid #3C8DBC;padding-bottom: 9px;margin: 10px 0 20px 0;"></div>
		<div class="row">
			<div class="col-md-6">
				<!-- general form elements -->
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<h3 class="box-title"> Scripts </h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->

					<div class="box-body table-responsive" style="display: block;">
						<table id="example1" class="table table-striped table-bordered" style="text-align: right;">
							<thead>
								<tr>
									<th align="right" style="text-align: right;">Action</th>
									<th style="display:none;">Index</th>
									<th align="right" style="text-align: right;">Script Name</th>
									<th align="right" style="text-align: right;">Pending Buy Qty</th>
									<th align="right" style="text-align: right;">Pending Short Qty</th>
									<th align="right" style="text-align: right;">Net Qty</th>
									<th align="right" style="text-align: right;">Buy Average</th>
									<th align="right" style="text-align: right;">Short Average</th>
									<th align="right" style="text-align: right;">Brokerage Percentage</th>
									<th align="right" style="text-align: right;">Brokerage Fix</th>
								</tr>
							</thead>
							<tbody id="script_part">
								<?php                               	
								$total_script = count($scripts);
								if($total_script > 1)
								{
									foreach($scripts as $row)
									{
										if(isset($_GET['filter']) && $_GET['filter']!='' && $_GET['filter']!=0)
										{
											if($row['buy_qty_pending'] == 0 && $row['sell_qty_pending'] == 0)
											{	
												continue;
											}
										}
								?>
								<tr>
									<td nowrap> <a href="<?=base_url('average/scripts/'.$row['script_id'])?>" >Edit</a><a onclick="return confirm('Are you sure delete ?')" href="<?=base_url()?>entry/deleteScript?id=<?php echo $row['script_id'];?>" >/Delete</a> </td>
									<td nowrap><a href="<?= base_url();?>average/manualEntry?script_id=<?=$row['script_id'];?>"><?php echo $row['script_name'];?></a></td>
									<td nowrap><?php if($row['buy_qty_pending']==''){ echo '0';} else {echo $row['buy_qty_pending']; }?></td>
									<td nowrap><?php if($row['sell_qty_pending']==''){ echo '0';} else {echo $row['sell_qty_pending']; }?></td>
									<td nowrap><?=$row['buy_qty_pending']-$row['sell_qty_pending'];?></td>
									<td nowrap><?php if($row['b_average']==''){ echo '0';} else {echo $row['b_average']; }?></td>
									<td nowrap><?php if($row['s_average']==''){ echo '0';} else {echo $row['s_average']; }?></td>
									<td nowrap><?php if($row['brokPercentage']==''){ echo '0';} else {echo $row['brokPercentage']; }?></td>
									<td nowrap><?php if($row['brokFix']==''){ echo '0';} else {echo $row['brokFix']; }?></td>
									<!--<td nowrap><?php //echo $row['created_at'];?></td>-->

								</tr>  
								<?php }
								}
								?>

							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
		</div>
	</div>
	<!-- /.box-body -->
</div>
<script>
	$(document).ready(function(){
		$('#filter').change(function(){
			$( "#script_part").html("");
			var filter_id = $(this).val();
			if(filter_id == 1)
				window.location.href = window.location.href + "?filter=1";
			else
				window.location.href = '<?=base_url()?>average/scripts';
		});
	});
</script>
