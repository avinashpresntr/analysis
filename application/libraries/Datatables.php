<?php

/**
 * Class AppLib
 */
class Datatables
{
    var $table = '';
    var $column_order = array();
    var $column_search = array();
    var $order = array();
    var $joins = array();
    var $select = '';
    var $where = array();
    
    /**
     * Datatables constructor.
     * @param array $config
     */
    function __construct(array $config = array())
    {
        $this->ci =& get_instance();
        $this->ci->load->database();

        if (count($config) > 0)
        {
            $this->initialize($config);
        }
    }

    /**
     * Initialize preferences
     *
     * @param	array
     * @return	CI_Datatables
     */
    public function initialize($config = array())
    {
        foreach ($config as $key => $val)
        {
            if (isset($this->$key))
            {
                $this->$key = $val;
            }
        }
        return $this;
    }

    /**
     *
     */
    private function _get_datatables_query()
    {
        if($this->select != ''){
            $this->ci->db->select($this->select);
        }

        $this->ci->db->from($this->table);
        

        if(!empty($this->joins)){
            foreach($this->joins as $join){
                if(isset($join['join_table']) &&  isset($join['join_by']) && $join['join_table'] != '' && $join['join_by'] != ''){
                    $join_table = $join['join_table'];
                    $join_by = $join['join_by'];
                    $join_type = isset($join['join_type'])?$join['join_type']:'inner';
                    $this->ci->db->join("$join_table","$join_by","$join_type");
                }
            }
        }

        $i = 0;
        foreach ($this->column_search as $item) // loop column
        {
            if(isset($_POST['search']['value'])) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->ci->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->ci->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->ci->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->ci->db->group_end(); //close bracket
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->ci->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->ci->db->order_by(key($order), $order[key($order)]);
        }
        
        if(isset($this->where))
        {
            $where = $this->where;
            $this->ci->db->where(key($where), $where[key($where)]);
        }
        
    }

    /**
     * @return mixed
     */
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(isset($_POST['length']) && $_POST['length'] != -1)
            $this->ci->db->limit($_POST['length'],$_POST['start']);
        $query = $this->ci->db->get();
        return $query->result();
    }

    /**
     * @return mixed
     */
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->ci->db->get();
        return $query->num_rows();
    }

    /**
     * @return mixed
     */
    public function count_all()
    {
        $this->ci->db->from($this->table);
        return $this->ci->db->count_all_results();
    }
}