<?php
class EntryModel extends CI_Model
{
    protected $currCompetition = 0;
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getBuySell(){
       
        //$this->db->from('entry');
     
        $buyorsell['buy']= "";
        $buyorsell['sell']= "";
        $query =  $this->db->query('SELECT entry.*,scripts.script_name FROM entry, scripts where entry.script_id = scripts.script_id ORDER BY entry.id DESC');
        if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				//print_r($value);
				if($value['buy_sell'] == 0)
				{
					$buyorsell['buy'][$key]=$value;
				}
				else
				{
					$buyorsell['sell'][$key]=$value;
				}
			}
        }else{
            return false;
        }
        return $buyorsell;
        /*echo "<pre>";
        print_r($buyorsell['buy']);
        echo "will you get me someone";
        print_r($buyorsell['sell']);
        echo "</pre>";*/
    }


    function getBuySellByScriptId($script_id){
       
        //$this->db->from('entry');
     
        $buyorsell['buy']= "";
        $buyorsell['sell']= "";
        $query =  $this->db->query('SELECT entry.*,scripts.script_name FROM entry, scripts where entry.script_id = scripts.script_id and scripts.script_id='.$script_id.' ORDER BY entry.id ASC');
        //$query =  $this->db->query('SELECT entry.*,scripts.script_name FROM entry, scripts where entry.script_id = scripts.script_id and scripts.script_id='.$script_id.' ORDER BY entry.id DESC');
        if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				//print_r($value);
				if($value['buy_sell'] == 0)
				{
					$buyorsell['buy'][$key]=$value;
				}
				else
				{
					$buyorsell['sell'][$key]=$value;
				}
			}
        }else{
            return false;
        }
        return $buyorsell;        
    }

    function getBuySellByScriptIdAndDates($script_id,$fromDate,$toDate)
    {
		$buyorsell['buy']= "";
        $buyorsell['sell']= "";
        $query =  $this->db->query('SELECT entry.*,scripts.script_name FROM entry, scripts where entry.script_id = scripts.script_id and scripts.script_id='.$script_id." AND pick_date>='".$fromDate."' AND pick_date<='".$toDate."' ORDER BY id DESC");
        //echo 'SELECT entry.*,scripts.script_name FROM entry, scripts where entry.script_id = scripts.script_id and scripts.script_id='.$script_id." AND pick_date>='".$fromDate."' AND pick_date<='".$toDate."'";exit;
        if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				//print_r($value);
				if($value['buy_sell'] == 0)
				{
					$buyorsell['buy'][$key]=$value;
				}
				else
				{
					$buyorsell['sell'][$key]=$value;
				}
			}
        }else{
            return false;
        }
        return $buyorsell;      
	}
	
    function getBuySellByScriptAndExpiryId($script_id,$expiry_id,$fromDate,$toDate){
       
        //$this->db->from('entry');
     
        $buyorsell['buy']= "";
        $buyorsell['sell']= "";
		
		if($fromDate!='' && $toDate!=''){
			//$sql_query='SELECT entry.*,scripts.script_name FROM entry, scripts where entry.script_id = scripts.script_id and scripts.script_id='.$script_id.' OR entry.id IN (SELECT sattlement.entry_id from sattlement,sattlement_master WHERE sattlement_master.id=sattlement.settlement_master_id ) AND entry.pick_date>="'.date('Y-m-d',strtotime(str_replace("/", "-",$fromDate))).'" AND entry.pick_date<="'.date('Y-m-d',strtotime(str_replace("/", "-",$toDate))).'"';

			$sql_query='SELECT entry.*,scripts.script_name FROM entry, scripts where entry.script_id = scripts.script_id and scripts.script_id='.$script_id.' AND entry.pick_date>="'.date('Y-m-d',strtotime(str_replace("/", "-",$fromDate))).'" AND entry.pick_date<="'.date('Y-m-d',strtotime(str_replace("/", "-",$toDate))).'"';
			
			if($expiry_id!='')
			{
				$sql_query.=' AND entry.expiry_id IN("'.$expiry_id.'")';
			}
			$sql_query.=' ORDER BY entry.pick_date ASC';
			//echo $sql_query;exit;
			$query =  $this->db->query($sql_query);
			
		}
		else{
			$sql_query='SELECT entry.*,scripts.script_name FROM entry, scripts where entry.script_id = scripts.script_id and scripts.script_id='.$script_id.' ';
			if($expiry_id!='')
			{
				$sql_query.=' AND entry.expiry_id IN("'.$expiry_id.'") ';
			}
			$sql_query.=' ORDER BY entry.pick_date ASC';
        	$query =  $this->db->query($sql_query);
		}
		
        if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				//print_r($value);
				if($value['buy_sell'] == 0)
				{
					$buyorsell['buy'][$key]=$value;
				}
				else
				{
					$buyorsell['sell'][$key]=$value;
				}
			}
        }else{
            return false;
        }
        return $buyorsell;        
    }

    function getAllScripts()
    {
		$query =  $this->db->query('SELECT *,(SELECT sum(qty_pending) from entry where script_id=scripts.script_id AND buy_sell=0 AND qty_pending>=0) AS buy_qty_pending,(SELECT sum(qty_pending) from entry where script_id=scripts.script_id AND buy_sell=1 AND qty_pending>=0) AS sell_qty_pending,(select ROUND(sum(qty_pending*price)/sum(qty_pending),2) from entry where buy_sell=0 AND qty_pending>=0 AND script_id=scripts.script_id) as b_average,(select ROUND(sum(qty_pending*price)/sum(qty_pending),2) from entry where buy_sell=1 AND qty_pending>=0 AND script_id=scripts.script_id) as s_average FROM scripts');
        if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				$buyorsell[$key]=$value;
			}
        }else{
            return false;
        }
        return $buyorsell;
	}

	 function getExpiryByScriptId($script_id){       
        //$this->db->from('entry');        
        $query =  $this->db->query('SELECT expiry_id,expiry_name FROM expiry WHERE script_id='.$script_id);
        if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				$data[$key]['expiry_id']=$value['expiry_id'];
				$data[$key]['expiry_name']=$value['expiry_name'];
			}
        }else{
            return false;
        }
        return $data;        
    }
    function getBrokByScriptId($script_id){       
        //$this->db->from('entry');        
        $query =  $this->db->query('SELECT brokPercentage,brokFix FROM scripts WHERE script_id='.$script_id);
        if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				$data[$key]['brokPercentage']=$value['brokPercentage'];
				$data[$key]['brokFix']=$value['brokFix'];
			}
        }else{
            return false;
        }
        return $data;        
    }

    function getScripts(){       
        //$this->db->from('entry');        
        $query =  $this->db->query('SELECT script_id,script_name FROM scripts');
        if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				$data[$key]['script_id']=$value['script_id'];
				$data[$key]['script_name']=$value['script_name'];
			}
        }else{
            return false;
        }
        return $data;        
    }

	
	function getAllExpiry()
    {
		$query =  $this->db->query('SELECT expiry.expiry_id,expiry.expiry_name,scripts.script_name FROM expiry, scripts where expiry.script_id=scripts.script_id');
        if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				$expiry[$key]=$value;
			}
        }else{
            return false;
        }
        return $expiry;
	}

	function deleteSettlement($entry_id)
	{
		$query =  $this->db->query('SELECT * FROM `sattlement` WHERE `entry_id` = '.$entry_id);
        if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				$data[$key]=$value['settlement_master_id'];
			}
        }else{
            return false;
        }
        if(isset($data) && !empty($data))
        {
			$whereclause = implode(',',$data);
			$this->db->query("DELETE from sattlement_master WHERE id IN (".$whereclause.")");
			$this->db->query("DELETE from sattlement WHERE entry_id = ".$entry_id."");
		}
	}
	
	function updateEntryByEntryIdForJoomkhaDelete($entry_id,$qty)
	{
		$query =  $this->db->query('SELECT * FROM `entry` WHERE `id` = '.$entry_id);
        if($query->num_rows() > 0)
        {
			foreach($query->result_array() as $key => $value)
			{
				$qty_pending=$value['qty_pending'];
				$data['qty_pending'] = $qty_pending + $qty;
				$this->db->where('id', $entry_id);
				$this->db->update('entry', $data);

				$this->db->where('entry_id', $entry_id);
				$this->db->delete('sattlement');
			}
        }
	}
	
	function getEntryByEntryId($entry_id){
       
        //$this->db->from('entry');
     
        $entryData['entry']= "";
        $query =  $this->db->query('SELECT * FROM entry where id='.$entry_id.'');
        if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				$entryData['entry'][$key]=$value;
			}
        }else{
            return false;
        }
        return $entryData;        
    }
	
	function get_all_with_where($table_name,$order_by_column,$order_by_value,$where_array)
	{
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where($where_array);
		$this->db->order_by($order_by_column,$order_by_value);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
