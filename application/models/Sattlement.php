<?php
class Sattlement extends CI_Model
{
    protected $currCompetition = 0;
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getMaterByScriptId($script_id){
       
        //$this->db->from('entry');
        $query =  $this->db->query('SELECT * FROM `sattlement_master` where script_id = '.$script_id);
        if($query->num_rows() > 0){
			 return $query->result_array();
        }else{
            return false;
        }
        //return $query->result_array();
    }

    function getSattlementByMasterId($master_id){
       
        //$this->db->from('entry');
        $query =  $this->db->query('SELECT * FROM `sattlement_master` where script_id = '.$script_id);
        if($query->num_rows() > 0){
			 return $query->result_array();
        }else{
            return false;
        }
        //return $query->result_array();
    }

     function getSettlementByScriptId($script_id){
       
        //$this->db->from('entry');
        $settlements = array();
        $master =  $this->db->query('SELECT * FROM `sattlement_master` where script_id = '.$script_id);
        if($master->num_rows() > 0){
			foreach($master->result_array() as $key => $value)
			{
				$settlements[$value['id']]['id'] = $value['id'];
				$settlements[$value['id']]['script_id'] = $value['script_id'];
				$settlements[$value['id']]['expiry_id'] = $value['expiry_id'];
				$settlements[$value['id']]['date_time'] = $value['date_time'];
				
				$child =  $this->db->query('SELECT sattlement.*,sattlement.qty as part_qty,DATE_FORMAT(entry.pick_date,"%d/%m/%Y") AS newDate FROM `sattlement`,`entry` where sattlement.entry_id=entry.id AND sattlement.settlement_master_id = '.$value['id']);
				
				$total_buy = 0;
				$total_sell = 0;
				$total_brokerage = 0;
				
				if($child->num_rows() > 0){
					foreach($child->result_array() as $key1 => $value1)
					{
						if($value1['buy_sell'] == 0)
						{
							$settlements[$value['id']]['sattlements']['buy'][$value1['id']] = $value1;
							$total_buy = $total_buy+($value1['qty']*$value1['price']);
						}
						else
						{
							$settlements[$value['id']]['sattlements']['sell'][$value1['id']] = $value1;
							$total_sell = $total_sell+($value1['qty']*$value1['price']);				
						}

						if($value1['brokPercentage'] > 0)
						{
							$total_brokerage = $total_brokerage+($value1['qty']*$value1['price']*$value1['brokPercentage']/100);
						}
						else
						{
							$total_brokerage = $total_brokerage+($value1['qty']*$value1['brokFix']);				
						}
					}
					$settlements[$value['id']]['total_buy'] = $total_buy;
					$settlements[$value['id']]['total_sell'] = $total_sell;
					$settlements[$value['id']]['total_brokerage'] = $total_brokerage;
				}
			}
        }else{
            return false;
        }        
        return $settlements;        
    }
    
    function getSettlementByScriptAndExpiryId($script_id,$expiry_id = 0,$fromDate,$toDate){
        
        $settlements = array();
		
        if($fromDate!='' && $toDate!=''){
			$sql_query='SELECT * FROM `sattlement_master` where script_id = '.$script_id.' AND max_date>="'.date('Y-m-d',strtotime($fromDate)).'" AND 
		max_date<="'.date('Y-m-d',strtotime($toDate)).'"';
			if($expiry_id!='')	
			{
				$sql_query.=' AND expiry_id IN("'.$expiry_id.'")';
			}
			$sql_query .=' ORDER BY id DESC';
			$master =  $this->db->query($sql_query);
		}
		else{
			$sql_query='SELECT * FROM `sattlement_master` where script_id = '.$script_id.' ';
			if($expiry_id!='')
			{
				$sql_query.=' AND expiry_id IN("'.$expiry_id.'")';
			}
			$sql_query .=' ORDER BY id DESC';
			$master =  $this->db->query($sql_query);
		}
		
        if($master->num_rows() > 0){
			foreach($master->result_array() as $key => $value)
			{
				$settlements[$value['id']]['id'] = $value['id'];
				$settlements[$value['id']]['script_id'] = $value['script_id'];
				$settlements[$value['id']]['expiry_id'] = $value['expiry_id'];
				$settlements[$value['id']]['date_time'] = $value['date_time'];
				
				$child =  $this->db->query('SELECT sattlement.*,sattlement.qty as part_qty,DATE_FORMAT(entry.pick_date,"%d/%m/%Y") AS newDate FROM `sattlement`,`entry` where sattlement.entry_id=entry.id AND sattlement.settlement_master_id = '.$value['id']);
				
				$total_buy = 0;
				$total_sell = 0;
				$total_brokerage = 0;
				
				if($child->num_rows() > 0){
					foreach($child->result_array() as $key1 => $value1)
					{
						if($value1['buy_sell'] == 0)
						{
							$settlements[$value['id']]['sattlements']['buy'][$value1['id']] = $value1;
							$total_buy = $total_buy+($value1['qty']*$value1['price']);
						}
						else
						{
							$settlements[$value['id']]['sattlements']['sell'][$value1['id']] = $value1;
							$total_sell = $total_sell+($value1['qty']*$value1['price']);				
						}

						if($value1['brokPercentage'] > 0)
						{
							$total_brokerage = $total_brokerage+($value1['qty']*$value1['price']*$value1['brokPercentage']/100);
						}
						else
						{
							$total_brokerage = $total_brokerage+($value1['qty']*$value1['brokFix']);				
						}
					}
					$settlements[$value['id']]['total_buy'] = $total_buy;
					$settlements[$value['id']]['total_sell'] = $total_sell;
					$settlements[$value['id']]['total_brokerage'] = $total_brokerage;
				}
			}
        }else{
            return false;
        }       
        return $settlements;        
    }

    function getSettlementById($sattlement_master_id)
    {
        $query =  $this->db->query('SELECT * FROM `sattlement` where settlement_master_id = '.$sattlement_master_id);
        if($query->num_rows() > 0){
			 return $query->result_array();
        }else{
            return false;
        }
    }
	function getSettlementByEntryId($entry_id)
    {
        $query =  $this->db->query('SELECT * FROM `sattlement` where entry_id = '.$entry_id);
        if($query->num_rows() > 0){
			 return $query->result_array();
        }else{
            return false;
        }
    }
}
?>
