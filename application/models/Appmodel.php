<?php
ini_set('max_execution_time', 3000);

class AppModel extends CI_Model
{
    protected $currCompetition = 0;
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function login($username,$pass){
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('admin_email',$username);
        $this->db->where('admin_password',md5($pass));
        $this->db->limit('admin');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return false;
        }
    }
    
    function fileList($filenameLike)
    {
        $this->db->select('*');
        $this->db->from('files');
        $this->db->like('file_name', $filenameLike);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function fileList_order($filenameLike,$field,$order)
    {
        $this->db->select('*');
        $this->db->from('files');
        $this->db->like('file_name', $filenameLike);
        $this->db->order_by($field, $order);
        $query = $this->db->get();
        
        return $query->result();
    }
    //High Low 
    public function highlowlist()
    {
        $this->db->select("*");
        $this->db->from("highlow_sheet");
        $query = $this->db->get();
        return $query->result();
    }
    public function checkHighlowDate($hl_date)
    {
        $this->db->select('*');
        $this->db->from('highlow_sheet');
        $this->db->where('hl_date',$hl_date);
        
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return 1;
        }else{
            return 0;
        }
    }
    
    function updateHighlow($data)
    {
        $this->db->where('hl_date', $data['hl_date']);
        $this->db->update('highlow_sheet', $data); 
    }
    
    function insertHighlow($data)
    {
        $this->db->insert('highlow_sheet',$data);
    }
    
    //Data sheet
    public function filenamelist($namelike)
    {
        $this->db->select('*');
        $this->db->from("files");
        $this->db->like('file_name', $namelike);
        $query = $this->db->get();
        return $query->result();
    }
    public function datasheetlist()
    {
        $this->db->select('do_date,do_buy_sell,do_price,dt_absolute_performance');
        $this->db->from("data_sheet");
        $query = $this->db->get();
        return $query->result();
    }
    public function insertFilename($Filename,$data_file_id){
        $this->db->set('file_name',$Filename);
        
        $this->db->set('data_file_id',$data_file_id);
        $this->db->set('currentdate','NOW()',FALSE);
        $this->db->insert('files');
        $lastId = $this->db->insert_id();
        return $lastId;
    }
    
    public function checkDatasheetDate($do_date)
    {
        $this->db->select('*');
        $this->db->from('data_sheet');
        $this->db->where('do_date',$do_date);
        
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return 1;
        }else{
            return 0;
        }
    }
    
    function updateDatasheet($data)
    {
        $this->db->where('do_date', $data['do_date']);
        $this->db->update('data_sheet', $data); 
    }
    
    function checkFilename($Filename)
    {
        $this->db->select('*');
        $this->db->from('files');
        $this->db->where('file_name',$Filename);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return 1;
        }else{
            return 0;
        }
    }
    function insertDatasheet($data)
    {
        $this->db->insert('data_sheet',$data);
    }
    function insertStatistics($data_statistics)
    {
        $this->db->insert('file_statistics',$data_statistics);
    }
    
    function fromtolist($id)
    {
        $this->db->select('*');
        $this->db->from('data_sheet');
        $this->db->where('file_id',$id);
        $query = $this->db->get();
        return $query->result();
        
    }
    function tabledata($file_id, $tablename, $tablefield)
    {
        $this->db->select($tablefield);
        $this->db->from($tablename);
        $this->db->where('file_id',$file_id);
        $query = $this->db->get();
        return $query->result();
    }
	function getFileByShorOrBuy($id,$buyorshort)
    {
        $this->db->select('*');
        $this->db->from('data_sheet');
        $this->db->where('file_id',$id);
        $this->db->where('do_buy_sell',$buyorshort);
        $this->db->order_by('do_date', 'ASC');
        //short by date
        $query = $this->db->get();
        return $query->result();        
    }
	
	function getFileByShortOrBuyMinus($id,$buyorshort)
    {
        $this->db->select('*');
        $this->db->from('data_sheet');
        $this->db->where('file_id',$id);
        $this->db->where('do_buy_sell',$buyorshort);
		$this->db->where('dt_absolute_performance < ',0);
        $this->db->order_by('do_date', 'ASC');
        //short by date
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        return $query->result();        
    }
   
    function getHighLowSheetData($data_sheet_id)
    {
        $this->db->select('*');
        $this->db->from('highlow_sheet');
        $this->db->where('data_file_id',$data_sheet_id);
        $this->db->order_by('hl_date', 'ASC');
       // $this->db->where('do_buy_sell',$buyorshort);
        //short by date
        $query = $this->db->get();
        if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
        
    }
    
    function getHighLowDataByDate($currentdate,$nextdate,$file_id)
    {
		/*if($currentdate==$nextdate)
		{*/
			//$date = new DateTime($nextdate);
			//$date->add(new DateInterval('P10D'));
			//echo $date->format('Y-m-d') . "\n";
		//}
        $this->db->select('*');
        $this->db->from('highlow_sheet');
        //$this->db->where('hl_date',$date);
        $this->db->where('hl_date >', $currentdate);
		$this->db->where('hl_date <=', $nextdate);
		$this->db->where('data_file_id',$file_id);
		$this->db->order_by('hl_date', 'ASC');
		//$this->db->limit(1);
        //$this->db->order_by('hl_date', 'ASC');
       // $this->db->where('do_buy_sell',$buyorshort);
        //short by date
        $query = $this->db->get();        
		//echo $this->db->last_query();exit;
        return $query->result();
    }
	
	  
    function getHighLowDataByDate2($currentdate,$nextdate,$file_id)
    {
		
        $this->db->select('*');
        $this->db->from('highlow_sheet');
        
        $this->db->where('hl_date >=', $currentdate);
		$this->db->where('hl_date <', $nextdate);
		$this->db->where('data_file_id',$file_id);
		$this->db->order_by('hl_date', 'ASC');
		
        $query = $this->db->get();        
		
        return $query->result();
    }
    
	public function getFilenamebyId($fileid)
	{
		$this->db->select('file_name');
        $this->db->from('files');
        $this->db->where('file_id',$fileid);        
        $query = $this->db->get();        
        return $query->row();
	}
	
    public function searchDatafileId($filename)
    {
        $this->db->select('file_id');
        $this->db->from("files");
        $this->db->like('file_name', $filename);
        $query = $this->db->get();
        $query->result();
        foreach ($query->result() as $row)
        {
              $file_id = $row->file_id;
        }
        if(!empty($file_id)){
            return $file_id;    
        }else{
            return 0;
        }
    }
    
    function updateDatasheetHlId($data_file_id, $lastId)
    {
        $data = array('hl_file_id' => $lastId);
        $this->db->where('file_id', $data_file_id);
        $this->db->update('data_sheet', $data); 
    }
    
    function delete_multiple($ids, $tbl)
    {
        $this->db->where_in('file_id', $ids)->delete($tbl);
        $this->db->where_in('file_id', $ids)->delete("files");
    }
    
    function delete_multiple_2($ids, $tbl)
    { 
        $this->db->where_in('file_id', $ids)->delete($tbl);
        $this->db->where_in('file_id', $ids)->delete("files");
        $this->db->where_in('data_file_id', $ids)->delete('highlow_sheet');
        $this->db->where_in('data_file_id', $ids)->delete('files');
        $this->db->where_in('file_id', $ids)->delete('file_statistics');
    }
    
    function getFileStatistics($id)
    {
        $this->db->select('*');
        $this->db->from('file_statistics');
        $this->db->where('file_id',$id);
        //$this->db->where('do_buy_sell',$buyorshort);
		//$this->db->where('dt_absolute_performance < ',0);
        $this->db->order_by('file_statistics_id', 'ASC');
        //short by date
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        return $query->result();
    }
    
    function get_allMasterRecored($table,$id,$name){
        $query = $this->db->query("SELECT `" . $id . "`  as id,`" . $name . "` as text FROM `" . $table . "` ORDER BY `" . $name . "` ASC");
        return $query->result();
    }

    function get_allMasterRecoredExpiry($table,$id,$name,$scriptId){
		
        $query = $this->db->query("SELECT `" . $id . "`  as id,`" . $name . "` as text FROM `" . $table . "` WHERE script_id = ".$scriptId."  ORDER BY `" . $name . "` ASC");
        return $query->result();

        echo json_encode($recored_arr);
        return $recored_arr;
    }
	
	function update($table_name,$data_array,$where_array){
		$this->db->where($where_array);
		$rs = $this->db->update($table_name, $data_array);
		return $rs;
	}
}
