<?php
ini_set('max_execution_time', 3000);

class FileModel extends CI_Model
{
    protected $currCompetition = 0;
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }   
    
    function updateFile($data)
    {
        $this->db->where('file_id', $data['file_id']);
        $this->db->update('files', $data);
    }
}
