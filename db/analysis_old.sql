-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 20, 2017 at 04:58 PM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 5.6.29-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `analysis`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(22) NOT NULL,
  `admin_lname` varchar(22) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_password` varchar(222) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_email`, `admin_img`, `admin_password`) VALUES
(1, 'main', 'admin', 'admin@gmail.com', '', '75d23af433e0cea4c0e45a56dba18b30');

-- --------------------------------------------------------

--
-- Table structure for table `data_sheet`
--

CREATE TABLE `data_sheet` (
  `data_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `ds_name` varchar(222) NOT NULL,
  `ds_all_trades` double NOT NULL,
  `ds_long_trades` double NOT NULL,
  `ds_short_trades` double NOT NULL,
  `do_date` datetime NOT NULL,
  `do_buy_sell` varchar(222) NOT NULL,
  `do_price` double NOT NULL,
  `do_qty` double NOT NULL,
  `do_current_value` double NOT NULL,
  `dt_brokerage_fees` varchar(222) NOT NULL,
  `dt_entry_date` varchar(222) NOT NULL,
  `dt_exit_date` varchar(222) NOT NULL,
  `dt_type` varchar(222) NOT NULL,
  `dt_no_bars` varchar(222) NOT NULL,
  `dt_absolute_performance` double NOT NULL,
  `dt_brokerage` double NOT NULL,
  `hl_file_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(222) NOT NULL,
  `data_file_id` int(11) DEFAULT NULL,
  `currentdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `highlow_sheet`
--

CREATE TABLE `highlow_sheet` (
  `hl_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `hl_date` date NOT NULL,
  `hl_time` time NOT NULL,
  `hl_open` double NOT NULL,
  `hl_high` double NOT NULL,
  `hl_low` double NOT NULL,
  `hl_close` double NOT NULL,
  `hl_back_test_signal` varchar(222) NOT NULL,
  `data_file_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `data_sheet`
--
ALTER TABLE `data_sheet`
  ADD PRIMARY KEY (`data_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `highlow_sheet`
--
ALTER TABLE `highlow_sheet`
  ADD PRIMARY KEY (`hl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `data_sheet`
--
ALTER TABLE `data_sheet`
  MODIFY `data_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=485;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `highlow_sheet`
--
ALTER TABLE `highlow_sheet`
  MODIFY `hl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1035;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
ALTER TABLE `scripts` ADD `buy_average` DOUBLE NOT NULL DEFAULT '0' AFTER `created_at`, ADD `short_average` DOUBLE NOT NULL DEFAULT '0' AFTER `buy_average`;
