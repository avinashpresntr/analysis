-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 28, 2017 at 08:44 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `analysis`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(22) NOT NULL,
  `admin_lname` varchar(22) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_password` varchar(222) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_email`, `admin_img`, `admin_password`) VALUES
(1, 'main', 'admin', 'admin@gmail.com', '', '75d23af433e0cea4c0e45a56dba18b30');

-- --------------------------------------------------------

--
-- Table structure for table `data_sheet`
--

CREATE TABLE `data_sheet` (
  `data_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `ds_name` varchar(222) NOT NULL,
  `ds_all_trades` double NOT NULL,
  `ds_long_trades` double NOT NULL,
  `ds_short_trades` double NOT NULL,
  `do_date` datetime NOT NULL,
  `do_buy_sell` varchar(222) NOT NULL,
  `do_price` double NOT NULL,
  `do_qty` double NOT NULL,
  `do_current_value` double NOT NULL,
  `dt_brokerage_fees` varchar(222) NOT NULL,
  `dt_entry_date` varchar(222) NOT NULL,
  `dt_exit_date` varchar(222) NOT NULL,
  `dt_type` varchar(222) NOT NULL,
  `dt_no_bars` varchar(222) NOT NULL,
  `dt_absolute_performance` double NOT NULL,
  `dt_brokerage` double NOT NULL,
  `hl_file_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `entry`
--

CREATE TABLE `entry` (
  `id` int(11) NOT NULL,
  `pick_date` date DEFAULT NULL,
  `script_id` int(11) NOT NULL COMMENT 'Script name from files table',
  `expiry_id` int(11) NOT NULL DEFAULT '1' COMMENT '1= Cash(Default)',
  `price` double NOT NULL,
  `qty` double NOT NULL,
  `qty_pending` double NOT NULL,
  `buy_sell` int(11) NOT NULL,
  `client_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entry`
--

INSERT INTO `entry` (`id`, `pick_date`, `script_id`, `expiry_id`, `price`, `qty`, `qty_pending`, `buy_sell`, `client_id`) VALUES
(1, '2017-02-15', 1, 1, 10, 20, 17, 0, 1),
(2, '2017-02-25', 2, 1, 78, 23, 23, 1, 1),
(3, '2017-02-25', 1, 1, 20, 10, 8, 0, 1),
(4, '2017-02-10', 2, 1, 20, 5, 5, 0, 1),
(5, '2017-02-18', 1, 1, 15, 20, 14, 1, 1),
(6, '2017-02-17', 2, 1, 10, 50, 50, 0, 1),
(7, '2017-02-21', 1, 1, 30, 15, 14, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `expiry`
--

CREATE TABLE `expiry` (
  `expiry_id` int(11) NOT NULL,
  `script_id` int(11) NOT NULL,
  `expiry_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expiry`
--

INSERT INTO `expiry` (`expiry_id`, `script_id`, `expiry_name`) VALUES
(1, 1, 'Rajkot'),
(2, 1, 'Vadodara'),
(3, 1, 'Surat'),
(4, 2, 'Bombay'),
(5, 2, 'Nagpur'),
(6, 2, 'Pune');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(222) NOT NULL,
  `data_file_id` int(11) DEFAULT NULL,
  `currentdate` datetime NOT NULL,
  `highest_high` double DEFAULT NULL,
  `lowest_low` double DEFAULT NULL,
  `count_buy` text,
  `count_short` text,
  `buy_first_percentage` double DEFAULT NULL,
  `buy_first_percentage_long` double DEFAULT NULL,
  `buy_first_percentage_short` double DEFAULT NULL,
  `buy_average_percentage` double DEFAULT NULL,
  `buy_average_percentage_long` double DEFAULT NULL,
  `buy_average_percentage_short` double DEFAULT NULL,
  `buy_last_percentage` double DEFAULT NULL,
  `buy_last_percentage_long` double DEFAULT NULL,
  `buy_last_percentage_short` double DEFAULT NULL,
  `short_first_percentage` double DEFAULT NULL,
  `short_first_percentage_long` double DEFAULT NULL,
  `short_first_percentage_short` double DEFAULT NULL,
  `short_average_percentage` double DEFAULT NULL,
  `short_average_percentage_long` double DEFAULT NULL,
  `short_average_percentage_short` double DEFAULT NULL,
  `short_last_percentage` double DEFAULT NULL,
  `short_last_percentage_long` double DEFAULT NULL,
  `short_last_percentage_short` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `file_statistics`
--

CREATE TABLE `file_statistics` (
  `file_statistics_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `statistics_text` varchar(222) NOT NULL,
  `all_trades` double NOT NULL,
  `long_trades` double NOT NULL,
  `short_trades` double NOT NULL,
  `isNA` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `highlow_sheet`
--

CREATE TABLE `highlow_sheet` (
  `hl_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `hl_date` date NOT NULL,
  `hl_time` time NOT NULL,
  `hl_open` double NOT NULL,
  `hl_high` double NOT NULL,
  `hl_low` double NOT NULL,
  `hl_close` double NOT NULL,
  `hl_back_test_signal` varchar(222) NOT NULL,
  `data_file_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sattlement`
--

CREATE TABLE `sattlement` (
  `id` int(11) NOT NULL,
  `settlement_master_id` int(11) NOT NULL,
  `entry_id` int(11) NOT NULL,
  `script_id` int(11) NOT NULL,
  `expiry_id` int(11) NOT NULL,
  `qty` double NOT NULL,
  `price` int(11) NOT NULL,
  `buy_sell` int(11) NOT NULL COMMENT '0 = buy, 1= sell'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sattlement`
--

INSERT INTO `sattlement` (`id`, `settlement_master_id`, `entry_id`, `script_id`, `expiry_id`, `qty`, `price`, `buy_sell`) VALUES
(18, 7, 1, 1, 1, 3, 10, 0),
(19, 7, 3, 1, 1, 2, 20, 0),
(20, 7, 7, 1, 1, 1, 30, 0),
(21, 7, 5, 1, 1, 6, 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sattlement_master`
--

CREATE TABLE `sattlement_master` (
  `id` int(11) NOT NULL,
  `script_id` int(11) NOT NULL,
  `expiry_id` int(11) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sattlement_master`
--

INSERT INTO `sattlement_master` (`id`, `script_id`, `expiry_id`, `date_time`) VALUES
(7, 1, 1, '2017-02-28 09:23:07');

-- --------------------------------------------------------

--
-- Table structure for table `scripts`
--

CREATE TABLE `scripts` (
  `script_id` int(11) NOT NULL,
  `script_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `scripts`
--

INSERT INTO `scripts` (`script_id`, `script_name`, `created_at`) VALUES
(1, 'gold', '2017-02-18 10:23:35'),
(2, 'silver', '2017-02-18 10:24:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `data_sheet`
--
ALTER TABLE `data_sheet`
  ADD PRIMARY KEY (`data_id`);

--
-- Indexes for table `entry`
--
ALTER TABLE `entry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seller_beacon_id` (`script_id`);

--
-- Indexes for table `expiry`
--
ALTER TABLE `expiry`
  ADD PRIMARY KEY (`expiry_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `file_statistics`
--
ALTER TABLE `file_statistics`
  ADD PRIMARY KEY (`file_statistics_id`),
  ADD KEY `file_statistics_ibfk_1` (`file_id`);

--
-- Indexes for table `highlow_sheet`
--
ALTER TABLE `highlow_sheet`
  ADD PRIMARY KEY (`hl_id`);

--
-- Indexes for table `sattlement`
--
ALTER TABLE `sattlement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sattlement_master`
--
ALTER TABLE `sattlement_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scripts`
--
ALTER TABLE `scripts`
  ADD PRIMARY KEY (`script_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `data_sheet`
--
ALTER TABLE `data_sheet`
  MODIFY `data_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entry`
--
ALTER TABLE `entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `expiry`
--
ALTER TABLE `expiry`
  MODIFY `expiry_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `file_statistics`
--
ALTER TABLE `file_statistics`
  MODIFY `file_statistics_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `highlow_sheet`
--
ALTER TABLE `highlow_sheet`
  MODIFY `hl_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sattlement`
--
ALTER TABLE `sattlement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `sattlement_master`
--
ALTER TABLE `sattlement_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `scripts`
--
ALTER TABLE `scripts`
  MODIFY `script_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `entry`
--
ALTER TABLE `entry`
  ADD CONSTRAINT `seller_beacon_id` FOREIGN KEY (`script_id`) REFERENCES `scripts` (`script_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
ALTER TABLE `scripts` ADD `buy_average` DOUBLE NOT NULL DEFAULT '0' AFTER `created_at`, ADD `short_average` DOUBLE NOT NULL DEFAULT '0' AFTER `buy_average`;
ALTER TABLE `sattlement_master` ADD `max_date` DATE NULL AFTER `date_time`;
ALTER TABLE `entry` CHANGE `expiry_id` `expiry_id` VARCHAR(255) NOT NULL DEFAULT '1' COMMENT '1= Cash(Default)';
ALTER TABLE `scripts` ADD `brokPercentage` DOUBLE NULL AFTER `short_average`, ADD `brokFix` DOUBLE NULL AFTER `brokPercentage`;
ALTER TABLE `entry` ADD `brokPercentage` DOUBLE NULL AFTER `client_id`, ADD `brokFix` DOUBLE NULL AFTER `brokPercentage`;
ALTER TABLE `sattlement` ADD `brokPercentage` DOUBLE NULL AFTER `buy_sell`, ADD `brokFix` DOUBLE NULL AFTER `brokPercentage`;
ALTER TABLE `sattlement` CHANGE `price` `price` DOUBLE NOT NULL;

-- Avinash : 2018_08_22 02:15 PM
ALTER TABLE `files` CHANGE `file_id` `file_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `data_sheet` CHANGE `data_id` `data_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `file_statistics` CHANGE `file_statistics_id` `file_statistics_id` INT(11) NOT NULL AUTO_INCREMENT;
